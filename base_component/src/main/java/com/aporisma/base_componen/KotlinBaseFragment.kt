package com.arc.base_core_component

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

abstract class KotlinBaseFragment : Fragment(){

    protected lateinit var inflater: LayoutInflater
    var intentData: Bundle? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        this.inflater = inflater
        val view = inflater.inflate(this.getLayoutId(), container, false)
        this.intentData = this.arguments
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        this.initBaseCreateView()
    }

    protected abstract fun getLayoutId(): Int
    protected abstract fun initBaseCreateView()
    protected abstract fun initDestroy()

    override fun onDestroy() {
        initDestroy()
        super.onDestroy()
    }
}