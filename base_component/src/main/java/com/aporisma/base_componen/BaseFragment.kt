package com.arc.base_core_component

import android.widget.Toast

abstract class BaseFragment: KotlinBaseFragment() {
    override fun initBaseCreateView() {
        initCreateView()
        onClick()
    }

    protected abstract fun initCreateView()

    override fun onDestroyView() {
        super.onDestroyView()
    }

    open fun onClick(){

    }

    protected fun showToast(message:String){
        Toast.makeText(context,message, Toast.LENGTH_SHORT).show()
    }
}