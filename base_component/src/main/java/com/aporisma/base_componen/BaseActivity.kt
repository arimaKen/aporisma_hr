package com.aporisma.base_componen

import android.widget.Toast
import com.arc.base_core_component.KotlinBaseActivity

abstract class BaseActivity : KotlinBaseActivity(){
    override fun initBeforeCreateContent() {

    }

    override fun initBaseCreate() {
        updateStatusBarTextColor(STATUSBAR_TEXT_COLOR_LIGHT)
        initCreate()
        onClick()
    }

    protected abstract fun initCreate()

    open fun onClick(){

    }

    override fun onBackPressed() {
        finish()

    }

    protected fun showToast(message:String){
        Toast.makeText(this,message, Toast.LENGTH_SHORT).show()
    }
}