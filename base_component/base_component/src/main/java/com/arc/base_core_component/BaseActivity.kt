package com.arc.base_core_component

import android.widget.Toast

abstract class BaseActivity : KotlinBaseActivity(){
    override fun initBeforeCreateContent() {

    }

    override fun initBaseCreate() {
        updateStatusBarTextColor(STATUSBAR_TEXT_COLOR_LIGHT)
        initCreate()

        onClick()
    }

    protected abstract fun initCreate()

    open fun onClick(){

    }

    override fun onBackPressed() {
        if(supportFragmentManager.backStackEntryCount <= 1){
            finish()
        }else{
            supportFragmentManager.popBackStack()
        }
    }

    protected fun showToast(message:String){
        Toast.makeText(this,message, Toast.LENGTH_SHORT).show()
    }
}