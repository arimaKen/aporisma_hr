package com.aporisma.aporisma_api.Repo;

import android.content.Context;
import com.aporisma.aporisma_api.Response.ResponseCourier;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("ALL")
public class BaseRepo {
    public Context context;
    public ResponseCourier courier;
    public Map<String, Object> headerRequest = new HashMap<>();
    public String userId;


    public BaseRepo(Context context, ResponseCourier courier) {
        this.context = context;
        this.courier = courier;

        headerRequest.put("Content-type", "application/json");
    }
}
