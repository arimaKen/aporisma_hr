package com.aporisma.aporisma_api.Repo;

import android.content.Context;
import android.util.Log;

import com.aporisma.aporisma_api.AporismaClient;
import com.aporisma.aporisma_api.Model.Approval.getRow.APIResultApvTS;
import com.aporisma.aporisma_api.Model.Approval.getRow.APIResultRApvReim;
import com.aporisma.aporisma_api.Model.Approval.APIResult;
import com.aporisma.aporisma_api.Model.BaseModel;
import com.aporisma.aporisma_api.Model.Company.APIResultCompany;
import com.aporisma.aporisma_api.Model.Employee.APIResultEmployee;
import com.aporisma.aporisma_api.Model.Employee.APIResultGetAprvAccess;
import com.aporisma.aporisma_api.Model.Employee.APIResultGroupE;
import com.aporisma.aporisma_api.Model.Employee.APIResultMasterEmployee;
import com.aporisma.aporisma_api.Model.Notification.APIResultAnnounc;
import com.aporisma.aporisma_api.Model.Notification.APIResultNotification;
import com.aporisma.aporisma_api.Model.Notification.APIResultReadNotif;
import com.aporisma.aporisma_api.Model.Paid.APIResultPaidReim;
import com.aporisma.aporisma_api.Model.Param.APIResultParam;
import com.aporisma.aporisma_api.Model.Reimbursement.APIResultListReimHeader;
import com.aporisma.aporisma_api.Model.Reimbursement.APIResultReimDetail;
import com.aporisma.aporisma_api.Model.Reimbursement.APIResultReimHeader;
import com.aporisma.aporisma_api.Model.TimeSheet.APIResultTSD;
import com.aporisma.aporisma_api.Model.TimeSheet.APIResultTSH;
import com.aporisma.aporisma_api.Model.TimeSheet.APIResultTSHD;
import com.aporisma.aporisma_api.Model.TimeSheet.APIResultTSHRow;
import com.aporisma.aporisma_api.Model.Token.APIResultToken;
import com.aporisma.aporisma_api.Model.Token.Token;
import com.aporisma.aporisma_api.Response.ResponseCourier;
import com.google.gson.Gson;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;
import static com.aporisma.aporisma_api.AporismaClient.getInstance;
import static com.aporisma.aporisma_api.AporismaClient.getService;

@SuppressWarnings("SpellCheckingInspection")
public class Repo extends BaseRepo {

    private static retrofit2.Call call1;
    private static String tokenAuth;

    public Repo(Context context, ResponseCourier courier) {
        super(context, courier);
    }

    public void callTOKEN() {
            Token request = new Token();
            request.username = "api";
            request.password = "api";
            String requestJSON = new Gson().toJson(request);
            getInstance();

            Call<APIResultToken> call = AporismaClient.getService().getToken(headerRequest, requestJSON);
            call.enqueue(new Callback<APIResultToken>() {
                @Override
                public void onResponse (Call<APIResultToken> call,
                                        Response<APIResultToken> response) {
                    if (response.code() == 200) {
                        tokenAuth = response.body().token;
                        courier.getResponse(response.body(), response.body().message);

                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            courier.getResponse(null, jsonObject.getString("error"));
                        } catch (Exception e) {
                            courier.getResponse(null, response.message());
                        }
                    }
                }

                @Override
                public void onFailure (Call<APIResultToken> call,
                                       Throwable t) {
                    courier.getResponse(null, "Please check your connection");

                }
            });
    }

    public void login(String request) {

        getInstance();
        try {
            Call<APIResultEmployee> call = getService().login("Bearer " + tokenAuth, headerRequest, request);
            call1 = call;
            call.enqueue(new Callback<APIResultEmployee>() {
                @Override
                public void onResponse(Call<APIResultEmployee> call, Response<APIResultEmployee> response) {
                    if (response.isSuccessful()) {
                        courier.getResponse(response.body(), response.body().message);
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            courier.getResponse(null, jsonObject.getString("error"));
                        } catch (Exception e) {
                            courier.getResponse(null, response.message());
                        }
                    }
                }

                @Override
                public void onFailure(Call<APIResultEmployee> call, Throwable t) {
                    if (call.isCanceled()) {
                        courier.getResponse(null, "Request Cancel");
                    } else {
                        courier.getResponse(null, "Fail to connect server\n" + t.toString());
                    }
                }
            });
        } catch (NoClassDefFoundError | RuntimeException e) {
            courier.getResponse(null, e.toString());
        }
    }

    public void cancelRequest() {
        call1.cancel();
    }

    public void getlistNotif(String id_employee) {
        getInstance();
        try {
            Call<APIResultNotification> call = getService().getListNotif("Bearer " + tokenAuth, id_employee);
            call.enqueue(new Callback<APIResultNotification>() {
                @Override
                public void onResponse(Call<APIResultNotification> call, Response<APIResultNotification> response) {
                    if (response.isSuccessful()) {
                        courier.getResponse(response.body(), "");
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            courier.getResponse(null, jsonObject.getString("error"));
                        } catch (Exception e) {
                            courier.getResponse(null, response.message());
                        }
                    }
                }

                @Override
                public void onFailure(Call<APIResultNotification> call, Throwable t) {
                    courier.getResponse(null, "Fail to connect server\n" + t.toString());
                    Log.e("Throwable --- >", t.toString());
                }
            });
        } catch (Exception e) {
            courier.getResponse(null, e.toString());
        }


    }

    public void readNotif(String id_notif) {
        getInstance();
        try {
            Call<APIResultReadNotif> call = getService().readNotif("Bearer " + tokenAuth, id_notif);
            call.enqueue(new Callback<APIResultReadNotif>() {
                @Override
                public void onResponse(Call<APIResultReadNotif> call, Response<APIResultReadNotif> response) {
                    if (response.isSuccessful()) {
                        courier.getResponse(response.body(), "");
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            courier.getResponse(null, jsonObject.getString("error"));
                        } catch (Exception e) {
                            courier.getResponse(null, response.message());
                        }
                    }
                }

                @Override
                public void onFailure(Call<APIResultReadNotif> call, Throwable t) {
                    courier.getResponse(null, "Fail to connect server\n" + t.toString());
                    Log.e("Throwable --- >", t.toString());
                }
            });
        } catch (Exception e) {
            courier.getResponse(null, e.toString());
        }


    }

    public void getAnnouncement(String id_notif) {
        getInstance();
        try {
            Call<APIResultAnnounc> call = getService().getAnnouncement("Bearer " + tokenAuth, id_notif);
            call.enqueue(new Callback<APIResultAnnounc>() {
                @Override
                public void onResponse(Call<APIResultAnnounc> call, Response<APIResultAnnounc> response) {
                    if (response.isSuccessful()) {
                        courier.getResponse(response.body(), "");
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            courier.getResponse(null, jsonObject.getString("error"));
                        } catch (Exception e) {
                            courier.getResponse(null, response.message());
                        }
                    }
                }

                @Override
                public void onFailure(Call<APIResultAnnounc> call, Throwable t) {
                    courier.getResponse(null, "Fail to connect server\n" + t.toString());
                    Log.e("Throwable --- >", t.toString());
                }
            });
        } catch (Exception e) {
            courier.getResponse(null, e.toString());
        }


    }

    public void appvSubmission(final String request) {

//        headerRequest.put("Authorization", "Bearer " + tokenAuth);

        getInstance();
        Call<APIResult> call = getService().approvedSubmission("Bearer " + tokenAuth, headerRequest, request);
        call.enqueue(new Callback<APIResult>() {

            @Override
            public void onResponse(Call<APIResult> call, Response<APIResult> response) {
                if (response.isSuccessful()) {
                    courier.getResponse(response.body(), response.body().message);
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        courier.getResponse(null, jsonObject.getString("error"));
                    } catch (Exception e) {
                        courier.getResponse(null, response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<APIResult> call, Throwable t) {
                courier.getResponse(null, "Fail to connect server\n" + t.toString());
                Log.d(TAG, "onFailure: " + t.toString());

            }
        });
    }

    //-----------------Employee------------------------------------------------

    public void getEmployeeData(String id_company, String id_employee) {
        getInstance();
        try {
            Call<APIResultMasterEmployee> call = getService().getEmployeeData("Bearer " + tokenAuth, id_company, id_employee);
            call1 = call;
            call.enqueue(new Callback<APIResultMasterEmployee>() {
                @Override
                public void onResponse(Call<APIResultMasterEmployee> call, Response<APIResultMasterEmployee> response) {
                    if (response.isSuccessful()) {
                        courier.getResponse(response.body(), response.message());
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            courier.getResponse(null, jsonObject.getString("error"));
                        } catch (Exception e) {
                            courier.getResponse(null, response.message());
                        }
                    }
                }

                @Override
                public void onFailure(Call<APIResultMasterEmployee> call, Throwable t) {
                    if (call.isCanceled()) {
                        courier.getResponse(null, "Request Cancel");
                    } else {
                        courier.getResponse(null, "Fail to connect server\n" + t.toString());
                        Log.i("error JSON", t.toString());
                    }
                }
            });
        } catch (NoClassDefFoundError | RuntimeException e) {
            courier.getResponse(null, e.toString());
        }
    }

    public void getTypeClaim(String id_company) {

        getInstance();
        try {
            Call<APIResultParam> call = getService().getParam("Bearer " + tokenAuth, "REIMBURSE", id_company);

            call.enqueue(new Callback<APIResultParam>() {
                @Override
                public void onResponse(Call<APIResultParam> call, Response<APIResultParam> response) {
                    if (response.isSuccessful()) {
                        courier.getResponse(response.body(), response.message());
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            courier.getResponse(null, jsonObject.getString("error"));
                        } catch (Exception e) {
                            courier.getResponse(null, response.message());
                        }
                    }
                }

                @Override
                public void onFailure(Call<APIResultParam> call, Throwable t) {
                    courier.getResponse(null, "Fail to connect server\n" + t.toString());
                }
            });
        } catch (Exception e) {
            courier.getResponse(null, e.toString());
        }


    }

    public void getTypeTask(String id_company) {

        getInstance();
        try {
            Call<APIResultParam> call = getService().getParam("Bearer " + tokenAuth, "TASK", id_company);

            call.enqueue(new Callback<APIResultParam>() {
                @Override
                public void onResponse(Call<APIResultParam> call, Response<APIResultParam> response) {
                    if (response.isSuccessful()) {
                        courier.getResponse(response.body(), response.message());
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            courier.getResponse(null, jsonObject.getString("error"));
                        } catch (Exception e) {
                            courier.getResponse(null, response.message());
                        }
                    }
                }

                @Override
                public void onFailure(Call<APIResultParam> call, Throwable t) {
                    courier.getResponse(null, "Fail to connect server\n" + t.toString());
                }
            });
        } catch (Exception e) {
            courier.getResponse(null, e.toString());
        }


    }

    public void getCustList(String id_company) {

        getInstance();
        try {
            Call<APIResultParam> call = getService().getParam("Bearer " + tokenAuth, "CUST", id_company);

            call.enqueue(new Callback<APIResultParam>() {
                @Override
                public void onResponse(Call<APIResultParam> call, Response<APIResultParam> response) {
                    if (response.isSuccessful()) {
                        courier.getResponse(response.body(), response.message());
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            courier.getResponse(null, jsonObject.getString("error"));
                        } catch (Exception e) {
                            courier.getResponse(null, response.message());
                        }
                    }
                }

                @Override
                public void onFailure(Call<APIResultParam> call, Throwable t) {
                    courier.getResponse(null, "Fail to connect server\n" + t.toString());
                }
            });
        } catch (Exception e) {
            courier.getResponse(null, e.toString());
        }


    }

    public void getGReim(String id_company,String id_employee) {

        getInstance();
        try {
            Call<APIResultGroupE> call = getService().getGroupEmpl("Bearer " + tokenAuth, id_company,id_employee, "REIMBURSE");

            call.enqueue(new Callback<APIResultGroupE>() {
                @Override
                public void onResponse(Call<APIResultGroupE> call, Response<APIResultGroupE> response) {
                    if (response.isSuccessful()) {
                        courier.getResponse(response.body(), response.message());
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            courier.getResponse(null, jsonObject.getString("error"));
                        } catch (Exception e) {
                            courier.getResponse(null, response.message());
                        }
                    }
                }

                @Override
                public void onFailure(Call<APIResultGroupE> call, Throwable t) {
                    courier.getResponse(null, "Fail to connect server\n" + t.toString());
                }
            });
        } catch (Exception e) {
            courier.getResponse(null, e.toString());
        }


    }

    public void getGTask(String id_company,String id_employee) {

        getInstance();
        try {
            Call<APIResultGroupE> call = getService().getGroupEmpl("Bearer " + tokenAuth, id_company,id_employee, "TASK");

            call.enqueue(new Callback<APIResultGroupE>() {
                @Override
                public void onResponse(Call<APIResultGroupE> call, Response<APIResultGroupE> response) {
                    if (response.isSuccessful()) {
                        courier.getResponse(response.body(), response.message());
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            courier.getResponse(null, jsonObject.getString("error"));
                        } catch (Exception e) {
                            courier.getResponse(null, response.message());
                        }
                    }
                }

                @Override
                public void onFailure(Call<APIResultGroupE> call, Throwable t) {
                    courier.getResponse(null, "Fail to connect server\n" + t.toString());
                }
            });
        } catch (Exception e) {
            courier.getResponse(null, e.toString());
        }


    }

    public void accessAproval(String id_company,String id_employee) {

        getInstance();
        try {
            Call<APIResultGetAprvAccess> call = getService().getAccessApproval("Bearer " + tokenAuth, id_company,id_employee);

            call.enqueue(new Callback<APIResultGetAprvAccess>() {
                @Override
                public void onResponse(Call<APIResultGetAprvAccess> call, Response<APIResultGetAprvAccess> response) {
                    if (response.isSuccessful()) {
                        courier.getResponse(response.body(), response.message());
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            courier.getResponse(null, jsonObject.getString("error"));
                        } catch (Exception e) {
                            courier.getResponse(null, response.message());
                        }
                    }
                }

                @Override
                public void onFailure(Call<APIResultGetAprvAccess> call, Throwable t) {
                    courier.getResponse(null, "Fail to connect server\n" + t.toString());
                }
            });
        } catch (Exception e) {
            courier.getResponse(null, e.toString());
        }


    }

    public void searchCompany(String code) {

        getInstance();
        try {
            Call<APIResultCompany> call = getService().searchCompany(code, "Bearer " + tokenAuth);
            call.enqueue(new Callback<APIResultCompany>() {
                @Override
                public void onResponse(Call<APIResultCompany> call, Response<APIResultCompany> response) {
                    if (response.body().error == null) {
                        courier.getResponse(response.body(), response.message());
                    } else {
                        courier.getResponse(null, response.body().error);
                    }
                }

                @Override
                public void onFailure(Call<APIResultCompany> call, Throwable t) {
                    courier.getResponse(null, "Fail to connect server\n" + t.toString());
                    Log.e("error Exception", t.toString());
                }
            });
        } catch (Exception e) {
            courier.getResponse(null, e.toString());
        }


    }

    //-----------------Reim------------------------------------------------
    public void getReimHeader(String id_employee,String id_company) {
        getInstance();
        try {
            Call<APIResultListReimHeader> call = getService().getReimHeader("Bearer " + tokenAuth, id_employee,id_company);
            call.enqueue(new Callback<APIResultListReimHeader>() {
                @Override
                public void onResponse(Call<APIResultListReimHeader> call, Response<APIResultListReimHeader> response) {
                    if (response.isSuccessful()) {
                        courier.getResponse(response.body(), "");
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            courier.getResponse(null, jsonObject.getString("error"));
                        } catch (Exception e) {
                            courier.getResponse(null, response.message());
                        }
                    }
                }

                @Override
                public void onFailure(Call<APIResultListReimHeader> call, Throwable t) {
                    courier.getResponse(null, "Fail to connect server\n" + t.toString());
                    Log.e("Throwable --- >", t.toString());
                }
            });
        } catch (Exception e) {
            courier.getResponse(null, e.toString());
        }


    }

    public void getReimHDetail(String id_reimH) {
        getInstance();
        try {
            Call<APIResultReimHeader> call = getService().getReimHeaderD("Bearer " + tokenAuth, id_reimH);
            call.enqueue(new Callback<APIResultReimHeader>() {
                @Override
                public void onResponse(Call<APIResultReimHeader> call, Response<APIResultReimHeader> response) {
                    if (response.isSuccessful()) {
                        courier.getResponse(response.body(), "");
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            courier.getResponse(null, jsonObject.getString("error"));
                        } catch (Exception e) {
                            courier.getResponse(null, response.message());
                        }
                    }
                }

                @Override
                public void onFailure(Call<APIResultReimHeader> call, Throwable t) {
                    courier.getResponse(null, "Fail to connect server\n" + t.toString());
                    Log.e("Throwable --- >", t.toString());
                }
            });
        } catch (Exception e) {
            courier.getResponse(null, e.toString());
        }


    }

    public void getReimDetail(String id_employee) {

        getInstance();
        try {
            Call<APIResultReimDetail> call = getService().getReimDetail("Bearer " + tokenAuth, id_employee);
            call.enqueue(new Callback<APIResultReimDetail>() {
                @Override
                public void onResponse(Call<APIResultReimDetail> call, Response<APIResultReimDetail> response) {
                    if (response.isSuccessful()) {
                        courier.getResponse(response.body(), response.body().message);
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            courier.getResponse(null, jsonObject.getString("error"));
                        } catch (Exception e) {
                            courier.getResponse(null, response.message());
                        }
                    }
                }

                @Override
                public void onFailure(Call<APIResultReimDetail> call, Throwable t) {
                    courier.getResponse(null, "Fail to connect server\n" + t.toString());
                    Log.e("Throwable --- >", t.toString());
                }
            });
        } catch (Exception e) {
            courier.getResponse(null, e.toString());
        }


    }

    public void addReimDetail(final String request) {

//        headerRequest.put("Authorization", "Bearer " + tokenAuth);

        getInstance();
        Call<APIResultReimDetail> call = getService().addReimDetail("Bearer " + tokenAuth, headerRequest, request);
        call.enqueue(new Callback<APIResultReimDetail>() {

            @Override
            public void onResponse(Call<APIResultReimDetail> call, Response<APIResultReimDetail> response) {
                if (response.isSuccessful()) {
                    courier.getResponse(response.body(), response.body().message);
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        courier.getResponse(null, jsonObject.getString("error"));
                    } catch (Exception e) {
                        courier.getResponse(null, response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<APIResultReimDetail> call, Throwable t) {
                courier.getResponse(null, "Fail to connect server\n" + t.toString());
                Log.d(TAG, "onFailure: " + t.toString());

            }
        });
    }

    public void updateReimDetail(final String request) {

//        headerRequest.put("Authorization", "Bearer " + tokenAuth);

        getInstance();
        Call<APIResultReimDetail> call = getService().updateReimDetail("Bearer " + tokenAuth, headerRequest, request);
        call.enqueue(new Callback<APIResultReimDetail>() {

            @Override
            public void onResponse(Call<APIResultReimDetail> call, Response<APIResultReimDetail> response) {
                if (response.isSuccessful()) {
                    courier.getResponse(response.body(), response.body().message);
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        courier.getResponse(null, jsonObject.getString("error"));
                    } catch (Exception e) {
                        courier.getResponse(null, response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<APIResultReimDetail> call, Throwable t) {
                courier.getResponse(null, "Fail to connect server\n" + t.toString());
                Log.d(TAG, "onFailure: " + t.toString());

            }
        });
    }

    public void addReimHeader(final String request) {

//        headerRequest.put("Authorization", "Bearer " + tokenAuth);

        getInstance();
        Call<APIResultReimHeader> call = getService().addReimHeader("Bearer " + tokenAuth, headerRequest, request);
        call.enqueue(new Callback<APIResultReimHeader>() {

            @Override
            public void onResponse(Call<APIResultReimHeader> call, Response<APIResultReimHeader> response) {
                if (response.isSuccessful()) {
                    courier.getResponse(response.body(), response.body().message);
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        courier.getResponse(null, jsonObject.getString("error"));
                    } catch (Exception e) {
                        courier.getResponse(null, response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<APIResultReimHeader> call, Throwable t) {
                courier.getResponse(null, "Fail to connect server\n" + t.toString());
                Log.d(TAG, "onFailure: " + t.toString());

            }
        });
    }

    public void updateReimHeader(final String request) {

//        headerRequest.put("Authorization", "Bearer " + tokenAuth);

        getInstance();
        Call<APIResultReimHeader> call = getService().updateReimHeader("Bearer " + tokenAuth, headerRequest, request);
        call.enqueue(new Callback<APIResultReimHeader>() {

            @Override
            public void onResponse(Call<APIResultReimHeader> call, Response<APIResultReimHeader> response) {
                if (response.isSuccessful()) {
                    courier.getResponse(response.body(), response.body().message);
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        courier.getResponse(null, jsonObject.getString("error"));
                    } catch (Exception e) {
                        courier.getResponse(null, response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<APIResultReimHeader> call, Throwable t) {
                courier.getResponse(null, "Fail to connect server\n" + t.toString());
                Log.d(TAG, "onFailure: " + t.toString());

            }
        });
    }

    public void getRowReimApv(String id_company) {

        getInstance();
        try {
            Call<APIResultRApvReim> call = getService().getRowReimEmplApv("Bearer " + tokenAuth,id_company);
            call.enqueue(new Callback<APIResultRApvReim>() {
                @Override
                public void onResponse(Call<APIResultRApvReim> call, Response<APIResultRApvReim> response) {
                    if (response.isSuccessful()) {
                        courier.getResponse(response.body(), response.body().message);
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            courier.getResponse(null, jsonObject.getString("error"));
                        } catch (Exception e) {
                            courier.getResponse(null, response.message());
                        }
                    }
                }

                @Override
                public void onFailure(Call<APIResultRApvReim> call, Throwable t) {
                    courier.getResponse(null, "Fail to connect server\n" + t.toString());
                    Log.e("Throwable --- >", t.toString());
                }
            });
        } catch (Exception e) {
            courier.getResponse(null, e.toString());
        }


    }

    public void paidReim(String idHeaderReim) {

        getInstance();
        try {
            Call<APIResultPaidReim> call = getService().paidReim("Bearer " + tokenAuth, idHeaderReim);
            call.enqueue(new Callback<APIResultPaidReim>() {
                @Override
                public void onResponse(Call<APIResultPaidReim> call, Response<APIResultPaidReim> response) {
                    if (response.isSuccessful()) {
                        courier.getResponse(response.body(), response.body().message);
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            courier.getResponse(null, jsonObject.getString("error"));
                        } catch (Exception e) {
                            courier.getResponse(null, response.message());
                        }
                    }
                }

                @Override
                public void onFailure(Call<APIResultPaidReim> call, Throwable t) {
                    courier.getResponse(null, "Fail to connect server\n" + t.toString());
                    Log.e("Throwable --- >", t.toString());
                }
            });
        } catch (Exception e) {
            courier.getResponse(null, e.toString());
        }


    }

    public void approveReim(final String request) {

//        headerRequest.put("Authorization", "Bearer " + tokenAuth);

        getInstance();
        Call<APIResult> call = getService().approvedReim("Bearer " + tokenAuth, headerRequest, request);
        call.enqueue(new Callback<APIResult>() {

            @Override
            public void onResponse(Call<APIResult> call, Response<APIResult> response) {
                if (response.isSuccessful()) {
                    courier.getResponse(response.body(), response.body().message);
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        courier.getResponse(null, jsonObject.getString("error"));
                    } catch (Exception e) {
                        courier.getResponse(null, response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<APIResult> call, Throwable t) {
                courier.getResponse(null, "Fail to connect server\n" + t.toString());
                Log.d(TAG, "onFailure: " + t.toString());

            }
        });
    }

    public void delReimD(final String id_reim) {

//        headerRequest.put("Authorization", "Bearer " + tokenAuth);

        getInstance();
        Call<BaseModel> call = getService().delReimD("Bearer " + tokenAuth, id_reim);
        call.enqueue(new Callback<BaseModel>() {

            @Override
            public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                if (response.isSuccessful()) {
                    courier.getResponse(response.body(), response.body().message);
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        courier.getResponse(null, jsonObject.getString("error"));
                    } catch (Exception e) {
                        courier.getResponse(null, response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseModel> call, Throwable t) {
                courier.getResponse(null, "Fail to connect server\n" + t.toString());
                Log.d(TAG, "onFailure: " + t.toString());

            }
        });
    }

    //----------------------Timesheet------------------------------------------
    public void getTimesheetH(String id_employee) {
        getInstance();
        try {
            Call<APIResultTSHRow> call = getService().getTSHeader("Bearer " + tokenAuth, id_employee);
            call.enqueue(new Callback<APIResultTSHRow>() {
                @Override
                public void onResponse(Call<APIResultTSHRow> call, Response<APIResultTSHRow> response) {
                    if (response.isSuccessful()) {
                        courier.getResponse(response.body(), "");
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            courier.getResponse(null, jsonObject.getString("error"));
                        } catch (Exception e) {
                            courier.getResponse(null, response.message());
                        }
                    }
                }

                @Override
                public void onFailure(Call<APIResultTSHRow> call, Throwable t) {
                    courier.getResponse(null, "Fail to connect server\n" + t.toString());
                    Log.e("Throwable --- >", t.toString());
                }
            });
        } catch (Exception e) {
            courier.getResponse(null, e.toString());
        }


    }

    public void getTimesheetD(String id_employee) {
        getInstance();
        try {
            Call<APIResultTSD> call = getService().getTSDetails("Bearer " + tokenAuth, id_employee);
            call.enqueue(new Callback<APIResultTSD>() {
                @Override
                public void onResponse(Call<APIResultTSD> call, Response<APIResultTSD> response) {
                    if (response.isSuccessful()) {
                        courier.getResponse(response.body(), "");
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            courier.getResponse(null, jsonObject.getString("error"));
                        } catch (Exception e) {
                            courier.getResponse(null, response.message());
                        }
                    }
                }

                @Override
                public void onFailure(Call<APIResultTSD> call, Throwable t) {
                    courier.getResponse(null, "Fail to connect server\n" + t.toString());
                    Log.e("Throwable --- >", t.toString());
                }
            });
        } catch (Exception e) {
            courier.getResponse(null, e.toString());
        }


    }

    public void getTimesheetHD(String id_tsForm) {
        getInstance();
        try {
            Call<APIResultTSHD> call = getService().getTSHD("Bearer " + tokenAuth, id_tsForm);
            call.enqueue(new Callback<APIResultTSHD>() {
                @Override
                public void onResponse(Call<APIResultTSHD> call, Response<APIResultTSHD> response) {
                    if (response.isSuccessful()) {
                        courier.getResponse(response.body(), "");
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            courier.getResponse(null, jsonObject.getString("error"));
                        } catch (Exception e) {
                            courier.getResponse(null, response.message());
                        }
                    }
                }

                @Override
                public void onFailure(Call<APIResultTSHD> call, Throwable t) {
                    courier.getResponse(null, "Fail to connect server\n" + t.toString());
                    Log.e("Throwable --- >", t.toString());
                }
            });
        } catch (Exception e) {
            courier.getResponse(null, e.toString());
        }


    }

    public void addTSDetail(final String request) {

//        headerRequest.put("Authorization", "Bearer " + tokenAuth);

        getInstance();
        Call<APIResultTSD> call = getService().addTSDetails("Bearer " + tokenAuth, headerRequest, request);
        call.enqueue(new Callback<APIResultTSD>() {

            @Override
            public void onResponse(Call<APIResultTSD> call, Response<APIResultTSD> response) {
                if (response.isSuccessful()) {
                    courier.getResponse(response.body(), response.body().message);
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        courier.getResponse(null, jsonObject.getString("error"));
                    } catch (Exception e) {
                        courier.getResponse(null, response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<APIResultTSD> call, Throwable t) {
                courier.getResponse(null, "Fail to connect server\n" + t.toString());
                Log.d(TAG, "onFailure: " + t.toString());

            }
        });
    }

    public void updateTSDetail(final String request) {

//        headerRequest.put("Authorization", "Bearer " + tokenAuth);

        getInstance();
        Call<APIResultTSD> call = getService().updateTSDetail("Bearer " + tokenAuth, headerRequest, request);
        call.enqueue(new Callback<APIResultTSD>() {

            @Override
            public void onResponse(Call<APIResultTSD> call, Response<APIResultTSD> response) {
                if (response.isSuccessful()) {
                    courier.getResponse(response.body(), response.body().message);
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        courier.getResponse(null, jsonObject.getString("error"));
                    } catch (Exception e) {
                        courier.getResponse(null, response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<APIResultTSD> call, Throwable t) {
                courier.getResponse(null, "Fail to connect server\n" + t.toString());
                Log.d(TAG, "onFailure: " + t.toString());

            }
        });
    }

    public void addTSHeader(final String request) {

//        headerRequest.put("Authorization", "Bearer " + tokenAuth);

        getInstance();
        Call<APIResultTSH> call = getService().addTSHeader("Bearer " + tokenAuth, headerRequest, request);
        call.enqueue(new Callback<APIResultTSH>() {

            @Override
            public void onResponse(Call<APIResultTSH> call, Response<APIResultTSH> response) {
                if (response.isSuccessful()) {
                    courier.getResponse(response.body(), response.body().message);
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        courier.getResponse(null, jsonObject.getString("error"));
                    } catch (Exception e) {
                        courier.getResponse(null, response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<APIResultTSH> call, Throwable t) {
                courier.getResponse(null, "Fail to connect server\n" + t.toString());
                Log.d(TAG, "onFailure: " + t.toString());

            }
        });
    }

    public void updateTSHeader(final String request) {

//        headerRequest.put("Authorization", "Bearer " + tokenAuth);

        getInstance();
        Call<APIResultTSH> call = getService().updateTSHeader("Bearer " + tokenAuth, headerRequest, request);
        call.enqueue(new Callback<APIResultTSH>() {

            @Override
            public void onResponse(Call<APIResultTSH> call, Response<APIResultTSH> response) {
                if (response.isSuccessful()) {
                    courier.getResponse(response.body(), response.body().message);
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        courier.getResponse(null, jsonObject.getString("error"));
                    } catch (Exception e) {
                        courier.getResponse(null, response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<APIResultTSH> call, Throwable t) {
                courier.getResponse(null, "Fail to connect server\n" + t.toString());
                Log.d(TAG, "onFailure: " + t.toString());

            }
        });
    }

    public void getRowTSApv(String id_company) {

        getInstance();
        try {
            Call<APIResultApvTS> call = getService().getRowTSEmplApv("Bearer " + tokenAuth, id_company);
            call.enqueue(new Callback<APIResultApvTS>() {
                @Override
                public void onResponse(Call<APIResultApvTS> call, Response<APIResultApvTS> response) {
                    if (response.isSuccessful()) {
                        courier.getResponse(response.body(), response.body().message);
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            courier.getResponse(null, jsonObject.getString("error"));
                        } catch (Exception e) {
                            courier.getResponse(null, response.message());
                        }
                    }
                }

                @Override
                public void onFailure(Call<APIResultApvTS> call, Throwable t) {
                    courier.getResponse(null, "Fail to connect server\n" + t.toString());
                    Log.e("Throwable --- >", t.toString());
                }
            });
        } catch (Exception e) {
            courier.getResponse(null, e.toString());
        }


    }

    public void approveTimeS(final String request) {

//        headerRequest.put("Authorization", "Bearer " + tokenAuth);

        getInstance();
        Call<APIResult> call = getService().approvedTimeS("Bearer " + tokenAuth, headerRequest, request);
        call.enqueue(new Callback<APIResult>() {

            @Override
            public void onResponse(Call<APIResult> call, Response<APIResult> response) {
                if (response.isSuccessful()) {
                    courier.getResponse(response.body(), response.body().message);
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        courier.getResponse(null, jsonObject.getString("error"));
                    } catch (Exception e) {
                        courier.getResponse(null, response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<APIResult> call, Throwable t) {
                courier.getResponse(null, "Fail to connect server\n" + t.toString());
                Log.d(TAG, "onFailure: " + t.toString());

            }
        });
    }

    public void delTimeD(final String id_timeD) {

//        headerRequest.put("Authorization", "Bearer " + tokenAuth);

        getInstance();
        Call<BaseModel> call = getService().delTimeD("Bearer " + tokenAuth, id_timeD);
        call.enqueue(new Callback<BaseModel>() {

            @Override
            public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                if (response.isSuccessful()) {
                    courier.getResponse(response.body(), response.body().message);
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        courier.getResponse(null, jsonObject.getString("error"));
                    } catch (Exception e) {
                        courier.getResponse(null, response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseModel> call, Throwable t) {
                courier.getResponse(null, "Fail to connect server\n" + t.toString());
                Log.d(TAG, "onFailure: " + t.toString());

            }
        });
    }


//    public void getTypeLeave() {
//
//        getInstance();
//        try {
//            Call<APIResultParam> call = getService().getParam("Bearer " + tokenAuth,"TYPECUTI");
//
//            call.enqueue(new Callback<APIResultParam>() {
//                @Override
//                public void onResponse(Call<APIResultParam> call, Response<APIResultParam> response) {
//                    if (response.isSuccessful()) {
//                        courier.getResponse(response.body(), response.message());
//                    } else {
//                        try {
//                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
//                            courier.getResponse(null, jsonObject.getString("error"));
//                        } catch (Exception e) {
//                            courier.getResponse(null, response.message());
//                        }
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<APIResultParam> call, Throwable t) {
//                    courier.getResponse(null, "Fail to connect server\n"+t.toString());
//                }
//            });
//        } catch (Exception e) {
//            courier.getResponse(null, e.toString());
//        }
//
//
//    }

    //    public void getMasterEmployee(String company) {
//
//        getInstance();
//        try {
//            Call<APIResultMasterEmployee> call = getService().getMasterEmployee("Bearer " + tokenAuth, company);
//            call.enqueue(new Callback<APIResultMasterEmployee>() {
//                @Override
//                public void onResponse(Call<APIResultMasterEmployee> call, Response<APIResultMasterEmployee> response) {
//                    if (response.isSuccessful()) {
//                        courier.getResponse(response.body(), response.body().message);
//                    } else {
//                        try {
//                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
//                            courier.getResponse(null, jsonObject.getString("error"));
//                        } catch (Exception e) {
//                            courier.getResponse(null, response.message());
//                        }
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<APIResultMasterEmployee> call, Throwable t) {
//                    courier.getResponse(null, "Fail to connect server\n"+t.toString());
//                }
//            });
//        } catch (Exception e) {
//            courier.getResponse(null, e.toString());
//        }
//
//
//    }
    //    public void addEmployee(final String request) {
//
//        getInstance();
//        Call<APIResultEmployee> call = getService().addEmployee("Bearer " + tokenAuth, headerRequest, request);
//        call.enqueue(new Callback<APIResultEmployee>() {
//
//            @Override
//            public void onResponse(Call<APIResultEmployee> call, Response<APIResultEmployee> response) {
//                if (response.isSuccessful()) {
//                    courier.getResponse(response.body(), response.body().message);
//                } else {
//                    courier.getResponse(null,response.body().error);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<APIResultEmployee> call, Throwable t) {
//                courier.getResponse(null, "Fail to connect server\n" + t.toString());
//                Log.d(TAG, "onFailure: " + t.toString());
//
//            }
//        });
//    }
    //-----------------Parameter-----------------------------------------------
//    public void getCompanySize(String id_company) {
//
//        getInstance();
//        try {
//            Call<APIResultParam> call = getService().getParam("Bearer " + tokenAuth,"comsize",id_company);
//
//            call.enqueue(new Callback<APIResultParam>() {
//                @Override
//                public void onResponse(Call<APIResultParam> call, Response<APIResultParam> response) {
//                    if (response.isSuccessful()) {
//                        courier.getResponse(response.body(), response.message());
//                    } else {
//                        try {
//                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
//                            courier.getResponse(null, jsonObject.getString("error"));
//                        } catch (Exception e) {
//                            courier.getResponse(null, response.message());
//                        }
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<APIResultParam> call, Throwable t) {
//                    courier.getResponse(null, "Fail to connect server\n"+t.toString());
//                }
//            });
//        } catch (Exception e) {
//            courier.getResponse(null, e.toString());
//        }
//
//
//    }
    //    public void addCompany(final String request) {
//
//        getInstance();
//        Call<APIResultCompanyAdd> call = getService().addCompany("Bearer " + tokenAuth, headerRequest, request);
//        call.enqueue(new Callback<APIResultCompanyAdd>() {
//
//            @Override
//            public void onResponse(Call<APIResultCompanyAdd> call, Response<APIResultCompanyAdd> response) {
//                if (response.isSuccessful()) {
//                    courier.getResponse(response.body(), response.body().message);
//                } else {
//                    try {
//                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
//                        courier.getResponse(null, jsonObject.getString("error"));
//                    } catch (Exception e) {
//                        courier.getResponse(null, response.message());
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<APIResultCompanyAdd> call, Throwable t) {
//                courier.getResponse(null, "Fail to connect server\n" + t.toString());
//                Log.d(TAG, "onFailure: " + t.toString());
//
//            }
//        });
//    }
    //--------------------------------Reim-----------------------------------

}
