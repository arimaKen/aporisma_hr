package com.aporisma.aporisma_api;

import com.aporisma.aporisma_api.Model.Approval.APIResult;
import com.aporisma.aporisma_api.Model.Approval.getRow.APIResultApvTS;
import com.aporisma.aporisma_api.Model.Approval.getRow.APIResultRApvReim;
import com.aporisma.aporisma_api.Model.BaseModel;
import com.aporisma.aporisma_api.Model.Company.APIResultCompany;
import com.aporisma.aporisma_api.Model.Company.APIResultCompanyAdd;
import com.aporisma.aporisma_api.Model.Employee.APIResultEmployee;
import com.aporisma.aporisma_api.Model.Employee.APIResultGetAprvAccess;
import com.aporisma.aporisma_api.Model.Employee.APIResultGroupE;
import com.aporisma.aporisma_api.Model.Employee.APIResultMasterEmployee;
import com.aporisma.aporisma_api.Model.Notification.APIResultAnnounc;
import com.aporisma.aporisma_api.Model.Notification.APIResultNotification;
import com.aporisma.aporisma_api.Model.Notification.APIResultReadNotif;
import com.aporisma.aporisma_api.Model.Paid.APIResultPaidReim;
import com.aporisma.aporisma_api.Model.Param.APIResultParam;
import com.aporisma.aporisma_api.Model.Reimbursement.APIResultListReimHeader;
import com.aporisma.aporisma_api.Model.Reimbursement.APIResultReimDetail;
import com.aporisma.aporisma_api.Model.Reimbursement.APIResultReimHeader;
import com.aporisma.aporisma_api.Model.TimeSheet.APIResultTSD;
import com.aporisma.aporisma_api.Model.TimeSheet.APIResultTSH;
import com.aporisma.aporisma_api.Model.TimeSheet.APIResultTSHD;
import com.aporisma.aporisma_api.Model.TimeSheet.APIResultTSHRow;
import com.aporisma.aporisma_api.Model.Token.APIResultToken;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Service {

    @POST ( "token" )
    Call<APIResultToken> getToken (@HeaderMap Map<String, Object> header,
                                   @Body String request);

    @POST ( "login" )
    Call<APIResultEmployee> login (@Header ( "Authorization" ) String Token,
                                   @HeaderMap Map<String, Object> header,
                                   @Body String request);

    @GET ( "company" )
    Call<APIResultCompany> searchCompany (@Query ( "code" ) String code,
                                          @Header ( "Authorization" ) String Token);

    @POST ( "company/register" )
    Call<APIResultCompanyAdd> addCompany (@Header ( "Authorization" ) String Token,
                                          @HeaderMap Map<String, Object> header,
                                          @Body String request);

    @GET ( "parameter" )
    Call<APIResultParam> getParam (@Header ( "Authorization" ) String Token,
                                   @Query ( "code" ) String paramG,
                                   @Query ( "id_company" ) String compID);

    @GET ( "notification" )
    Call<APIResultNotification> getListNotif (@Header ( "Authorization" ) String Token,
                                              @Query ( "code" ) String id_employee);

    @GET ( "notification" )
    Call<APIResultReadNotif> readNotif (@Header ( "Authorization" ) String Token,
                                        @Query ( "read" ) String id_notif);

    @GET ( "notification" )
    Call<APIResultAnnounc> getAnnouncement (@Header ( "Authorization" ) String Token,
                                            @Query ( "ancmnt" ) String id_company);

    @POST("approval/apv")
    Call<APIResult> approvedSubmission (@Header ( "Authorization" ) String Token,
                                          @HeaderMap Map<String, Object> header,
                                          @Body String request);

    //-------------------------------------EMPLOYEE SERVICE-----------------------------------------//
    @GET ( "employee/approvalAccess" )
    Call<APIResultGetAprvAccess> getAccessApproval (@Header ( "Authorization" ) String Token,
                                                    @Query ( "id_company" ) String id_company,
                                                    @Query ( "id_employee" ) String id_employee);

    @GET ( "employee" )
    Call<APIResultMasterEmployee> getEmployeeData (@Header ( "Authorization" ) String Token,
                                                   @Query ( "code" ) String id_company,
                                                   @Query ( "idempl" ) String id_empl);

    @GET ( "employee" )
    Call<APIResultMasterEmployee> getMasterEmployee (@Header ( "Authorization" ) String Token,
                                                     @Query ( "code" ) String id_company);

    @GET ( "employee/group" )
    Call<APIResultGroupE> getGroupEmpl (@Header ( "Authorization" ) String Token,
                                        @Query ( "id_company" ) String id_company,
                                        @Query ( "code" ) String id_employee,
                                        @Query ( "tipe" ) String tipe);

    @POST ( "employee/register" )
    Call<APIResultEmployee> addEmployee (@Header ( "Authorization" ) String Token,
                                         @HeaderMap Map<String, Object> header,
                                         @Body String request);

    //----------------------------------------REIMBURSEMENT SERVICE---------------------------------//
    @GET ( "reimburse/rowHeader" )
    Call<APIResultListReimHeader> getReimHeader (@Header ( "Authorization" ) String Token,
                                                 @Query ( "code" ) String id_employee,
                                                 @Query ( "id_company" ) String id_company);

    @GET ( "reimburse/detailH" )
    Call<APIResultReimHeader> getReimHeaderD (@Header ( "Authorization" ) String Token,
                                              @Query ( "code" ) String id_reimH);

    @GET ( "reimburse/rowDetail" )
    Call<APIResultReimDetail> getReimDetail (@Header ( "Authorization" ) String Token,
                                             @Query ( "code" ) String id_employee);

    @GET ( "reimburse/paid" )
    Call<APIResultPaidReim> paidReim (@Header ( "Authorization" ) String Token,
                                      @Query ( "code" ) String idFormReim);

    @POST ( "reimburse/detail" )
    Call<APIResultReimDetail> addReimDetail (@Header ( "Authorization" ) String Token,
                                             @HeaderMap Map<String, Object> header,
                                             @Body String request);

    @GET ( "reimburse/delDetail" )
    Call<BaseModel> delReimD (@Header ( "Authorization" ) String Token,
                              @Query ( "code" ) String id_reim);

    @POST ( "reimburse/detailUpdate" )
    Call<APIResultReimDetail> updateReimDetail (@Header ( "Authorization" ) String Token,
                                                @HeaderMap Map<String, Object> header,
                                                @Body String request);

    @POST ( "reimburse/header" )
    Call<APIResultReimHeader> addReimHeader (@Header ( "Authorization" ) String Token,
                                             @HeaderMap Map<String, Object> header,
                                             @Body String request);

    @POST ( "reimburse/headerUpdate" )
    Call<APIResultReimHeader> updateReimHeader (@Header ( "Authorization" ) String Token,
                                                @HeaderMap Map<String, Object> header,
                                                @Body String request);

    @POST ( "reimburse/approved" )
    Call<APIResult> approvedReim (@Header ( "Authorization" ) String Token,
                                  @HeaderMap Map<String, Object> header,
                                  @Body String request);

    @GET ( "approval/rowApvRM" )
    Call<APIResultRApvReim> getRowReimEmplApv (@Header ( "Authorization" ) String Token,
                                               @Query ( "id_company" ) String id_company);

    //--------------------------------------TIMESHEET SERVICE--------------------------------------//
    @GET ( "timesheet/header" )
    Call<APIResultTSHRow> getTSHeader (@Header ( "Authorization" ) String Token,
                                       @Query ( "code" ) String id_employee);

    @GET ( "timesheet/details" )
    Call<APIResultTSD> getTSDetails (@Header ( "Authorization" ) String Token,
                                     @Query ( "code" ) String id_employee);

    @GET ( "timesheet/delDetail" )
    Call<BaseModel> delTimeD (@Header ( "Authorization" ) String Token,
                              @Query ( "code" ) String id_timeD);

    @GET ( "timesheet/headerD" )
    Call<APIResultTSHD> getTSHD (@Header ( "Authorization" ) String Token,
                                 @Query ( "code" ) String id_tsForm);

    @GET ( "approval/rowApvTS" )
    Call<APIResultApvTS> getRowTSEmplApv (@Header ( "Authorization" ) String Token,
                                          @Query ( "id_company" ) String id_company);

    @POST ( "timesheet/details" )
    Call<APIResultTSD> addTSDetails (@Header ( "Authorization" ) String Token,
                                     @HeaderMap Map<String, Object> header,
                                     @Body String request);

    @POST ( "timesheet/updateDetails" )
    Call<APIResultTSD> updateTSDetail (@Header ( "Authorization" ) String Token,
                                       @HeaderMap Map<String, Object> header,
                                       @Body String request);

    @POST ( "timesheet/header" )
    Call<APIResultTSH> addTSHeader (@Header ( "Authorization" ) String Token,
                                    @HeaderMap Map<String, Object> header,
                                    @Body String request);

    @POST ( "timesheet/updateHeader" )
    Call<APIResultTSH> updateTSHeader (@Header ( "Authorization" ) String Token,
                                       @HeaderMap Map<String, Object> header,
                                       @Body String request);

    @POST ( "timesheet/approved" )
    Call<APIResult> approvedTimeS (@Header ( "Authorization" ) String Token,
                                   @HeaderMap Map<String, Object> header,
                                   @Body String request);
}
