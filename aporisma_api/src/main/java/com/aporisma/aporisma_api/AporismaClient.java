package com.aporisma.aporisma_api;

import android.annotation.SuppressLint;
import android.util.Log;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class AporismaClient {

    public static String BASE_URL;

    private static AporismaClient instance;
    private static Retrofit retrofit;

    @SuppressLint("NewApi")
    private AporismaClient() {
        try {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addInterceptor(loggingInterceptor)
                    .writeTimeout(1, TimeUnit.MINUTES)
                    .readTimeout(1, TimeUnit.MINUTES)
                    .connectTimeout(1, TimeUnit.MINUTES);

        retrofit = new Retrofit.Builder().baseUrl("http://apiuat.mini-ess.com/api/")
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build()).build();
            Log.d("URL", BASE_URL);
        }catch (Exception e){
            Log.d("URL", e.toString());
        }
    }

    public static AporismaClient getInstance() {
        if (instance == null) {
            instance = new AporismaClient();
        }
        return instance;
    }

    public static Service getService() {
        return retrofit.create(Service.class);
    }

}

