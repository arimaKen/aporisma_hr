package com.aporisma.aporisma_api.Response;

public interface ResponseCourier {
    void getResponse(BaseResponse response, String message);
}
