package com.aporisma.aporisma_api.Model.Reimbursement.DataStatus;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class FormReimbursement {
    @SerializedName("id_form")
    @Expose
    public String idForm;
    @SerializedName("id_employee")
    @Expose
    public String idEmployee;
    @SerializedName("date_assign")
    @Expose
    public String dateAssign;
    @SerializedName("subject")
    @Expose
    public String subject;
    @SerializedName("nominal")
    @Expose
    public String nominal;
    @SerializedName("details")
    @Expose
    public List<Reimbursement> details = new ArrayList<>();
    @SerializedName("status")
    @Expose
    public String status;

}
