package com.aporisma.aporisma_api.Model.Employee.Data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataStatusEmployee {

    @SerializedName("id_employee")
    @Expose
    public String idEmployee;
    @SerializedName("NIK")
    @Expose
    public Object nIK;
    @SerializedName("nama_depan")
    @Expose
    public String namaDepan;
    @SerializedName("nama_belakang")
    @Expose
    public String namaBelakang;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("telpn")
    @Expose
    public String telpn;
    @SerializedName("posisi")
    @Expose
    public String posisi;
    @SerializedName("emplgroup_id")
    @Expose
    public String emplgroupId;
    @SerializedName("id_group")
    @Expose
    public Object idGroup;
    @SerializedName("username")
    @Expose
    public String username;
    @SerializedName("password")
    @Expose
    public String password;
    @SerializedName("id_company")
    @Expose
    public String idCompany;
    @SerializedName("approved")
    @Expose
    public String approved;
    @SerializedName("approval_user")
    @Expose
    public Object approvalUser;
    @SerializedName("approval_date")
    @Expose
    public Object approvalDate;
    @SerializedName("created_date")
    @Expose
    public String createdDate;
    @SerializedName("created_user")
    @Expose
    public String createdUser;
    @SerializedName("updated_user")
    @Expose
    public Object updatedUser;
    @SerializedName("updated_date")
    @Expose
    public Object updatedDate;
    @SerializedName("last_login")
    @Expose
    public Object lastLogin;
    @SerializedName("cookie")
    @Expose
    public String cookie;
    @SerializedName("photo")
    @Expose
    public String photo;
    @SerializedName("emplgroup_name")
    @Expose
    public String emplgroupName;
    public boolean pp;
}
