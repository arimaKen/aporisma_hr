package com.aporisma.aporisma_api.Model.Notification;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataStatus {

    @SerializedName("id_notif")
    @Expose
    public String idNotif;
    @SerializedName("subject")
    @Expose
    public String subject;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("is_read")
    @Expose
    public String isRead;
    @SerializedName("id_employee")
    @Expose
    public String idEmployee;
    @SerializedName("created_user")
    @Expose
    public String createdUser;
    @SerializedName("created_date")
    @Expose
    public String createdDate;
    @SerializedName("updated_date")
    @Expose
    public Object updatedDate;
    @SerializedName("updated_user")
    @Expose
    public Object updatedUser;

}
