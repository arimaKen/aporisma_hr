package com.aporisma.aporisma_api.Model.TimeSheet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class TimeSheetHD {
    @SerializedName("id_ts")
    @Expose
    public String idTs;
    @SerializedName("subject")
    @Expose
    public String subject;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("nama_depan")
    @Expose
    public String namaDepan;
    @SerializedName("nama_belakang")
    @Expose
    public String namaBelakang;
    @SerializedName("approval_tpl_name")
    @Expose
    public String approvalTplName;
    @SerializedName("created_date")
    @Expose
    public String createdDate;
    @SerializedName("posisi")
    @Expose
    public String posisi;


    @SerializedName("id_employee")
    @Expose
    public String idEmployee;
    @SerializedName("id_company")
    @Expose
    public String idCompany;

    @SerializedName("ts_no")
    @Expose
    public String tsNo;

    @SerializedName("is_delete")
    @Expose
    public String isDelete;
    @SerializedName("approved_user")
    @Expose
    public String approvedUser;
    @SerializedName("approved_date")
    @Expose
    public String approvedDate;
    @SerializedName("approved_desc")
    @Expose
    public String approvedDesc;

    @SerializedName("created_user")
    @Expose
    public String createdUser;
    @SerializedName("updated_date")
    @Expose
    public String updatedDate;
    @SerializedName("updated_user")
    @Expose
    public String updatedUser;

    @SerializedName("details")
    @Expose
    public List<TimeSheetDetail> details = new ArrayList<>();
}
