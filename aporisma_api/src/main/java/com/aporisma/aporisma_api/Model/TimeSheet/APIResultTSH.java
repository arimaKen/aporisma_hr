package com.aporisma.aporisma_api.Model.TimeSheet;

import com.aporisma.aporisma_api.Response.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class APIResultTSH extends BaseResponse {
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data_status")
    @Expose
    public TimeSheetHeader dataStatus;
}
