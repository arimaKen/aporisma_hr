package com.aporisma.aporisma_api.Model.Approval.getRow;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ApvReim {
    @SerializedName("id_form")
    @Expose
    public String idForm;
    @SerializedName("id_employee")
    @Expose
    public String idEmployee;
    @SerializedName("id_company")
    @Expose
    public String idCompany;
    @SerializedName("reimburse_no")
    @Expose
    public String reimburseNo;
    @SerializedName("date_assign")
    @Expose
    public Object dateAssign;
    @SerializedName("subject")
    @Expose
    public String subject;
    @SerializedName("nominal")
    @Expose
    public String nominal;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("is_paid")
    @Expose
    public String isPaid;
    @SerializedName("paid_user")
    @Expose
    public String paidUser;
    @SerializedName("paid_date")
    @Expose
    public String paidDate;
    @SerializedName("nama_employee")
    @Expose
    public String namaEmployee;
    @SerializedName("created_date")
    @Expose
    public String createdDate;
    @SerializedName("created_user")
    @Expose
    public String createdUser;
    @SerializedName("status_str")
    @Expose
    public String statusStr;
    @SerializedName("approval_id")
    @Expose
    public String approvalId;
    @SerializedName("approval_tpl_id")
    @Expose
    public String approvalTplId;
    @SerializedName("doc_id")
    @Expose
    public String docId;
    @SerializedName("doc_type")
    @Expose
    public String docType;
    @SerializedName("curr_step")
    @Expose
    public String currStep;
    @SerializedName("status_apv")
    @Expose
    public String statusApv;
    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("nama_approver")
    @Expose
    public String namaApprover;
    @SerializedName("canceled")
    @Expose
    public String canceled;
    @SerializedName("LastApproval")
    @Expose
    public String lastApproval;
}
