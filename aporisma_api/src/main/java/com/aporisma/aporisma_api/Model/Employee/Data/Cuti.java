package com.aporisma.aporisma_api.Model.Employee.Data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Cuti {
    @SerializedName("id_cuti")
    @Expose
    public Integer idCuti;
    @SerializedName("id_employee")
    @Expose
    public Integer idEmployee;
    @SerializedName("bulan")
    @Expose
    public String bulan;
    @SerializedName("jatah")
    @Expose
    public Integer jatah;
    @SerializedName("sisa")
    @Expose
    public Integer sisa;
    @SerializedName("historyCuti")
    @Expose
    public List<HistoryCuti> historyCuti = new ArrayList<>();

}
