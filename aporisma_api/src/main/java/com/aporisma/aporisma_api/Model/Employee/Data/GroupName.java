package com.aporisma.aporisma_api.Model.Employee.Data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GroupName {
    @SerializedName("approval_tpl_id")
    @Expose
    public String approvalTplId;
    @SerializedName("id_company")
    @Expose
    public String idCompany;
    @SerializedName("approval_tpl_name")
    @Expose
    public String approvalTplName;
    @SerializedName("approval_type")
    @Expose
    public String approvalType;
    @SerializedName("step")
    @Expose
    public String step;
    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("orig_user")
    @Expose
    public String origUser;
}
