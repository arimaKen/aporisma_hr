package com.aporisma.aporisma_api.Model.Param;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataStatus {
    @SerializedName("param_id")
    @Expose
    public Integer paramId;
    @SerializedName("paramgroup_id")
    @Expose
    public Integer paramgroupId;
    @SerializedName("param_code")
    @Expose
    public String paramCode;
    @SerializedName("param_value")
    @Expose
    public String paramValue;
    @SerializedName("display_order")
    @Expose
    public Integer displayOrder;
    @SerializedName("paramgroup_code")
    @Expose
    public String paramgroupCode;
    @SerializedName("paramgroup_name")
    @Expose
    public String paramgroupName;
    @SerializedName("approval")
    @Expose
    public String approval;

}
