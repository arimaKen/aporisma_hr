package com.aporisma.aporisma_api.Model.Employee;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HistoryCuti {
    @SerializedName("id_History")
    @Expose
    public Integer id_History;
    @SerializedName("id_cuti")
    @Expose
    public Integer id_cuti;
    @SerializedName("employee_name")
    @Expose
    public String employee_name;
    @SerializedName("type_cuti")
    @Expose
    public String type_cuti;
    @SerializedName("date_start")
    @Expose
    public String date_start;
    @SerializedName("date_assign")
    @Expose
    public String date_assign;
    @SerializedName("date_end")
    @Expose
    public String date_end;
    @SerializedName("note")
    @Expose
    public String note;
    @SerializedName("lampiran")
    @Expose
    public String lampiran;
    @SerializedName("verifikasi")
    @Expose
    public String verifikasi;
}
