package com.aporisma.aporisma_api.Model.Notification;

import com.aporisma.aporisma_api.Response.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class APIResultNotification extends BaseResponse {
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data_status")
    @Expose
    public List<DataStatus> dataStatus = null;
    @SerializedName("error")
    @Expose
    public String error;
}
