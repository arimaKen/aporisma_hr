package com.aporisma.aporisma_api.Model.TimeSheet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class TimeSheetHeader {
    @SerializedName("id_ts")
    @Expose
    public String idTs;
    @SerializedName("ts_no")
    @Expose
    public String tsNo;
    @SerializedName("id_employee")
    @Expose
    public String idEmployee;
    @SerializedName("id_company")
    @Expose
    public String idCompany;
    @SerializedName("subject")
    @Expose
    public String subject;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("nama_depan")
    @Expose
    public String namaDepan;
    @SerializedName("nama_belakang")
    @Expose
    public String namaBelakang;
    @SerializedName("created_date")
    @Expose
    public String createdDate;
    @SerializedName("created_user")
    @Expose
    public String createdUser;
    @SerializedName("approved_user")
    @Expose
    public String approvedUser;
    @SerializedName("approved_date")
    @Expose
    public String approvedDate;
    public List<TimeSheetDetail> details = new ArrayList<>();
}
