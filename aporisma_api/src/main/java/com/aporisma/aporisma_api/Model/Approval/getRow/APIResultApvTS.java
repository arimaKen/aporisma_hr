package com.aporisma.aporisma_api.Model.Approval.getRow;

import com.aporisma.aporisma_api.Response.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class APIResultApvTS extends BaseResponse {

    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data_status")
    @Expose
    public List<ApvTS> dataStatus = new ArrayList<>();
}
