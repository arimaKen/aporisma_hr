package com.aporisma.aporisma_api.Model.Company;

import com.aporisma.aporisma_api.Response.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataStatus extends BaseResponse  {
    @SerializedName("id_company")
    @Expose
    public Integer idCompany;
    @SerializedName("nama_company")
    @Expose
    public String namaCompany;
    @SerializedName("alamat_company")
    @Expose
    public String alamatCompany;
    @SerializedName("telpn")
    @Expose
    public String telpn;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("approved")
    @Expose
    public String approved;
    @SerializedName("company_size")
    @Expose
    public String companySize;
}
