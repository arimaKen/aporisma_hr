package com.aporisma.aporisma_api.Model.Approval;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class post {
    @SerializedName("id_doc")
    @Expose
    public Integer idDoc;
    @SerializedName("id_employee")
    @Expose
    public Integer idEmployee;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("createdUser")
    @Expose
    public String createdUser;
    @SerializedName("remark")
    @Expose
    public String remark;
    @SerializedName("doc_type")
    @Expose
    public String docType;
}
