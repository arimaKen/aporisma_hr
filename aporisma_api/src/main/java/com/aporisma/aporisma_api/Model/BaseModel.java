package com.aporisma.aporisma_api.Model;

import com.aporisma.aporisma_api.Response.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BaseModel extends BaseResponse {

    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("error")
    @Expose
    public String error;
    @SerializedName("data_status")
    @Expose
    public Boolean dataStatus;

}
