package com.aporisma.aporisma_api.Model.Reimbursement;

import com.aporisma.aporisma_api.Model.Reimbursement.DataStatus.Reimbursement;
import com.aporisma.aporisma_api.Response.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class APIResultSendReim extends BaseResponse {
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("error")
    @Expose
    public String error;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data_status")
    @Expose
    public Reimbursement dataStatus;
    @SerializedName("retCode")
    @Expose
    public Integer retCode;
    @SerializedName("token")
    @Expose
    public String token;
}
