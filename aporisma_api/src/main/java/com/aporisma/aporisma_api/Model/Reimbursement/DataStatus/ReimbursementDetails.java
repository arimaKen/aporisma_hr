package com.aporisma.aporisma_api.Model.Reimbursement.DataStatus;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReimbursementDetails {

    @SerializedName("id_reim")
    @Expose
    public Integer idReim;
    @SerializedName("id_employee")
    @Expose
    public Integer idEmployee;
    @SerializedName("nominal")
    @Expose
    public Integer nominal;
    @SerializedName("tanggal")
    @Expose
    public String tanggal;
    @SerializedName("id_tipe")
    @Expose
    public String tipe;
    @SerializedName("catatan")
    @Expose
    public String catatan;
    @SerializedName("pathLocal")
    @Expose
    public String pathLocal;
    @SerializedName("photo")
    @Expose
    public String photo;
    @SerializedName("photo_type")
    @Expose
    public String photoType;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("id_form")
    @Expose
    public String idForm;
    @SerializedName("param_code")
    @Expose
    public String tipeValue;
    @SerializedName("created_user")
    @Expose
    public String createdUser;
    @SerializedName("created_date")
    @Expose
    public String createdDate;
    @SerializedName("update_user")
    @Expose
    public String updateUser;
    public String selected;


}
