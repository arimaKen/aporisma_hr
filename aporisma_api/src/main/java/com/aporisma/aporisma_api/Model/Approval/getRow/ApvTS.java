package com.aporisma.aporisma_api.Model.Approval.getRow;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ApvTS {
    @SerializedName("id_ts")
    @Expose
    public String idTs;
    @SerializedName("id_employee")
    @Expose
    public String idEmployee;
    @SerializedName("id_company")
    @Expose
    public String idCompany;
    @SerializedName("ts_no")
    @Expose
    public String tsNo;
    @SerializedName("subject")
    @Expose
    public String subject;
    @SerializedName("is_delete")
    @Expose
    public String isDelete;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("nama_employee")
    @Expose
    public String namaEmployee;
    @SerializedName("created_date")
    @Expose
    public String createdDate;
    @SerializedName("created_user")
    @Expose
    public String createdUser;
    @SerializedName("status_str")
    @Expose
    public String statusStr;
    @SerializedName("approval_id")
    @Expose
    public String approvalId;
    @SerializedName("approval_tpl_id")
    @Expose
    public String approvalTplId;
    @SerializedName("doc_id")
    @Expose
    public String docId;
    @SerializedName("doc_type")
    @Expose
    public String docType;
    @SerializedName("curr_step")
    @Expose
    public String currStep;
    @SerializedName("status_apv")
    @Expose
    public String statusApv;
    @SerializedName("canceled")
    @Expose
    public String canceled;
    @SerializedName("LastApproval")
    @Expose
    public String lastApproval;
    @SerializedName("tgl_awal")
    @Expose
    public String tglAwal;
    @SerializedName("tgl_akhir")
    @Expose
    public String tglAkhir;
    @SerializedName("nama_approver")
    @Expose
    public String namaApprover;
    @SerializedName("user_id")
    @Expose
    public String userId;
}
