package com.aporisma.aporisma_api.Model.TimeSheet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TimeSheetDetail {
    @SerializedName("id_tsdetail")
    @Expose
    public String idTsdetail;
    @SerializedName("id_employee")
    @Expose
    public String idEmployee;
    @SerializedName("id_ts")
    @Expose
    public Object idTs;
    @SerializedName("date")
    @Expose
    public String date;
    @SerializedName("time_in")
    @Expose
    public Object timeIn;
    @SerializedName("time_out")
    @Expose
    public Object timeOut;
    @SerializedName("lokasi")
    @Expose
    public String lokasi;
    @SerializedName("tipe_pekerjaan")
    @Expose
    public String type;
    @SerializedName("customer")
    @Expose
    public String customer;
    @SerializedName("project")
    @Expose
    public String project;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("approval")
    @Expose
    public String approval;
    @SerializedName("approval_date")
    @Expose
    public String approvalDate;
    @SerializedName("created_date")
    @Expose
    public String createdDate;
    @SerializedName("created_user")
    @Expose
    public String createdUser;
    @SerializedName("updated_date")
    @Expose
    public String updatedDate;
    @SerializedName("updated_user")
    @Expose
    public String updatedUser;
    public String selected;

}
