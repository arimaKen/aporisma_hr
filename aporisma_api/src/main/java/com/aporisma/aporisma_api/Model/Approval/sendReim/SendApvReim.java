package com.aporisma.aporisma_api.Model.Approval.sendReim;

import java.util.ArrayList;
import java.util.List;

public class SendApvReim {
    public String id_form;
    public String id_employee;
    public String id_company;
    public String approved_user;
    public String approved_date;
    public String approved_desc;
    public String status;
    public List<Integer> details = new ArrayList<>();
}
