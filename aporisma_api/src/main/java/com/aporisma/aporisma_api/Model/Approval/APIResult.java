package com.aporisma.aporisma_api.Model.Approval;

import com.aporisma.aporisma_api.Response.BaseResponse;

public class APIResult extends BaseResponse {
    public String message;
    public String status;
    public String error;
}
