package com.aporisma.aporisma_api.Model.Employee;

import com.aporisma.aporisma_api.Model.Employee.Data.ApprovalType;
import com.aporisma.aporisma_api.Response.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class APIResultGetAprvAccess extends BaseResponse {
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data_status")
    @Expose
    public List<ApprovalType> dataStatus = new ArrayList<>();

}
