package com.aporisma.aporisma_api.Model.Paid;

import com.aporisma.aporisma_api.Response.BaseResponse;

public class APIResultPaidReim extends BaseResponse {
    public String message;
    public String data_status;
    public String error;
}
