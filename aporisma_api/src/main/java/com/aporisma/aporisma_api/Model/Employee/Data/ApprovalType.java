package com.aporisma.aporisma_api.Model.Employee.Data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ApprovalType {
    @SerializedName("approval_type")
    @Expose
    public String approvalType;
}
