package com.aporisma.aporisma_api.Model.Employee;

import com.aporisma.aporisma_api.Model.Employee.Data.DataStatusEmployee;
import com.aporisma.aporisma_api.Response.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class APIResultMasterEmployee extends BaseResponse {
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("error")
    @Expose
    public String error;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data_status")
    @Expose
    public List<DataStatusEmployee> dataStatus = new ArrayList<>();
    @SerializedName("retCode")
    @Expose
    public Integer retCode;
    @SerializedName("token")
    @Expose
    public String token;
}
