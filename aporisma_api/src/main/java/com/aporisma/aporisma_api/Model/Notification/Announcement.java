package com.aporisma.aporisma_api.Model.Notification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Announcement {
    @SerializedName("id_cmpny1")
    @Expose
    public String idCmpny1;
    @SerializedName("id_company")
    @Expose
    public String idCompany;
    @SerializedName("tipe_cmpny1")
    @Expose
    public String tipeCmpny1;
    @SerializedName("img")
    @Expose
    public Object img;
    @SerializedName("judul")
    @Expose
    public String judul;
    @SerializedName("isi")
    @Expose
    public String isi;
    @SerializedName("attachment")
    @Expose
    public Object attachment;
    @SerializedName("attachment_type")
    @Expose
    public String attachmentType;
    @SerializedName("attachment_name")
    @Expose
    public String attachmentName;
    @SerializedName("created_date")
    @Expose
    public String createdDate;
    @SerializedName("created_user")
    @Expose
    public String createdUser;
    @SerializedName("updated_date")
    @Expose
    public String updatedDate;
    @SerializedName("updated_user")
    @Expose
    public String updatedUser;
}
