package com.aporisma.aporisma_api.Model.Reimbursement.DataStatus;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ReimbursementHeader {
    @SerializedName("id_form")
    @Expose
    public String idForm;
    @SerializedName("id_employee")
    @Expose
    public String idEmployee;
    @SerializedName("id_company")
    @Expose
    public String idCompany;
    @SerializedName("date_assign")
    @Expose
    public String dateAssign;
    @SerializedName("subject")
    @Expose
    public String subject;
    @SerializedName("nominal")
    @Expose
    public String nominal;
    @SerializedName("details")
    @Expose
    public List<ReimbursementDetails> details = new ArrayList<>();
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("is_paid")
    @Expose
    public String isPaid;
    @SerializedName("created_user")
    @Expose
    public String createdUser;

    @SerializedName("approved_user")
    @Expose
    public String approvedUser;
    @SerializedName("approved_desc")
    @Expose
    public String approveDesc;
    @SerializedName("approved_date")
    @Expose
    public String approvedDate;
    @SerializedName("reimburse_no")
    @Expose
    public String reimburseNo;
    @SerializedName("created_date")
    @Expose
    public String createdDate;
    @SerializedName("nama_depan")
    @Expose
    public String namaDepan;
    @SerializedName("approval_tpl_name")
    @Expose
    public String approvalTplName;


}
