package com.aporisma.aporisma_api.Model.Employee;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DataStatusEmployee {

    @SerializedName("idKaryawan")
    @Expose
    public Integer idKaryawan;
    @SerializedName("username")
    @Expose
    public String username;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("fullname")
    @Expose
    public String fullname;
    @SerializedName("position")
    @Expose
    public String position;
    @SerializedName("userPermission")
    @Expose
    public String userPermission;
    @SerializedName("cuti")
    @Expose
    public ArrayList<Cuti> cuti = null;

}
