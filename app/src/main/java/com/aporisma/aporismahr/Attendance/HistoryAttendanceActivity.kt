package com.aporisma.aporismahr.Attendance

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.appcompat.widget.Toolbar
import com.aporisma.aporismahr.Helper.GettingLocationHelper
import com.aporisma.aporismahr.R
import com.aporisma.base_componen.BaseActivity
import kotlinx.android.synthetic.main.attendance_activity.*

class HistoryAttendanceActivity : BaseActivity() {

    var lastDate: Int = 0
    val mContext = this@HistoryAttendanceActivity

    @SuppressLint("SetTextI18n")
    override fun initCreate() {
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        GettingLocationHelper.getLocationWithCheckNetworkAndGPS(this@HistoryAttendanceActivity)
        lastDate = Integer.valueOf(GettingLocationHelper.dateOnly!!.toString()) - 7
        tvDateAttendance.text =
            "$lastDate ${GettingLocationHelper.monthOnly} - ${GettingLocationHelper.dateOnly} ${GettingLocationHelper.monthOnly}"
    }

    override fun getLayoutId(): Int = R.layout.attendance_activity

    override fun initDestroy() {
    }

    companion object {
        fun startThisActivity(ctx: Context) {
            val intent = Intent(ctx, HistoryAttendanceActivity::class.java)
            ctx.startActivity(intent)
        }
    }

    /*override fun onClick() {
        object : setOnClick(arrayOf(ivAdd)) {
            @SuppressLint("NewApi", "SetTextI18n")
            override fun doClick(view: View) {
                when (view.id) {
                    R.id.ivAdd -> {
                        AddAttendanceActivity.startThisActivity(this@HistoryAttendanceActivity)
                    }
                }
            }
        }
    }*/


}
