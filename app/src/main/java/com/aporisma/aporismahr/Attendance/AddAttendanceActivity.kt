@file:Suppress("DEPRECATION")

package com.aporisma.aporismahr.Attendance

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.aporisma.aporismahr.Helper.Dialog
import com.aporisma.aporismahr.Helper.GettingLocationHelper
import com.aporisma.aporismahr.Helper.GettingLocationHelper.getLocationWithCheckNetworkAndGPS
import com.aporisma.aporismahr.Helper.MovePictureToDirectory
import com.aporisma.aporismahr.Helper.getRealPathFromURI
import com.aporisma.aporismahr.Main.FullPreview
import com.aporisma.aporismahr.R
import com.aporisma.base_componen.BaseActivity
import com.aporisma.base_componen.setOnClick
import kotlinx.android.synthetic.main.attendance_detail_activity.*
import java.io.File


class AddAttendanceActivity : BaseActivity() {
    val mContext: Context = this@AddAttendanceActivity
    val activity: Activity = this@AddAttendanceActivity
    lateinit var viewActivity: View


    @SuppressLint("SetTextI18n")
    override fun initCreate() {
        viewActivity = window.decorView.rootView
        getLocationWithCheckNetworkAndGPS(mContext)
        tvTgl.text =  GettingLocationHelper.dateNow.toString()
        tvTime.text = GettingLocationHelper.time.toString()
        tvLocation.text = "${GettingLocationHelper.lat}\n${GettingLocationHelper.lon}"
        capturePhoto()
    }

    override fun getLayoutId(): Int = R.layout.attendance_detail_activity

    override fun initDestroy() {
    }

    override fun onClick() {
        object : setOnClick(arrayOf(ivFoto, btnSimpan)) {
            @SuppressLint("NewApi", "SetTextI18n")
            override fun doClick(view: View) {
                when (view.id) {
                    R.id.ivFoto -> {
                        capturePhoto()
                    }
                    R.id.btnSimpan -> {
                        Dialog.snackbar(mContext, viewActivity, "Attendance Saved")
                    }
                }
            }
        }
    }

    lateinit var imageUri: Uri
    private val takePicture = 115
    lateinit var imageurl: String

    @SuppressLint("SetTextI18n")
    override fun onBackPressed() {
        try {
            val dialogBuilder = android.app.AlertDialog.Builder(mContext)
            @SuppressLint("InflateParams") val dialogView =
                LayoutInflater.from(mContext).inflate(R.layout.dialog_choose, null)
            dialogBuilder.setView(dialogView)

            val tvMessage = dialogView.findViewById<TextView>(R.id.tvMessage)
            val tvTitle = dialogView.findViewById<TextView>(R.id.tvTitle)
            val btnOk = dialogView.findViewById<Button>(R.id.btnOk)
            val btnCancle = dialogView.findViewById<Button>(R.id.btnCancel)
            val b = dialogBuilder.create()
            tvTitle.text = "Confirmation"
            tvMessage.text = "Apakah anda yakin ingin membatalkan abensi ini ?"
            b.show()
            btnOk.setOnClickListener {
                val file = File(imageurl)
                if (file.exists()) {
                    file.delete()
                }
                b.dismiss()
                finish()

            }
            btnCancle.setOnClickListener {
                b.dismiss()
            }
        } catch (e: Exception) {
            Dialog.error(mContext, "ASCI-Ardi\nShowDialog", "error@BuildMessage\n$e\n${R.string.sendExceptionMessage}")
        }
    }

    private fun capturePhoto() {
        try {
            if (ivFoto.drawable == null) {
                val values = ContentValues()
                values.put(MediaStore.Images.Media.TITLE, "New Picture")
                values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera")
                imageUri = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)!!
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
                startActivityForResult(intent, takePicture)
            } else {
                openPreview()
            }
        } catch (e: Exception) {
            Toast.makeText(mContext, e.toString(), Toast.LENGTH_LONG).show()
            Log.e("error showingCamera", e.toString())
        }
    }

    private fun openPreview() {
        try {
            val dialogBuilder = AlertDialog.Builder(mContext)
            @SuppressLint("InflateParams") val dialogView =
                LayoutInflater.from(mContext).inflate(R.layout.dialog_preview, null)
            dialogBuilder.setView(dialogView)

            val ivPreview = dialogView.findViewById<ImageView>(R.id.ivPreview)
            val btnClose = dialogView.findViewById<Button>(R.id.btnClose)
            val btnChange = dialogView.findViewById<Button>(R.id.btnChange)
            val b = dialogBuilder.create()
            b.show()
            val localBitmap = BitmapFactory.decodeFile(imageurl)
            ivPreview.setImageBitmap(localBitmap)
            ivPreview.setOnClickListener {
                b.dismiss()
                FullPreview.startThisActivity(mContext, imageurl)
            }
            btnClose.setOnClickListener { b.dismiss() }
            btnChange.setOnClickListener {
                ivFoto.setImageDrawable(null)
                capturePhoto()
                b.dismiss()
            }
        } catch (e: Exception) {
            Dialog.error(mContext, "ASCI-Ardi\nAddAttendanceActivity", "error@openPreview\n$e\n${R.string.sendExceptionMessage}")

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (resultCode == RESULT_OK && requestCode == takePicture) {
                imageurl = getRealPathFromURI(activity, imageUri)
                MovePictureToDirectory.fileAttendance(mContext, imageurl, "sdcard/Aporisma/Picture/attenndance${GettingLocationHelper.datePhoto}.jpg", imageUri)
                imageurl = MovePictureToDirectory.fileAttendancePath
                val localBitmap = BitmapFactory.decodeFile(imageurl)
                ivFoto.setImageBitmap(localBitmap)
            }
        } catch (e: Exception) {
            Dialog.error(mContext, "ASCI-Ardi@Attendance", "ActivityResult:\n$e")
            Log.e("ERROR", e.toString())
        }
    }

    companion object {
        fun startThisActivity(ctx: Context) {
            val intent = Intent(ctx, AddAttendanceActivity::class.java)
            ctx.startActivity(intent)
        }
    }
}
