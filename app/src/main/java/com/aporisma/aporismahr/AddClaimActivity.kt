package com.aporisma.aporismahr

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.widget.Toolbar
import com.aporisma.aporisma_api.Model.Reimbursement.APIResultSendReim
import com.aporisma.aporisma_api.Model.Reimbursement.DataStatus.Reimbursement
import com.aporisma.aporisma_api.Model.Reimbursement.Type.APIResultTypeReim
import com.aporisma.aporisma_api.Repo.Repo
import com.aporisma.aporisma_api.Response.BaseResponse
import com.aporisma.aporisma_api.Response.ResponseCourier
import com.aporisma.aporismahr.Helper.DateHelper
import com.aporisma.aporismahr.Helper.Dialog.dismiss
import com.aporisma.aporismahr.Helper.Dialog.error
import com.aporisma.aporismahr.Helper.Dialog.loading
import com.aporisma.aporismahr.Helper.Dialog.succeddFinish
import com.aporisma.aporismahr.Helper.GetBase64.fromFilePath
import com.aporisma.aporismahr.Helper.getRealPathFromURI
import com.aporisma.base_componen.BaseActivity
import com.aporisma.base_componen.setOnClick
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_add_claim.*
import java.io.File


@Suppress("DEPRECATION")
class AddClaimActivity : BaseActivity(), ResponseCourier {
    override fun getResponse(response: BaseResponse<*>?, message: String?) {
        if (response is APIResultTypeReim) {
            for (i in 0 until response.dataStatus.size) {
                spinnerArray.add(response.dataStatus[i].type_reim)
            }
            insertSpinnerTipe()
        } else if (response is APIResultSendReim) {
            succeddFinish(mContext,mActivity,resources.getString(R.string.app_name),"$message")

        }
        dismiss()
    }

    val mActivity = this@AddClaimActivity
    var mContext = this@AddClaimActivity
    val repo = Repo(mContext, this@AddClaimActivity)
    val spinnerArray = ArrayList<String>()
    lateinit var detailReim: Reimbursement

    @SuppressLint("SetTextI18n")
    override fun initCreate() {
        val intent = intent
        val getExtraData = intent.extras
        if (getExtraData != null) {
            try {
                val stringExtra = getExtraData.get("data") as String?
                if (stringExtra != null) {
                    detailReim = Gson().fromJson(stringExtra, Reimbursement::class.java)
                    if (detailReim.pathLocal != null) {
                        imageurl = detailReim.pathLocal
                        val file = File(detailReim.pathLocal)
                        if (file.exists()) ivFoto.setImageDrawable(
                            Drawable.createFromPath(
                                detailReim.pathLocal
                            )
                        )
                        else error(
                            mContext,
                            resources.getString(R.string.app_name),
                            "Foto kemungkinan sudah terhapus dari memory internal"
                        )
                    }
                    etTotal.setText(detailReim.nominal.toString())
                    etDate.setText(detailReim.tanggal.split("T")[0])
                    for (i in 0 until spinnerArray.size) {
                        if (spinnerArray[i].equals(detailReim.tipe)) spType.setSelection(i)
                    }
                    etNote.setText(detailReim.catatan.toString())
                    btnAddKlaim.text = "UBAH"
                    btnAddKlaim.setCompoundDrawables(
                        mContext.resources.getDrawable(R.drawable.ic_edit),
                        null,
                        null,
                        null
                    )

                }
            } catch (e: java.lang.Exception) {
                error(mContext, resources.getString(R.string.app_name), e.toString())
            }
        }
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        toolbar.title = "Klaim Baru"
        toolbar.navigationIcon = resources.getDrawable(R.drawable.ic_back)
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener { finish() }
        etDate.showSoftInputOnFocus = false
        etTotal.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (etTotal.text.toString().matches(Regex("^0"))) etTotal.setText("")
            }

        })
        repo.getTypeClaim()
        loading(mContext, repo, "Sedang mendapatkan data")
    }

    override fun getLayoutId(): Int = R.layout.activity_add_claim

    override fun initDestroy() {
    }

    companion object {
        fun startThisActivity(ctx: Context, data: String?) {
            val intent = Intent(ctx, AddClaimActivity::class.java).putExtra("data", data)
            ctx.startActivity(intent)
        }
    }

    override fun onClick() {
        object : setOnClick(arrayOf(ivFoto, etDate, btnAddKlaim)) {
            @SuppressLint("NewApi", "SetTextI18n")
            override fun doClick(view: View) {
                when (view.id) {
                    R.id.ivFoto -> {
                        capturePhoto()
                    }
                    R.id.etDate -> {
                        DateHelper.datePicker(mContext, etDate)
                    }
                    R.id.btnAddKlaim -> {
                        if (btnAddKlaim.text.equals("TAMBAH")) {
                            val newReim =
                                Reimbursement()
                            newReim.idEmployee = 1
                            newReim.photo = fromFilePath(mContext, File(imageurl))
                            newReim.nominal = Integer.valueOf(etTotal.text.toString())
                            newReim.pathLocal = imageurl
                            newReim.tipe = spType.selectedItem.toString()
                            newReim.catatan = etNote.text.toString()
                            newReim.tanggal = etDate.text.toString()
                            newReim.status = "P"
                            repo.addReim(Gson().toJson(newReim))
                            loading(mContext, repo, "Sedang mengirim data klaim")
                        } else {
                            val newReim =
                                Reimbursement()
                            newReim.idReim = detailReim.idReim
                            newReim.idEmployee = 1
                            newReim.photo = fromFilePath(mContext, File(imageurl))
                            newReim.nominal = Integer.valueOf(etTotal.text.toString())
                            newReim.pathLocal = imageurl
                            newReim.tipe = spType.selectedItem.toString()
                            newReim.catatan = etNote.text.toString()
                            newReim.tanggal = etDate.text.toString()
                            newReim.status = "P"
                            repo.updateReim(Gson().toJson(newReim))
                            loading(mContext, repo, "Sedang mengupdate data klaim")
                        }
                    }
                }
            }
        }
    }

    lateinit var imageUri: Uri
    private val takePicture = 115
    private val insertImage = 116
    lateinit var imageurl: String
    private lateinit var bitmap: Bitmap

    private fun capturePhoto() {
        try {
            if (ivFoto.drawable == null) {
                chooseInsertImage()
            } else {
                openPreview()
            }
        } catch (e: Exception) {
            Toast.makeText(mContext, e.toString(), Toast.LENGTH_LONG).show()
            Log.e("error showingCamera", e.toString())
        }
    }

    @SuppressLint("SetTextI18n")
    fun chooseInsertImage() {
        try {
            val dialogBuilder = AlertDialog.Builder(mContext)
            @SuppressLint("InflateParams") val dialogView =
                LayoutInflater.from(mContext).inflate(R.layout.dialog_choose_image, null)
            dialogBuilder.setView(dialogView)

            val tvTitel = dialogView.findViewById<TextView>(R.id.tvTitel)
            val btnFromCamere = dialogView.findViewById<Button>(R.id.btnFromCamera)
            val btnFromGallery = dialogView.findViewById<Button>(R.id.btnFromGallery)
            val btnClose = dialogView.findViewById<Button>(R.id.btnClose)
            val b = dialogBuilder.create()
            tvTitel.text = "Insert Picture"
            b.setCanceledOnTouchOutside(false)
            b.show()

            val lp = WindowManager.LayoutParams()
            lp.copyFrom(b.window?.attributes)
            lp.gravity = Gravity.BOTTOM
            lp.width = WindowManager.LayoutParams.FILL_PARENT
            lp.windowAnimations = R.style.DialogAnimation
            b.window?.attributes = lp

            btnFromCamere.setOnClickListener {
                val values = ContentValues()
                values.put(MediaStore.Images.Media.TITLE, "New Picture")
                values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera")
                imageUri =
                    contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)!!
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
                startActivityForResult(intent, takePicture)
                b.dismiss()
            }
            btnFromGallery.setOnClickListener {
                val intent = Intent()
                intent.type = "fromBitmapImage/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(
                    Intent.createChooser(intent, "Select fromBitmapImage"),
                    insertImage
                )
                b.dismiss()
            }
            btnClose.setOnClickListener {
                b.dismiss()
            }

        } catch (e: Exception) {
            error(
                mContext,
                "ASCI-Ardi\nShowDialog",
                "error@BuildMessage\n$e\n${R.string.sendExceptionMessage}"
            )
        }
    }

    private fun openPreview() {
        try {
            val dialogBuilder = AlertDialog.Builder(mContext)
            @SuppressLint("InflateParams") val dialogView =
                LayoutInflater.from(mContext).inflate(R.layout.dialog_preview, null)
            dialogBuilder.setView(dialogView)
            val ivPreview = dialogView.findViewById<ImageView>(R.id.ivPreview)
            val btnUbah = dialogView.findViewById<Button>(R.id.btnChange)
            val btnClose = dialogView.findViewById<Button>(R.id.btnClose)
            val b = dialogBuilder.create()

            val lp = WindowManager.LayoutParams()
            lp.copyFrom(b.window?.attributes)
            lp.gravity = Gravity.BOTTOM
            lp.width = WindowManager.LayoutParams.FILL_PARENT
            lp.windowAnimations = R.style.DialogAnimation
            b.window?.attributes = lp
            b.show()
            ivPreview.setImageDrawable(Drawable.createFromPath(imageurl))
            btnUbah.setOnClickListener {
                b.dismiss()
                chooseInsertImage()
            }
            btnClose.setOnClickListener {
                b.dismiss()
            }
        } catch (e: Exception) {
            error(
                mContext,
                "ASCI-Ardi\nShowDialog",
                "error@openPreview\n$e\n${R.string.sendExceptionMessage}"
            )
//            Toast.makeText(mContext, e.toString(), Toast.LENGTH_LONG).show()

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (resultCode == RESULT_OK && requestCode == takePicture) {
                bitmap = MediaStore.Images.Media.getBitmap(contentResolver, imageUri)
                imageurl = getRealPathFromURI(this@AddClaimActivity, imageUri)
                val file = File(imageurl)
                if (file.exists()) {
                    tvTakePicture.visibility = GONE
                    ivFoto.setImageBitmap(bitmap)
                }
            } else if (resultCode == RESULT_OK && requestCode == insertImage && data != null) {
                imageUri = data.data!!
                bitmap = MediaStore.Images.Media.getBitmap(contentResolver, imageUri)
                imageurl = getRealPathFromURI(this@AddClaimActivity, imageUri)
                tvTakePicture.visibility = GONE
                ivFoto.setImageBitmap(bitmap)

            }
        } catch (e: Exception) {
            error(mContext, "ASCI-Ardi\nonActivityResult", e.toString())
            Log.e("ERROR", e.toString())
        }
    }

    private fun insertSpinnerTipe() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, spinnerArray)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        val sItems = findViewById<View>(R.id.spType) as Spinner
        sItems.adapter = adapter
    }

}
