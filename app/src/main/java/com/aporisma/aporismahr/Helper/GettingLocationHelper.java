package com.aporisma.aporismahr.Helper;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;
import androidx.core.app.ActivityCompat;
import com.aporisma.aporismahr.R;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Objects;

public class GettingLocationHelper {

    public static String date;
    public static String dateNow;
    public static String dateOnly;
    public static String monthOnly;
    public static String monthMini;
    public static String yearOnly;
    public static String time;
    public static String lat;
    public static String lon;
    public static String datePhoto;

    public static Location getLocationWithCheckNetworkAndGPS(Context mContext) {

        boolean isGpsEnabled;
        boolean isNetworkLocationEnabled;

        try {
            LocationManager lm = (LocationManager)
                    mContext.getSystemService(Context.LOCATION_SERVICE);
            assert lm != null;
            isGpsEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetworkLocationEnabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            Location networkLoacation = null, gpsLocation = null, finalLoc = null;
            if (isGpsEnabled)
                if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED) {

                    return null;
                }
            gpsLocation = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (isNetworkLocationEnabled)
                networkLoacation = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);


            date = String.valueOf(new SimpleDateFormat("MMM-yyyy" ,Locale.getDefault()).format(networkLoacation.getTime()));
            dateNow = String.valueOf(new SimpleDateFormat("dd-MMM-yyyy" ,Locale.getDefault()).format(networkLoacation.getTime()));
            datePhoto = String.valueOf(new SimpleDateFormat("ddMMyyyy" ,Locale.getDefault()).format(networkLoacation.getTime()));
            dateOnly = String.valueOf(new SimpleDateFormat("dd" ,Locale.getDefault()).format(networkLoacation.getTime()));
            monthOnly = String.valueOf(new SimpleDateFormat("MM" ,Locale.getDefault()).format(networkLoacation.getTime()));
            monthMini = String.valueOf(new SimpleDateFormat("MMM" ,Locale.getDefault()).format(networkLoacation.getTime()));
            yearOnly = String.valueOf(new SimpleDateFormat("yyyy" ,Locale.getDefault()).format(networkLoacation.getTime()));
            time = String.valueOf(new SimpleDateFormat("HH:mm" ,Locale.getDefault()).format(networkLoacation.getTime()));
            lat = String.valueOf(Objects.requireNonNull(networkLoacation).getLatitude());
            lon = String.valueOf(networkLoacation.getLongitude());

//            LocationDetails details = new LocationDetails();
//            assert networkLoacation != null;
//            details.latitude = String.valueOf(networkLoacation.getLatitude());
//            details.longitude = String.valueOf(networkLoacation.getLongitude());
//            details.altitude = (int) networkLoacation.getAltitude();
//            details.accuracy = (int) networkLoacation.getAccuracy();
//            details.time = String.valueOf(dateF.format(networkLoacation.getTime()));
//            PrefHelper.saveObjectToPref(mContext, "LocationDetails", details);

            if (gpsLocation != null) {

                //smaller the number more accurate result will
                if (gpsLocation.getAccuracy() > networkLoacation.getAccuracy())
                    return finalLoc = networkLoacation;
                else
                    return finalLoc = gpsLocation;

            } else {

                return finalLoc = networkLoacation;
            }

        } catch (Exception e) {
            Dialog.INSTANCE.error(mContext, "ASCI-Ardi\nLocationHelper",
                                        "error@getLocationWithCheckNetworkAndGPS\n" + e.toString() + "\n" +
                                        R.string.sendExceptionMessage);
            Log.e("Error Exception", e.toString());
        }
        return null;
    }

}
