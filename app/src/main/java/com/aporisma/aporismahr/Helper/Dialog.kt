@file:Suppress("DEPRECATION")

package com.aporisma.aporismahr.Helper

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.aporisma.aporisma_api.AporismaClient.BASE_URL
import com.aporisma.aporisma_api.Repo.Repo
import com.aporisma.aporismahr.Main.LoginActivity
import com.aporisma.aporismahr.Main.MainActivity
import com.aporisma.aporismahr.R
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.dialog_snackbar.view.*
import kotlin.system.exitProcess


object Dialog {

    lateinit var dismissDialog: AlertDialog

    @SuppressLint("ResourceAsColor", "SetTextI18n", "PrivateResource", "NewApi")
    fun error(mContext: Context, Title: String, Message: String) {
        try {
            val dialogBuilder = AlertDialog.Builder(mContext,R.style.DialogTheme)
            @SuppressLint("InflateParams") val dialogView =
                LayoutInflater.from(mContext).inflate(R.layout.dialog_message, null)
            dialogBuilder.setView(dialogView)

            val etSetting = dialogView.findViewById<EditText>(R.id.etSetting)
            val tvSetting = dialogView.findViewById<TextView>(R.id.tvSetting)
            val tvSettingTitle = dialogView.findViewById<TextView>(R.id.tvSettingTitle)
            val btnOK = dialogView.findViewById<Button>(R.id.btnOK)
            val btnCancel = dialogView.findViewById<Button>(R.id.btnCancel)
            val b = dialogBuilder.create()
            b.show()
            b.setCanceledOnTouchOutside(false)
            b.setCancelable(false)


            val lp = WindowManager.LayoutParams()
            lp.copyFrom(b.window?.attributes)
            lp.width = WindowManager.LayoutParams.MATCH_PARENT
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT
            lp.windowAnimations = R.style.DialogAnimation
            lp.gravity = Gravity.BOTTOM

            b.window?.attributes = lp

            tvSetting.textAlignment = View.TEXT_ALIGNMENT_CENTER
            etSetting.visibility = View.GONE
            btnOK.visibility = View.GONE
            tvSetting.text = Message
            tvSettingTitle.text = Title
            btnCancel.text = "OK"
            btnCancel.setOnClickListener { b.dismiss() }
        } catch (e: Exception) {
            error(mContext, "ASCI-Ardi\nShowDialog", "error@BuildMessage\n$e\n${R.string.sendExceptionMessage}")
        }
    }

    @SuppressLint("ResourceAsColor", "SetTextI18n", "PrivateResource", "NewApi")
    fun errorFinish(mContext: Context,mActivity: Activity ,Message: String) {
        try {
            val dialogBuilder = AlertDialog.Builder(mContext,R.style.DialogTheme)
            @SuppressLint("InflateParams") val dialogView =
                LayoutInflater.from(mContext).inflate(R.layout.dialog_message, null)
            dialogBuilder.setView(dialogView)

            val etSetting = dialogView.findViewById<EditText>(R.id.etSetting)
            val tvSetting = dialogView.findViewById<TextView>(R.id.tvSetting)
            val tvSettingTitle = dialogView.findViewById<TextView>(R.id.tvSettingTitle)
            val btnOK = dialogView.findViewById<Button>(R.id.btnOK)
            val btnCancel = dialogView.findViewById<Button>(R.id.btnCancel)
            val b = dialogBuilder.create()
            b.show()
            b.setCanceledOnTouchOutside(false)
            b.setCancelable(false)


            val lp = WindowManager.LayoutParams()
            lp.copyFrom(b.window?.attributes)
            lp.width = WindowManager.LayoutParams.MATCH_PARENT
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT
            lp.windowAnimations = R.style.DialogAnimation
            lp.gravity = Gravity.BOTTOM

            b.window?.attributes = lp

            tvSetting.textAlignment = View.TEXT_ALIGNMENT_CENTER
            etSetting.visibility = View.GONE
            btnOK.visibility = View.GONE
            tvSetting.text = Message
            tvSettingTitle.text = mContext.resources.getString(R.string.app_name)
            btnCancel.text = "OK"
            btnCancel.setOnClickListener {
                b.dismiss()
                mActivity.finish()
            }
        } catch (e: Exception) {
            error(mContext, "ASCI-Ardi\nShowDialog", "error@BuildMessage\n$e\n${R.string.sendExceptionMessage}")
        }
    }

    @SuppressLint("SetTextI18n")
    fun login(mContext: Context,mActivity: Activity , Message: String) {
        try {
            val dialogBuilder = AlertDialog.Builder(mContext,R.style.DialogTheme)
            @SuppressLint("InflateParams") val dialogView =
                LayoutInflater.from(mContext).inflate(R.layout.dialog_message, null)
            dialogBuilder.setView(dialogView)

            val etSetting = dialogView.findViewById<EditText>(R.id.etSetting)
            val tvSetting = dialogView.findViewById<TextView>(R.id.tvSetting)
            val tvSettingTitle = dialogView.findViewById<TextView>(R.id.tvSettingTitle)
            val btnOK = dialogView.findViewById<Button>(R.id.btnOK)
            val btnCancel = dialogView.findViewById<Button>(R.id.btnCancel)
            val b = dialogBuilder.create()
            b.show()
            b.setCanceledOnTouchOutside(false)

            val lp = WindowManager.LayoutParams()
            lp.copyFrom(b.window?.attributes)
            lp.width = WindowManager.LayoutParams.MATCH_PARENT
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT
            lp.windowAnimations = R.style.DialogAnimation
            lp.gravity = Gravity.BOTTOM

            b.window?.attributes = lp

            tvSetting.textAlignment = View.TEXT_ALIGNMENT_CENTER
            etSetting.visibility = View.GONE
            btnCancel.visibility = View.GONE
            tvSetting.text = Message
            tvSettingTitle.text = mContext.resources.getString(R.string.app_name)
            btnOK.text = "OK"
            btnOK.setOnClickListener {
                b.dismiss()
                MainActivity.startThisActivity(mContext)
                mActivity.finish()
            }
        } catch (e: Exception) {
            error(mContext, "ASCI-Ardi\nShowDialog", "error@BuildMessage\n$e\n${R.string.sendExceptionMessage}")
        }
    }

    @SuppressLint("SetTextI18n")
    fun finish(mContext: Context,  Message: String) {
        try {
            val dialogBuilder = AlertDialog.Builder(mContext,R.style.DialogTheme)
            @SuppressLint("InflateParams") val dialogView =
                LayoutInflater.from(mContext).inflate(R.layout.dialog_message, null)
            dialogBuilder.setView(dialogView)

            val etSetting = dialogView.findViewById<EditText>(R.id.etSetting)
            val tvSetting = dialogView.findViewById<TextView>(R.id.tvSetting)
            val tvSettingTitle = dialogView.findViewById<TextView>(R.id.tvSettingTitle)
            val btnOK = dialogView.findViewById<Button>(R.id.btnOK)
            val btnCancel = dialogView.findViewById<Button>(R.id.btnCancel)
            val b = dialogBuilder.create()
            b.show()
            b.setCanceledOnTouchOutside(false)
            b.setCancelable(false)

            val lp = WindowManager.LayoutParams()
            lp.copyFrom(b.window?.attributes)
            lp.width = WindowManager.LayoutParams.MATCH_PARENT
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT
            lp.windowAnimations = R.style.DialogAnimation
            lp.gravity = Gravity.BOTTOM

            b.window?.attributes = lp

            tvSetting.textAlignment = View.TEXT_ALIGNMENT_CENTER
            etSetting.visibility = View.GONE
            btnOK.visibility = View.GONE
            tvSetting.text = Message
            tvSettingTitle.text = mContext.resources.getString(R.string.app_name)
            btnCancel.text = "OK"
            btnCancel.setOnClickListener {
                b.dismiss()
                exitProcess(-1)
            }
        } catch (e: Exception) {
            error(mContext, "ASCI-Ardi\nShowDialog", "error@BuildMessage\n$e\n${R.string.sendExceptionMessage}")
        }
    }

    @SuppressLint("SetTextI18n")
    fun exit(mContext: Context, Message: String) {
        try {
            val dialogBuilder = AlertDialog.Builder(mContext,R.style.DialogTheme)
            @SuppressLint("InflateParams") val dialogView =
                LayoutInflater.from(mContext).inflate(R.layout.dialog_message, null)
            dialogBuilder.setView(dialogView)

            val etSetting = dialogView.findViewById<EditText>(R.id.etSetting)
            val tvSetting = dialogView.findViewById<TextView>(R.id.tvSetting)
            val tvSettingTitle = dialogView.findViewById<TextView>(R.id.tvSettingTitle)
            val btnOK = dialogView.findViewById<Button>(R.id.btnOK)
            val btnCancel = dialogView.findViewById<Button>(R.id.btnCancel)
            val b = dialogBuilder.create()
            b.show()
            b.setCanceledOnTouchOutside(false)
            b.setCancelable(false)

            val lp = WindowManager.LayoutParams()
            lp.copyFrom(b.window?.attributes)
            lp.width = WindowManager.LayoutParams.MATCH_PARENT
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT
            lp.windowAnimations = R.style.DialogAnimation
            lp.gravity = Gravity.BOTTOM

            b.window?.attributes = lp

            tvSetting.textAlignment = View.TEXT_ALIGNMENT_CENTER
            etSetting.visibility = View.GONE
            tvSetting.text = Message
            tvSettingTitle.text = mContext.resources.getString(R.string.app_name)
            btnCancel.text = "Cancel"
            btnOK.text = "Exit"
            btnCancel.setOnClickListener {
                b.dismiss()
            }
            btnOK.setOnClickListener {
                b.dismiss()
                exitProcess(-1)
            }
        } catch (e: Exception) {
            error(mContext, "ASCI-Ardi\nShowDialog", "error@BuildMessage\n$e\n${R.string.sendExceptionMessage}")
        }
    }

    fun logout(mContext: Context,mActivity: Activity ,Message: String) {
        try {
            val dialogBuilder = AlertDialog.Builder(mContext,R.style.DialogTheme)
            @SuppressLint("InflateParams") val dialogView =
                LayoutInflater.from(mContext).inflate(R.layout.dialog_message, null)
            dialogBuilder.setView(dialogView)

            val etSetting = dialogView.findViewById<EditText>(R.id.etSetting)
            val tvSetting = dialogView.findViewById<TextView>(R.id.tvSetting)
            val tvSettingTitle = dialogView.findViewById<TextView>(R.id.tvSettingTitle)
            val btnOK = dialogView.findViewById<Button>(R.id.btnOK)
            val btnCancel = dialogView.findViewById<Button>(R.id.btnCancel)
            val b = dialogBuilder.create()
            b.show()
            b.setCanceledOnTouchOutside(false)
            b.setCancelable(false)

            val lp = WindowManager.LayoutParams()
            lp.copyFrom(b.window?.attributes)
            lp.width = WindowManager.LayoutParams.MATCH_PARENT
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT
            lp.windowAnimations = R.style.DialogAnimation
            lp.gravity = Gravity.BOTTOM

            b.window?.attributes = lp

            tvSetting.textAlignment = View.TEXT_ALIGNMENT_CENTER
            etSetting.visibility = View.GONE
            tvSetting.text = Message
            tvSettingTitle.text = mContext.resources.getString(R.string.app_name)
            btnCancel.text = "Cancel"
            btnOK.text = "Logout"
            btnCancel.setOnClickListener {
                b.dismiss()
            }
            btnOK.setOnClickListener {
                b.dismiss()
                mActivity.finish()
                LoginActivity.startThisActivity(mContext,null)
                PrefHelper.remove(mContext,"swReminder")
            }
        } catch (e: Exception) {
            error(mContext, "ASCI-Ardi\nShowDialog", "error@BuildMessage\n$e\n${R.string.sendExceptionMessage}")
        }
    }

    fun succedd(mContext: Context, titel: String, Message: String) {
        try {
            val dialogBuilder = AlertDialog.Builder(mContext,R.style.DialogTheme)
            @SuppressLint("InflateParams") val dialogView =
                LayoutInflater.from(mContext).inflate(R.layout.dialog_message, null)
            dialogBuilder.setView(dialogView)

            val etSetting = dialogView.findViewById<EditText>(R.id.etSetting)
            val tvSetting = dialogView.findViewById<TextView>(R.id.tvSetting)
            val tvSettingTitle = dialogView.findViewById<TextView>(R.id.tvSettingTitle)
            val btnOK = dialogView.findViewById<Button>(R.id.btnOK)
            val btnCancel = dialogView.findViewById<Button>(R.id.btnCancel)
            val b = dialogBuilder.create()
            b.show()
            b.setCanceledOnTouchOutside(false)
            b.setCancelable(false)


            val lp = WindowManager.LayoutParams()
            lp.copyFrom(b.window?.attributes)
            lp.width = WindowManager.LayoutParams.MATCH_PARENT
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT
            lp.windowAnimations = R.style.DialogAnimation
            lp.gravity = Gravity.BOTTOM

            b.window?.attributes = lp

            tvSetting.textAlignment = View.TEXT_ALIGNMENT_CENTER
            etSetting.visibility = View.GONE
            btnCancel.visibility = View.GONE
            tvSetting.text = Message
            tvSettingTitle.text =titel
            btnOK.text = "OK"
            btnOK.setOnClickListener {
                b.dismiss()
            }
        } catch (e: Exception) {
            error(mContext, "ASCI-Ardi\nShowDialog", "error@BuildMessage\n$e\n${R.string.sendExceptionMessage}")
        }
    }

    fun succeddFinish(mContext: Context, mActivity: Activity, Message: String) {
        try {
            val dialogBuilder = AlertDialog.Builder(mContext,R.style.DialogTheme)
            @SuppressLint("InflateParams") val dialogView =
                LayoutInflater.from(mContext).inflate(R.layout.dialog_message, null)
            dialogBuilder.setView(dialogView)

            val etSetting = dialogView.findViewById<EditText>(R.id.etSetting)
            val tvSetting = dialogView.findViewById<TextView>(R.id.tvSetting)
            val tvSettingTitle = dialogView.findViewById<TextView>(R.id.tvSettingTitle)
            val btnOK = dialogView.findViewById<Button>(R.id.btnOK)
            val btnCancel = dialogView.findViewById<Button>(R.id.btnCancel)
            val b = dialogBuilder.create()
            b.show()
            b.setCanceledOnTouchOutside(false)
            b.setCancelable(false)


            val lp = WindowManager.LayoutParams()
            lp.copyFrom(b.window?.attributes)
            lp.width = WindowManager.LayoutParams.MATCH_PARENT
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT
            lp.windowAnimations = R.style.DialogAnimation
            lp.gravity = Gravity.BOTTOM

            b.window?.attributes = lp

            tvSetting.textAlignment = View.TEXT_ALIGNMENT_CENTER
            etSetting.visibility = View.GONE
            btnCancel.visibility = View.GONE
            tvSetting.text = Message
            tvSettingTitle.text = mContext.resources.getString(R.string.app_name)
            btnOK.text = "OK"
            btnOK.setOnClickListener {
                b.dismiss()
                mActivity.finish()
            }
        } catch (e: Exception) {
            error(mContext, "ASCI-Ardi\nShowDialog", "error@BuildMessage\n$e\n${R.string.sendExceptionMessage}")
        }
    }

    fun settingServer(mContext: Context, Message: String) {
        try {
            val dialogBuilder = AlertDialog.Builder(mContext,R.style.DialogTheme)
            @SuppressLint("InflateParams") val dialogView =
                LayoutInflater.from(mContext).inflate(R.layout.dialog_setting, null)
            dialogBuilder.setView(dialogView)

            val etSetting = dialogView.findViewById<EditText>(R.id.etSetting)
            val tvSetting = dialogView.findViewById<TextView>(R.id.tvSetting)
            val tvSettingTitle = dialogView.findViewById<TextView>(R.id.tvSettingTitle)
            val btnOK = dialogView.findViewById<Button>(R.id.btnOK)
            val btnCancel = dialogView.findViewById<Button>(R.id.btnCancel)
            val b = dialogBuilder.create()
            b.show()
            b.setCanceledOnTouchOutside(false)
            b.setCancelable(false)

            val lp = WindowManager.LayoutParams()
            lp.copyFrom(b.window?.attributes)
            lp.width = WindowManager.LayoutParams.MATCH_PARENT
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT
            lp.windowAnimations = R.style.DialogAnimation
            lp.gravity = Gravity.TOP

            b.window?.attributes = lp

            tvSetting.textAlignment = View.TEXT_ALIGNMENT_CENTER
            tvSetting.text = Message
            tvSettingTitle.text = mContext.resources.getString(R.string.app_name)
            btnOK.text = "OK"
            if (PrefHelper.getPref(mContext,"IPserver") != null) etSetting.setText(PrefHelper.getPref(mContext,"IPserver"))
            btnOK.setOnClickListener {
                BASE_URL = etSetting.text.toString()
                PrefHelper.saveToPref(mContext, "IPserver", etSetting.text.toString())
                b.dismiss()
                finish(mContext, "Silakan restart")
            }
            btnCancel.setOnClickListener{b.dismiss()}
        } catch (e: Exception) {
            error(mContext, "ASCI-Ardi\nShowDialog", "error@BuildMessage\n$e\n${R.string.sendExceptionMessage}")
        }
    }

    @SuppressLint("SetTextI18n")
    fun loading(mContext: Context, repo: Repo, mssg: String) {
        try {
            val dialogBuilder = AlertDialog.Builder(mContext,R.style.DialogTheme)
            @SuppressLint("InflateParams") val dialogView =
                LayoutInflater.from(mContext).inflate(R.layout.dialog_loading, null)
            dialogBuilder.setView(dialogView)
            val tvMessage = dialogView.findViewById<TextView>(R.id.tvMessage)
            val btnCancel = dialogView.findViewById<Button>(R.id.btnCancel)
            tvMessage.text = mssg

            val b = dialogBuilder.create()
            b.show()
            b.setCanceledOnTouchOutside(false)
            b.setCancelable(false)

            val lp = WindowManager.LayoutParams()
            lp.copyFrom(b.window?.attributes)
            lp.width = WindowManager.LayoutParams.MATCH_PARENT
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT
            lp.windowAnimations = R.style.DialogAnimation
            lp.gravity = Gravity.BOTTOM

            b.window?.attributes = lp
            dismissDialog = b

            btnCancel.setOnClickListener {
                repo.cancelRequest()
            }

        } catch (e: Exception) {
            error(mContext, "ASCI-Ardi\nShowDialog", "error@BuildMessage\n$e\n${R.string.sendExceptionMessage}")
        }
    }

    fun dismiss() {
        dismissDialog.dismiss()
    }

    @SuppressLint("SetTextI18n", "InflateParams")
    fun snackbar(mContext: Context, view: View, message: String) {
        val snackbar = Snackbar.make(view, "", Snackbar.LENGTH_LONG)
/*        snackbar.setAction("Show") {
            val alertDialog = AlertDialog.Builder(mContext).create()
            alertDialog.setMessage("Hello custom snackbar.")
            alertDialog.show()
        }*/

        // Get the view object.
        val snackbarView = snackbar.view as Snackbar.SnackbarLayout
        snackbarView.setBackgroundColor(mContext.resources.getColor(android.R.color.transparent))

        // Get custom view from external layout xml file.
        val customView = LayoutInflater.from(mContext).inflate(R.layout.dialog_snackbar, null)
        customView.tvSnacBar.text = message
        snackbarView.addView(customView, 0)

        snackbar.show()
    }

}
