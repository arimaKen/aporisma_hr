package com.aporisma.aporismahr.Helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Base64;

import com.aporisma.aporismahr.R;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.util.Base64.DEFAULT;
import static android.util.Base64.encodeToString;

@SuppressWarnings("ALL")
public class GetBase64 {

    public static String Base64String;

    public static String fromBitmapImage(Context context, Bitmap bm) {
        try {
            int H = bm.getHeight() / 2;
            int W = bm.getWidth() / 2;
            ByteArrayOutputStream ba = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 50, ba);
//            bm.createScaledBitmap(bm,W,H,true);
            byte[] by = ba.toByteArray();
            Base64String = encodeToString(by, DEFAULT);
        } catch (Exception e) {
            Dialog.INSTANCE.error(context, "ASCI-Ardi\nbase64",
                    "error@decodePicture\n" + e.toString() + R.string.sendExceptionMessage);
        }
        return Base64String;
    }

    public static String fromFilePath(Context context, File yourFile) {
        int size = (int) yourFile.length();
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(yourFile));
            buf.read(bytes, 0, bytes.length);
            buf.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            Dialog.INSTANCE.error(context, "ASCI-Ardi\nbase64",
                    "error@fromFilePath\n" + e.toString() + "\n" +
                            R.string.sendExceptionMessage);
            e.printStackTrace();
        }

        return Base64.encodeToString(bytes, Base64.NO_WRAP);
    }

    public static String parseBase64(String base64) {

        try {
            Pattern pattern = Pattern.compile("((?<=base64,).*\\s*)", Pattern.DOTALL | Pattern.MULTILINE);
            Matcher matcher = pattern.matcher(base64);
            if (matcher.find()) {
                return matcher.group().toString();
            } else {
                return "";
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return "";
    }

}
