package com.aporisma.aporismahr.Helper;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.text.TextUtils;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateHelper {
    public static String DATE_FORMAT_PICKER = "dd MMM yyyy";
    public static String DATE_FORMAT_DEFAULT = DATE_FORMAT_PICKER;

    public static String DATE_FORMAT_DB = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static String DATE_FORMAT_DB2 = "yyyy/MM/dd";
    public static String DATE_FORMAT_UI = "d MMMM yyyy";
    public static String DATE_FORMAT_UI2 = "d/M/yyyy";
    public static String DATE_FORMAT_UI3 = "d-M-yyyy";
    public static String DATE_FORMAT_JAM = "HH:mm";
    public static String DATE_FORMAT_UPLOAD = "yyyy-MM-dd";
    public static String DATE_FORMAT_UI_HARI = "EEEE, dd MMMM yyyy";

    public static String DATE_DAY_FORMAT_1 = "EE";
    public static String DATE_DAY_FORMAT_2 = "EEEE";

    public static void datePicker(Context context, final TextView textView) {
        final int year, month, day;
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dpd = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
                textView.setText(cekdate(selectedDay, selectedMonth, selectedYear));
            }
        }, year, month, day);
        dpd.show();
    }

    public static void datePicker(Context context, final EditText editText, final EditText startDate) {
        final int year, month, day;
        final Calendar c = Calendar.getInstance();

        String[] StartDate = startDate.getText().toString().split("-");

        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dpd = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
                editText.setText(cekdate(selectedDay, selectedMonth, selectedYear));
            }
        }, year, month, day);
        dpd.getDatePicker().setMaxDate(c.getTimeInMillis());
        dpd.show();
    }

    public static void datePicker(Context context, final EditText editText, final String is_backdate) {
        final int year, month, day;
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        if (is_backdate.equals("N")) {
            DatePickerDialog dpd = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
                    editText.setText(cekdate(selectedDay, selectedMonth, selectedYear));
                }
            }, year, month, day);
            dpd.getDatePicker().setMinDate(c.getTimeInMillis());
            dpd.show();
        } else if (is_backdate.equals("Y")){
            DatePickerDialog dpd = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
                    editText.setText(cekdate(selectedDay, selectedMonth, selectedYear));
                }
            }, year, month, day);
            dpd.getDatePicker().setMaxDate(c.getTimeInMillis());
            dpd.show();
        } else {
            DatePickerDialog dpd = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
                    editText.setText(cekdate(selectedDay, selectedMonth, selectedYear));
                }
            }, year, month, day);
            dpd.show();
        }
    }

    public static void datePicker(Context context, final EditText editText) {
        final int year, month, day;
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dpd = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
                editText.setText(cekdate(selectedDay, selectedMonth, selectedYear));
            }
        }, year, month, day);
        dpd.show();
    }

    public static void autoDateTime(Context context, final EditText editText) {
        SimpleDateFormat dateF = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        SimpleDateFormat timeF = new SimpleDateFormat("HH:mm", Locale.getDefault());
        String date = dateF.format(Calendar.getInstance().getTime());
        String time = timeF.format(Calendar.getInstance().getTime());

        editText.setText(date);
    }

    public static void autoDateTime(Context context, String String) {
        SimpleDateFormat dateF = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        SimpleDateFormat timeF = new SimpleDateFormat("HH:mm", Locale.getDefault());
        java.lang.String date = dateF.format(Calendar.getInstance().getTime());
        java.lang.String time = timeF.format(Calendar.getInstance().getTime());

        String = date;
    }


    @SuppressLint("SimpleDateFormat") public static long datePickerToEpoch(String strDate, int type) {
        try {
            if (type == 0) {
                strDate = strDate + " 00:00:00";
            } else {
                strDate = strDate + " 24:00:00";
            }
            return new SimpleDateFormat(DATE_FORMAT_PICKER + " HH:mm:ss").parse(strDate).getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0L;
    }

    public static String now() {
        return now(DATE_FORMAT_DB2);
    }

    public static String nowForSignature() {
        return now(DATE_FORMAT_PICKER);
    }

    public static String now(String dateformat) {
        Date date = new Date();
//        2017-07-13 15:22:52
        @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat(dateformat);
        return df.format(date.getTime());
    }

    private static String cekdate(int day, int month, int year) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_UPLOAD);
        return sdf.format(cal.getTime());
    }


    public static String format(String string, String startFormat, String resultFormat) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat(startFormat);
        try {
            Date date = sdf.parse(string);
            SimpleDateFormat sdfFormat = new SimpleDateFormat(resultFormat, new Locale("in"));
            return sdfFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return string;
        }
    }

    //    start time
    public static String lastDate(long epochSeconds) {
        try {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(epochSeconds);
            return formatter.format(calendar.getTime()) + "+07:00";
        } catch (NumberFormatException e) {
            return "1970-01-01T00:00:00+07:00";
        } catch (Exception e) {
            return "1970-01-01T00:00:00+07:00";
        }
    }

    public static String formatSingleItem(Date item, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US); // the format of your date
        return sdf.format(item);
    }

    public static String uploadDate(long epochSeconds) {
        try {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT_UPLOAD);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(epochSeconds);
            return formatter.format(calendar.getTime());
        } catch (NumberFormatException e) {
            return "1970-01-01";
        } catch (Exception e) {
            return "1970-01-01";
        }
    }

    public static String longToDate(String date) {
        if (!TextUtils.isEmpty(date)) {
            try {
                return longToDate(Long.parseLong(date));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static String longToDate(long epochSeconds) {
        try {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT_DEFAULT);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(epochSeconds);
            return formatter.format(calendar.getTime());
        } catch (NumberFormatException e) {
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    @SuppressLint("SimpleDateFormat") public static Long stringToEpoch(String strDate) {
        try {
            return new SimpleDateFormat(DATE_FORMAT_DEFAULT).parse(strDate).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0L;
    }

    public static void getTime(Context context, final EditText etH,final EditText etM/*, boolean next*/) {
        final int hour, minute;
        final Calendar c = Calendar.getInstance();/*
        if (next) {
            c.set(Calendar.HOUR_OF_DAY, +12);
        }*/
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute = c.get(Calendar.MINUTE);


        TimePickerDialog dpd = new TimePickerDialog(context, (view, hourOfDay, minute1) -> {
            String hday = hourOfDay + "";
            String min = minute1 + "";
            if (hday.length() == 1) {
                hday = "0" + hday;
            }
            if (min.length() == 1) {
                min = "0" + min;
            }
            etH.setText(hday);
            etM.setText(min);
        }, hour, minute, true);

        dpd.show();

    }

    public static long epochTime() {
        return System.currentTimeMillis();
    }

    public static String longToDateString(long epochSeconds, boolean withMinute, boolean withGMT) {
        try {
            String format = DATE_FORMAT_UI2;
            if (withMinute) {
                format = format + " HH:mm:ss";
            }
            if (withGMT) {
                TimeZone timeZone = TimeZone.getDefault();
                format = format + " " + timeZone.getDisplayName(false, TimeZone.SHORT).replace("GMT", "").replace(":", "");
            }

            SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.UK);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(epochSeconds);
            return formatter.format(calendar.getTime());
        } catch (NumberFormatException e) {
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    public static String formatlongtoDateTimeZone(Long unixdate, String format) {
        Date date = new Date(unixdate); // *1000 is to convert seconds to milliseconds
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat(format); // the format of your date
        sdf.setTimeZone(TimeZone.getDefault());
        return sdf.format(date);
    }

    public static String getTodayName(String format) {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();

        return new SimpleDateFormat(format, Locale.ENGLISH).format(date.getTime());
    }
}
