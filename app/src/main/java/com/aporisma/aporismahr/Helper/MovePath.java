package com.aporisma.aporismahr.Helper;

import android.content.Context;
import android.util.Log;

import java.io.*;

public class MovePath {

    public static String targetPath;

    public static void attendance(Context mContext, String sourceLocation, String targetLocation) {
        try {
            File sourceLocationFile = new File(sourceLocation);
            File targetLocationFile = new File(targetLocation);
            if (sourceLocationFile.exists()) {

                InputStream in = new FileInputStream(sourceLocationFile);
                OutputStream out = new FileOutputStream(targetLocationFile);

                // Copy the bits from instream to outstream
                byte[] buf = new byte[1024];
                int len;

                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }

                in.close();
                out.close();

                Log.d("MovePath", "Copy file successful.");
                Log.d("MovePath", targetLocationFile.getPath());
                targetPath = targetLocationFile.getPath();

            } else {
                Log.d("MovePath", "Copy file failed. Source file missing.");
            }
        } catch (Exception e) {
            Dialog.INSTANCE.error(mContext, "ASCI-Ardi@MovePath", e.getMessage());
        }
    }

}
