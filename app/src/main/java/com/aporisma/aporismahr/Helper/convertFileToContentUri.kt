package com.aporisma.aporismahr.Helper

import android.content.Context
import android.net.Uri
import android.provider.MediaStore
import java.io.File


@Throws(Exception::class)
fun convertFileToContentUri(context: Context, file: File): Uri {

    //Uri localImageUri = Uri.fromFile(localImageFile); // Not suitable as it's not a content Uri
    val cr = context.getContentResolver()
    val imagePath = file.getAbsolutePath()
    val imageName: String? = null
    val imageDescription: String? = null
    val uriString = MediaStore.Images.Media.insertImage(cr, imagePath, imageName, imageDescription)
    return Uri.parse(uriString)
}