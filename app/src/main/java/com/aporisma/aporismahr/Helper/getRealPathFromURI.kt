@file:Suppress("DEPRECATION")

package com.aporisma.aporismahr.Helper

import android.app.Activity
import android.net.Uri
import android.provider.MediaStore

fun getRealPathFromURI(activity: Activity,contentUri: Uri): String {
    val proj = arrayOf(MediaStore.Images.Media.DATA)
    val cursor = activity.managedQuery(contentUri, proj, null, null, null)
    val columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
    cursor.moveToFirst()
    return cursor.getString(columnIndex)
}