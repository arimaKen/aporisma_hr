package com.aporisma.aporismahr.Helper

import android.content.Context
import android.net.Uri
import com.aporisma.aporismahr.R
import java.io.File
import java.io.FileNotFoundException

object MovePictureToDirectory {

    lateinit var fileAttendancePath: String

    fun fileAttendance(mContext: Context, pathFrom: String, pathTo: String, imageUri: Uri) {
        try {
            val sourceLocation = File(pathFrom)
            val targetLocation = File(pathTo)

            if (sourceLocation.exists()) {
                if (targetLocation.exists()) {
                    targetLocation.delete()
                }
                sourceLocation.copyTo(targetLocation)
                sourceLocation.delete()
                mContext.contentResolver.delete(imageUri, null, null)

            }
            fileAttendancePath = targetLocation.path
        } catch (e: FileNotFoundException) {
            Dialog.error(mContext, "ASCI-Ardi\nPictureAcitivity", "error@fileAttendance\n" + e.toString() + R.string.sendExceptionMessage)
        }

    }

    fun fileReimbursement(mContext: Context, pathFrom: String) {
        val pathTo = "sdcard/Aporisma/Picture"
        try {
            val sourceLocation = File(pathFrom)
            val targetLocation = File("$pathTo/reim${sourceLocation.name}")
            if (targetLocation.exists()) {
                targetLocation.delete()
            }
            if (sourceLocation.exists()) {
                sourceLocation.copyTo(targetLocation)
                sourceLocation.delete()
            }
        } catch (e: Exception) {
            Dialog.error(mContext, "ASCI-Ardi\nPictureAcitivity", "error@fileAttendance\n" + e.toString() + R.string.sendExceptionMessage)
        }

    }


}