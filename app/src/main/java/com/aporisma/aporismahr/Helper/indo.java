package com.aporisma.aporismahr.Helper;

import java.text.NumberFormat;
import java.util.Locale;

public class indo {
    public static Locale localeID = new Locale("in", "ID");
    public static NumberFormat currency = NumberFormat.getCurrencyInstance(localeID);
}
