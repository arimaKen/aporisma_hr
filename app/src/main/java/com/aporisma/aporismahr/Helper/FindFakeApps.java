package com.aporisma.aporismahr.Helper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.ResolveInfo;

import java.util.ArrayList;
import java.util.List;

public class FindFakeApps {

    public static Boolean notPermissionApps = true;

    public FindFakeApps(Context mContext, Activity mActivity) {
        Intent intent = new Intent(Intent.ACTION_MAIN, null);

        // Set the newly created intent category to launcher
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
//        intent.setType(Intent.CATEGORY_APP_MAPS);

        // Set the intent flags
        intent.setFlags(
                Intent.FLAG_ACTIVITY_NEW_TASK |
                        Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED
        );

        // Generate a list of ResolveInfo object based on intent filter
        List<ResolveInfo> resolveInfoList = mContext.getPackageManager().queryIntentActivities(intent, 0);

        // Initialize a new ArrayList for holding non system package names
        List<String> packageNames = new ArrayList<>();

        // Loop through the ResolveInfo list
        for (ResolveInfo resolveInfo : resolveInfoList) {
            // Get the ActivityInfo from current ResolveInfo
            ActivityInfo activityInfo = resolveInfo.activityInfo;

            // If this is not a system app package
            if (!isSystemPackage(resolveInfo)) {
                // Add the non system package to the list
                packageNames.add(activityInfo.loadLabel(mContext.getPackageManager()).toString());
            }
        }

        List<String> listClone = new ArrayList<>();
        String lastApp = null;
        for (String string : packageNames) {
            if (string.matches("(?i)(fake).*")) {
                listClone.add(string);
                lastApp = string;
            }
        }
        if (listClone.size() <0) {
            for (String string : packageNames) {
                if (string.matches("(?i)(gps).*")) {
                    listClone.add(string);
                    lastApp = string;
                }
            }
        }
        if (listClone.size() > 0) {
            Dialog.INSTANCE.finish(mContext, "Found illegal application with name: " + lastApp + "\nThis application can't start if in your still instal this illegal application");
            notPermissionApps = false;
        }
    }

    private boolean isSystemPackage(ResolveInfo resolveInfo) {
        return ((resolveInfo.activityInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0);
    }

}
