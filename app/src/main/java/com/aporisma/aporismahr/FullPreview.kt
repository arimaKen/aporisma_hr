package com.aporisma.aporismahr

import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import kotlinx.android.synthetic.main.activity_full_preview.*
import java.io.File

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class FullPreview : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_preview)
        val extra : Bundle? = intent.extras
        val path = extra?.getString("imageUrl")
        if (File(path).exists()){
            val localBitmap = BitmapFactory.decodeFile(path)
            ivFull.setImageBitmap(localBitmap)
        } else {
            finish()
        }

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        toolbar.navigationIcon = resources.getDrawable(R.drawable.ic_close)
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener { finish() }
    }

    companion object {
        fun startThisActivity(ctx: Context, path: String) {
            val intent = Intent(ctx, FullPreview::class.java)
            intent.putExtra("imageUrl",path)
            ctx.startActivity(intent)
        }
    }
}
