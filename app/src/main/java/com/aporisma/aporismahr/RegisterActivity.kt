package com.aporisma.aporismahr

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.View
import com.aporisma.aporisma_api.Model.Company.APIResultCompanyAdd
import com.aporisma.aporisma_api.Model.Company.DataStatus
import com.aporisma.aporisma_api.Repo.Repo
import com.aporisma.aporisma_api.Response.BaseResponse
import com.aporisma.aporisma_api.Response.ResponseCourier
import com.aporisma.aporismahr.Helper.Dialog
import com.aporisma.aporismahr.Helper.Dialog.dismiss
import com.aporisma.aporismahr.Helper.Dialog.succeddFinish
import com.aporisma.base_componen.BaseActivity
import com.aporisma.base_componen.setOnClick
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : BaseActivity(),ResponseCourier {

    val mContext = this@RegisterActivity
    val mActivity = this@RegisterActivity
    val repo = Repo(mContext,this)

    override fun initCreate() {
    }

    override fun getLayoutId(): Int = R.layout.activity_register

    override fun initDestroy() {
    }

    override fun getResponse(response: BaseResponse<*>?, message: String?) {
        if (response is APIResultCompanyAdd){
            succeddFinish(mContext,mActivity,resources.getString(R.string.app_name),"$message")
            dismiss()
        }
    }

    companion object {
        fun startThisActivity(ctx: Context) {
            val intent = Intent(ctx, RegisterActivity::class.java)
            ctx.startActivity(intent)
        }
    }

    override fun onClick() {
        object : setOnClick(arrayOf(btnRegister)) {
            @SuppressLint("NewApi", "SetTextI18n")
            override fun doClick(view: View) {
                when (view.id) {
                    R.id.btnRegister -> {
                        Dialog.loading(mContext, repo, "Sedang mengirim data")
                        val addCompany = DataStatus();
                        addCompany.namaCompany = etCompanyName.text.toString()
                        addCompany.alamatCompany = etCompanyAddress.text.toString()
                        addCompany.email = etCompanyEmail.text.toString()
                        addCompany.telpn = etCompanytelp.text.toString()
                        val request = Gson().toJson(addCompany)
                        repo.addCompany(request)
                    }
                }
            }
        }
    }


}
