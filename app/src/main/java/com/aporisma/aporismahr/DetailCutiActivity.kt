package com.aporisma.aporismahr

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.aporisma.aporisma_api.Model.Employee.HistoryCuti
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_detail_cuti.*

class DetailCutiActivity : AppCompatActivity() {

    lateinit var detailCuti: HistoryCuti

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_cuti)
        val intent = intent
        val getExtraData = intent.extras
        if (getExtraData != null) {
            val stringExtra = getExtraData.get("detailCuti") as String?
            detailCuti = Gson().fromJson(stringExtra, HistoryCuti::class.java)
        }
        tvtglMulai.text = "Tanggal mulai\n" + detailCuti.date_start.split(" ")[0]
        tvtglAkhir.text = "Tanggal akhir\n" + detailCuti.date_end.split(" ")[0]
        tvNama.text = "Diminta oleh\n" + detailCuti.employee_name
        val jumlahCuti = 1 +
            Integer.valueOf(detailCuti.date_end.split("/")[0]) - Integer.valueOf(detailCuti.date_start.split("/")[0])
        tvJmlHari.text = "Jumlah hari\n" + jumlahCuti + " hari"
        tvTypeCuti.text = "Tipe Cuti\n" + detailCuti.type_cuti
        tvNote.text = "Alasan\n" + detailCuti.note
    }

    companion object {
        fun startThisActivity(ctx: Context, dataCuti: String) {
            val intent = Intent(ctx, DetailCutiActivity::class.java).putExtra("detailCuti", dataCuti)
            ctx.startActivity(intent)
        }
    }

}
