package com.aporisma.aporismahr

import android.content.Context
import android.content.Intent
import androidx.appcompat.widget.Toolbar
import com.aporisma.base_componen.BaseActivity

class FormCutiActivity : BaseActivity() {
    override fun initCreate() {
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener { finish() }
    }

    override fun getLayoutId(): Int = R.layout.activity_form_cuti

    override fun initDestroy() {

    }
    companion object {
        fun startThisActivity(ctx: Context) {
            val intent = Intent(ctx, FormCutiActivity::class.java)
            ctx.startActivity(intent)
        }
    }
}
