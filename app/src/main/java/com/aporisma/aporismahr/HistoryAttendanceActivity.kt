package com.aporisma.aporismahr

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.core.view.get
import com.aporisma.aporismahr.Helper.GettingLocationHelper
import com.aporisma.base_componen.BaseActivity
import kotlinx.android.synthetic.main.history_activity_attendance.*

class HistoryAttendanceActivity : BaseActivity() {

    var lastDate: Int = 0
    val mContext = this@HistoryAttendanceActivity

    @SuppressLint("SetTextI18n")
    override fun initCreate() {
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        GettingLocationHelper.getLocationWithCheckNetworkAndGPS(this@HistoryAttendanceActivity)
        lastDate = Integer.valueOf(GettingLocationHelper.dateOnly!!.toString()) - 7
        tvDateAttendance.text =
            "$lastDate ${GettingLocationHelper.monthOnly} - ${GettingLocationHelper.dateOnly} ${GettingLocationHelper.monthOnly}"
    }

    override fun getLayoutId(): Int = R.layout.history_activity_attendance

    override fun initDestroy() {
    }

    companion object {
        fun startThisActivity(ctx: Context) {
            val intent = Intent(ctx, HistoryAttendanceActivity::class.java)
            ctx.startActivity(intent)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_navigation, menu)
        menu?.get(0)?.setIcon(R.drawable.ic_add)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action1 -> {
                AddAttendanceActivity.startThisActivity(this@HistoryAttendanceActivity)
            }
            else -> super.onOptionsItemSelected(item)
        }
        return false
    }

    /*override fun onClick() {
        object : setOnClick(arrayOf(ivAdd)) {
            @SuppressLint("NewApi", "SetTextI18n")
            override fun doClick(view: View) {
                when (view.id) {
                    R.id.ivAdd -> {
                        AddAttendanceActivity.startThisActivity(this@HistoryAttendanceActivity)
                    }
                }
            }
        }
    }*/


}
