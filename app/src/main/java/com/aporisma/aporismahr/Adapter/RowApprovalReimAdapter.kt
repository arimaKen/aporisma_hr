package com.aporisma.aporismahr.Adapter

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.aporisma.aporisma_api.Model.Approval.getRow.ApvReim
import com.aporisma.aporisma_api.Model.Approval.post
import com.aporisma.aporisma_api.Model.Employee.Data.DataStatusEmployee
import com.aporisma.aporismahr.ApprovalActivity
import com.aporisma.aporismahr.Claim.ClaimHeaderActivity
import com.aporisma.aporismahr.Helper.Dialog
import com.aporisma.aporismahr.Helper.PrefHelper
import com.aporisma.aporismahr.Helper.indo
import com.aporisma.aporismahr.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.row_apv.view.*

class RowApprovalReimAdapter(
    val items: MutableList<ApvReim>, val listener: ApprovalActivity
) : RecyclerView.Adapter<RowApprovalReimAdapter.ViewHolder>() {

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_apv, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position], listener, context)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("ResourceAsColor", "ShowToast", "SetTextI18n", "NewApi")
        fun bindItem(data: ApvReim, listener: ApprovalActivity, context: Context) {
            try {
                val dataEmployee = PrefHelper.getObject(context,"dataEmployee",DataStatusEmployee::class.java)

                itemView.tvDocNum.text = data.reimburseNo
                itemView.tvDateR.text = data.createdDate.split(" ")[0]
                itemView.tvUser.text = data.namaEmployee
                itemView.tvStatus.text = data.statusStr
                itemView.tvTotal.text = indo.currency.format(data.nominal.toInt())
                itemView.tvUserApprover.text = data.namaApprover
                itemView.btnOptions.setOnCheckedChangeListener { _, isChecked ->
                    if (isChecked) {
                        itemView.llOptions.visibility = View.VISIBLE
                        itemView.ivOptions.setImageDrawable(context.getDrawable(R.drawable.ic_hide))
                    } else {
                        itemView.llOptions.visibility = View.GONE
                        itemView.ivOptions.setImageDrawable(context.getDrawable(R.drawable.ic_show))
                    }
                }
                itemView.btnDetails.setOnClickListener{
                    ClaimHeaderActivity.startThisActivity(context, data.idForm)
                }
                itemView.btnAccept.setOnClickListener{
                    inputNode(context,listener,data,1)
                }
                itemView.btnReject.setOnClickListener{
                    inputNode(context,listener,data,0)
                }


                if (data.status != "W") {
                    itemView.btnReject.isEnabled = false
                    itemView.btnAccept.isEnabled = false
                    itemView.btnReject.visibility = View.INVISIBLE
                    itemView.btnAccept.visibility = View.INVISIBLE
                    if (data.status == "A") itemView.llApproved.visibility = View.GONE
                    else itemView.tvUserApproverLbl.text = "Reject by : "
                }
                else {
                    if (data.userId != dataEmployee.idEmployee) {
                        itemView.btnReject.isEnabled = false
                        itemView.btnAccept.isEnabled = false
                        itemView.btnReject.visibility = View.INVISIBLE
                        itemView.btnAccept.visibility = View.INVISIBLE
                    }
                    else {
                        itemView.btnReject.isEnabled = true
                        itemView.btnAccept.isEnabled = true
                        itemView.btnReject.visibility = View.VISIBLE
                        itemView.btnAccept.visibility = View.VISIBLE
                    }
                }

            } catch (e: Exception) {
                Dialog.error(context, "ASCI-Ardi", "error@RAP\n$e")
            }
        }

        @SuppressLint("SetTextI18n")
        private fun inputNode(context: Context, listener: ApprovalActivity, data: ApvReim, status:Int) {
            try {
                val dialogBuilder = AlertDialog.Builder(context)
                @SuppressLint("InflateParams") val dialogView =
                    LayoutInflater.from(context).inflate(R.layout.dialog_message, null)
                dialogBuilder.setView(dialogView)

                val etSetting = dialogView.findViewById<EditText>(R.id.etSetting)
                val tvSetting = dialogView.findViewById<TextView>(R.id.tvSetting)
                val tvSettingTitle = dialogView.findViewById<TextView>(R.id.tvSettingTitle)
                val btnOK = dialogView.findViewById<Button>(R.id.btnOK)
                val btnCancel = dialogView.findViewById<Button>(R.id.btnCancel)
                val b = dialogBuilder.create()
                val statusApprov : String

                b.show()
                b.setCanceledOnTouchOutside(false)
                b.setCancelable(false)

                val lp = WindowManager.LayoutParams()
                lp.copyFrom(b.window?.attributes)
                lp.width = WindowManager.LayoutParams.MATCH_PARENT
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT
                lp.windowAnimations = R.style.DialogAnimation
                b.window?.attributes = lp

                tvSetting.textAlignment = View.TEXT_ALIGNMENT_CENTER
                etSetting.hint = "Insert note here!"
                tvSetting.text = "Please insert Remark"
                tvSettingTitle.text = context.resources.getString(R.string.app_name)
                btnCancel.setOnClickListener {
                    b.dismiss()
                }
                if (status == 1) {
                    btnOK.text = "Approved"
                    statusApprov = "A"
                } else {
                    btnOK.text = "Reject"
                    statusApprov = "R"
                }
                btnOK.setOnClickListener {
                    if(etSetting.length() > 0){
                        val post  = post()
                        val dataStatusEmployee = PrefHelper.getObject(context,"dataEmployee", DataStatusEmployee::class.java)
                        post.idDoc = data.docId.toInt()
                        post.docType = data.docType
                        post.idEmployee = dataStatusEmployee.idEmployee.toInt()
                        post.createdUser = dataStatusEmployee.namaDepan
                        post.remark = etSetting.text.toString()
                        post.status = statusApprov
                        listener.repo.appvSubmission(Gson().toJson(post))
                        Dialog.loading(context,listener.repo,"Loading...")
                    } else {
                        Dialog.error(context, context.resources.getString(R.string.app_name), "Please insert remark")
                    }
                    b.dismiss()
                }
            } catch (e: Exception) {
                Dialog.error(context, "ASCI-Ardi\nShowDialog", "error@BuildMessage\n$e\n${R.string.sendExceptionMessage}")
            }
        }
    }
}
