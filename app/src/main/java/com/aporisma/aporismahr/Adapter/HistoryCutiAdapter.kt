package com.aporisma.aporismahr.Adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aporisma.aporisma_api.Model.Employee.HistoryCuti
import com.aporisma.aporismahr.DetailCutiActivity
import com.aporisma.aporismahr.HistoryLeaveActivity
import com.aporisma.aporismahr.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.row_leave_history.view.*

class HistoryCutiAdapter(
    val items: MutableList<HistoryCuti>, val listener: HistoryLeaveActivity
) : RecyclerView.Adapter<HistoryCutiAdapter.ViewHolder>() {

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_leave_history, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position], listener, context)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("ResourceAsColor", "ShowToast", "SetTextI18n", "NewApi")
        fun bindItem(data: HistoryCuti, listener: HistoryLeaveActivity, context: Context) {

            itemView.tvJudul.text = data.note
            itemView.tvTglCuti.text = "Untuk : ${data.date_start.split(" ")[0]}-${data.date_end.split(" ")[0]}"
            itemView.tvNama.text = data.employee_name
            itemView.tvTglPengajuan.text = data.date_assign.split(" ")[0]
            itemView.tvVertifikasi.text = data.verifikasi
            itemView.row.setOnClickListener {
                val detailCuti = Gson().toJson(data)
                DetailCutiActivity.startThisActivity(context,detailCuti)
            }

        }
    }
}
