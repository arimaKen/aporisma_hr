package com.aporisma.aporismahr.Adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aporisma.aporisma_api.Model.Company.DataStatus
import com.aporisma.aporismahr.Main.LoginActivity
import com.aporisma.aporismahr.R
import kotlinx.android.synthetic.main.login_activity.*
import kotlinx.android.synthetic.main.row_data_string.view.*


class CompanyAdapater(
    private val items: MutableList<DataStatus>, private val listener: LoginActivity
) : RecyclerView.Adapter<CompanyAdapater.ViewHolder>() {

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_data_string, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(context, items[position], listener)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("ResourceAsColor", "ShowToast", "SetTextI18n", "NewApi")
        fun bindItem(
            context: Context, data: DataStatus, listener: LoginActivity
        ) {

            itemView.tvString.setText("${data.namaCompany}\n${data.alamatCompany}")
            itemView.tvString.setOnFocusChangeListener { _, _ ->
                listener.etUsername.requestFocus()
                listener.dataCompany = data
                listener.etCompany.setText(data.namaCompany)
                listener.svString.visibility = View.GONE
            }
            itemView.tvString.isClickable = true
        }
    }
}
