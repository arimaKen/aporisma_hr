package com.aporisma.aporismahr.Adapter

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aporisma.aporisma_api.Model.Notification.DataStatus
import com.aporisma.aporismahr.Helper.Dialog
import com.aporisma.aporismahr.Main.MainActivity
import com.aporisma.aporismahr.R
import kotlinx.android.synthetic.main.row_announcement.view.*

class NotificationAdapter(
    val items: MutableList<DataStatus>, val listener: MainActivity, val dialog: AlertDialog
) : RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_announcement, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position], listener, context, dialog)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("ResourceAsColor", "ShowToast", "SetTextI18n", "NewApi")
        fun bindItem(data: DataStatus, listener: MainActivity, context: Context, dialog: AlertDialog) {
            try {
                itemView.tvTitleAnnoun.text = "${data.createdUser} ${data.subject}"
                itemView.tvTitleAnnoun.textSize = 12f
                itemView.tvTitleAnnoun.setTextColor(R.color.black)
                itemView.tvDateAnnounce.text = data.createdDate
                itemView.tvDateAnnounce.textSize = 12f
                itemView.tvDateAnnounce.setTextColor(R.color.black)
                itemView.row.setBackgroundColor(android.R.color.transparent)
                itemView.vHover.visibility = View.GONE
                if (data.isRead != "N") itemView.ic_redDot.visibility = View.GONE
                itemView.row.setOnClickListener{
                    dialog.dismiss()
                    Dialog.succedd(context,"Notification",data.description)
                    if (data.isRead == "N") listener.repo.readNotif(data.idNotif)
                    listener.notificationAdapter.notifyDataSetChanged()
                    listener.repo.getlistNotif(listener.dataStatusEmployee.idEmployee)
                }
            } catch (e: Exception) {
                Dialog.error(context, "ASCI-Ardi", "error@NotifA\n$e")
            }
        }
    }
}
