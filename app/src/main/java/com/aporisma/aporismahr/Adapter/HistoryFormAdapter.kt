package com.aporisma.aporismahr.Adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aporisma.aporisma_api.Model.Reimbursement.DataStatus.FormReimbursement
import com.aporisma.aporismahr.FormClaimActivity
import com.aporisma.aporismahr.HistoryClaimActivity
import com.aporisma.aporismahr.R
import kotlinx.android.synthetic.main.row_claim.view.tvTglReim
import kotlinx.android.synthetic.main.row_claim.view.tvTotalReim
import kotlinx.android.synthetic.main.row_claim.view.tvTypeReim
import kotlinx.android.synthetic.main.row_form_claim.view.*


class HistoryFormAdapter(
    val items: MutableList<FormReimbursement>, val listener: HistoryClaimActivity
) : RecyclerView.Adapter<HistoryFormAdapter.ViewHolder>() {

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_form_claim, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position], listener, context)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("ResourceAsColor", "ShowToast", "SetTextI18n", "NewApi")
        fun bindItem(data: FormReimbursement, listener: HistoryClaimActivity, context: Context) {
            itemView.row.setOnClickListener{
                FormClaimActivity.startThisActivity(context,data.idForm)
            }

            itemView.tvTypeReim.text = data.subject
            itemView.tvTotalReim.text = data.nominal.toString()
            itemView.tvTglReim.text = data.dateAssign.split(" ")[0]
            when {
                data.status.equals("N") || data.status.equals("n") -> itemView.tvVertifikasi.text =
                    "Not Approved"
                data.status.equals("P") || data.status.equals("p") -> itemView.tvVertifikasi.text =
                    "Waiting Approved"
                data.status.equals("R") || data.status.equals("r") -> itemView.tvVertifikasi.text =
                    "Revisi"
                data.status.equals("A") || data.status.equals("a") -> itemView.tvVertifikasi.text =
                    "Approved"
            }
        }
    }
}
