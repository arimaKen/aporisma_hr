package com.aporisma.aporismahr.Adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aporisma.aporisma_api.Model.Reimbursement.DataStatus.ReimbursementDetails
import com.aporisma.aporismahr.Claim.ClaimActivity
import com.aporisma.aporismahr.Claim.ClaimDetailActivity
import com.aporisma.aporismahr.Helper.Dialog
import com.aporisma.aporismahr.Helper.indo
import com.aporisma.aporismahr.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.row_claim_d.view.*

class HistoryReimDetail(
    val items: MutableList<ReimbursementDetails>, val listener: ClaimActivity
) : RecyclerView.Adapter<HistoryReimDetail.ViewHolder>() {

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_claim_d, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position], listener, context)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("ResourceAsColor", "ShowToast", "SetTextI18n", "NewApi")
        fun bindItem(data: ReimbursementDetails, listener: ClaimActivity, context: Context) {
            try {
                itemView.tvTypeReim.text = data.tipeValue
                itemView.tvTotalReim.text = indo.currency.format(data.nominal.toDouble())
                itemView.tvTglReim.text = data.tanggal
                itemView.tvTime.text = data.createdDate
                itemView.ivChecked.visibility = View.GONE
                val localPhoto = Drawable.createFromPath(data.pathLocal)
                if (localPhoto!= null) itemView.ivKlaim.setImageDrawable(localPhoto)
                itemView.row.setOnClickListener {
                    val dataJSON = Gson().toJson(data)
                    ClaimDetailActivity.startThisActivity(context, dataJSON)
                }
            } catch (e: Exception) {
                Dialog.error(context, "ASCI-Ardi", "error@ClaimAdapter\n$e")
            }
        }
    }
}
