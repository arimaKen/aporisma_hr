package com.aporisma.aporismahr.Adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aporisma.aporisma_api.Model.TimeSheet.TimeSheetDetail
import com.aporisma.aporismahr.Helper.Dialog
import com.aporisma.aporismahr.R
import com.aporisma.aporismahr.Timesheet.TimeSheetActivity
import com.aporisma.aporismahr.Timesheet.TimeSheetDetailsActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.row_time_sheet_d.view.*

class HistoryTSDetail(
    val items: MutableList<TimeSheetDetail>, val listener: TimeSheetActivity
) : RecyclerView.Adapter<HistoryTSDetail.ViewHolder>() {

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_time_sheet_d, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position], listener, context)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("ResourceAsColor", "ShowToast", "SetTextI18n", "NewApi")
        fun bindItem(data: TimeSheetDetail, listener: TimeSheetActivity, context: Context) {
            try {
                itemView.tvCustomer.text = data.customer
                itemView.tvProject.text = data.type
                itemView.tvLocation.text = data.lokasi
                itemView.tvDate.text = data.date
                itemView.tvTimeIn.text = data.timeIn.toString()
                itemView.tvTimeOut.text = data.timeOut.toString()
                itemView.row.setOnClickListener {
                    val dataJSON = Gson().toJson(data)
                    TimeSheetDetailsActivity.startThisActivity(context, "R#$dataJSON")
                }
               itemView.ivChecked.visibility = View.GONE
            } catch (e: Exception) {
                Dialog.error(context, "ASCI-Ardi", "error@HistoryTSD\n$e")
            }
        }
    }
}
