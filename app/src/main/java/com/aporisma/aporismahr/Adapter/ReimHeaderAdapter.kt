package com.aporisma.aporismahr.Adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aporisma.aporisma_api.Model.Reimbursement.DataStatus.ReimbursementDetails
import com.aporisma.aporismahr.Claim.ClaimDetailActivity
import com.aporisma.aporismahr.Claim.ClaimHeaderActivity
import com.aporisma.aporismahr.Helper.Dialog
import com.aporisma.aporismahr.Helper.indo
import com.aporisma.aporismahr.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.claim_header_activity.*
import kotlinx.android.synthetic.main.row_claim_d.view.*
import java.text.NumberFormat
import java.util.*


class ReimHeaderAdapter(
    private val items: MutableList<ReimbursementDetails>, private val listener: ClaimHeaderActivity
) : RecyclerView.Adapter<ReimHeaderAdapter.ViewHolder>() {

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_claim_d, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position], listener, context)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("ResourceAsColor", "ShowToast", "SetTextI18n", "NewApi")
        fun bindItem(
            data: ReimbursementDetails, listener: ClaimHeaderActivity, context: Context
        ) {
            try {
                val localeID = Locale("in", "ID")
                val formatRupiah = NumberFormat.getCurrencyInstance(localeID)
                itemView.tvTypeReim.text = data.tipeValue
                itemView.tvTotalReim.text = indo.currency.format(data.nominal.toDouble())
                itemView.tvTglReim.text = data.tanggal
                itemView.tvTime.text = data.createdDate
                val localPhoto = Drawable.createFromPath(data.pathLocal)
                itemView.ic_time.visibility = View.GONE
                itemView.tvTime.visibility = View.GONE
                if (localPhoto != null) itemView.ivKlaim.setImageDrawable(localPhoto)
                itemView.row.setOnClickListener {
                    if (data.idForm == null) {
                        if (itemView.row.tag == "notSelected") {
                            itemView.ivChecked.visibility = View.VISIBLE
                            itemView.row.tag = "Selected"
                            listener.nominalSelected = listener.nominalSelected + data.nominal
                            listener.tvTotal.setText(formatRupiah.format(listener.nominalSelected.toDouble()))
                            data.selected = "true"
                        } else {
                            itemView.ivChecked.visibility = View.GONE
                            itemView.row.tag = "notSelected"
                            listener.nominalSelected = listener.nominalSelected - data.nominal
                            listener.tvTotal.setText(formatRupiah.format(listener.nominalSelected.toDouble()))
                            data.selected = "false"
                        }
                    } else if (data.status == "R"){
                        val dataJSON = Gson().toJson(data)
                        ClaimDetailActivity.startThisActivity(context, dataJSON)
                    }
                }
            } catch (e: Exception) {
                Dialog.error(context, "ASCI-Ardi", "error@ReimHeaderAdapter\n$e")
            }
        }
    }
}
