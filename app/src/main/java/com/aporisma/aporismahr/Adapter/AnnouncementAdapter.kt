package com.aporisma.aporismahr.Adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aporisma.aporisma_api.Model.Notification.Announcement
import com.aporisma.aporismahr.Helper.Dialog
import com.aporisma.aporismahr.Main.MainActivity
import com.aporisma.aporismahr.R
import kotlinx.android.synthetic.main.row_announcement.view.*

class AnnouncementAdapter(
    val items: MutableList<Announcement>, val listener: MainActivity
) : RecyclerView.Adapter<AnnouncementAdapter.ViewHolder>() {

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_announcement, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position], listener, context)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("ResourceAsColor", "ShowToast", "SetTextI18n", "NewApi")
        fun bindItem(data: Announcement, listener: MainActivity, context: Context) {
            try {
                itemView.tvTitleAnnoun.text = "${data.createdUser} ${data.judul}"
                itemView.tvDateAnnounce.text = data.createdDate
                itemView.ic_redDot.visibility = View.GONE
                itemView.row.setOnClickListener{
                    Dialog.succedd(context,"Announcement",data.isi)
                }
            } catch (e: Exception) {
                Dialog.error(context, "ASCI-Ardi", "error@NotifA\n$e")
            }
        }
    }
}
