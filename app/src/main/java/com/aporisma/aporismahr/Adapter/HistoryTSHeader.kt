package com.aporisma.aporismahr.Adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aporisma.aporisma_api.Model.TimeSheet.TimeSheetHeader
import com.aporisma.aporismahr.Helper.Dialog
import com.aporisma.aporismahr.R
import com.aporisma.aporismahr.Timesheet.TimeSheetActivity
import com.aporisma.aporismahr.Timesheet.TimeSheetHeaderActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.row_time_sheet_h.view.*

class HistoryTSHeader(
    val items: MutableList<TimeSheetHeader>, val listener: TimeSheetActivity
) : RecyclerView.Adapter<HistoryTSHeader.ViewHolder>() {

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_time_sheet_h, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position], listener, context)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("ResourceAsColor", "ShowToast", "SetTextI18n", "NewApi")
        fun bindItem(data: TimeSheetHeader, listener: TimeSheetActivity, context: Context) {
            try {
                itemView.tvSubject.text = "Doc # ${data.tsNo}"
                itemView.tvCreatedData.text ="Date Requested : ${data.createdDate}"
                when {/*
                    data.status == "P" || data.status == "p"  -> {
                        itemView.tvStatus.text = "Pending"
                    }*/
                    data.status == "W" || data.status == "w" -> {
                        itemView.tvStatus.text = "Waiting Approval"
                        itemView.tvStatus.setTextColor(context.resources.getColor(android.R.color.holo_orange_dark))
                    }
                    data.status == "A" || data.status == "a" -> {
                        itemView.tvStatus.text = "Approval by ${data.approvedUser}\n${data.approvedDate}"
                        itemView.tvStatus.setTextColor(context.resources.getColor(android.R.color.holo_blue_dark))
                    }
                    data.status == "R" || data.status == "r" -> {
                        itemView.tvStatus.text = "Revisi by ${data.approvedUser}\n${data.approvedDate}"
                        itemView.tvStatus.setTextColor(context.resources.getColor(android.R.color.holo_red_dark))
                    }
                }
                itemView.row.setOnClickListener {
                    val dataJSON = Gson().toJson(data)
                    if (data.status == "R"){
                        TimeSheetHeaderActivity.startThisActivity(context, "R;$dataJSON")
                    } else {
                        TimeSheetHeaderActivity.startThisActivity(context, dataJSON)
                    }
                }
            } catch (e: Exception) {
                Dialog.error(context, "ASCI-Ardi", "error@ClaimAdapter\n$e")
            }
        }
    }
}
