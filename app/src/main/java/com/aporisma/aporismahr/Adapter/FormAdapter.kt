package com.aporisma.aporismahr.Adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aporisma.aporisma_api.Model.Reimbursement.DataStatus.Reimbursement
import com.aporisma.aporismahr.FormClaimActivity
import com.aporisma.aporismahr.R
import kotlinx.android.synthetic.main.activity_form_claim.*
import kotlinx.android.synthetic.main.row_claim.view.*
import java.text.NumberFormat
import java.util.*


class FormAdapter(
    val items: MutableList<Reimbursement>, val listener: FormClaimActivity
) : RecyclerView.Adapter<FormAdapter.ViewHolder>() {

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_claim, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position], listener, context)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("ResourceAsColor", "ShowToast", "SetTextI18n", "NewApi")
        fun bindItem(data: Reimbursement, listener: FormClaimActivity, context: Context) {
            val localeID = Locale("in", "ID")
            val formatRupiah = NumberFormat.getCurrencyInstance(localeID)
            itemView.tvTypeReim.text = data.tipe
            itemView.tvTotalReim.text = data.nominal.toString()
            itemView.tvTglReim.text = data.tanggal.replace("T"," ")
            val localPhoto = Drawable.createFromPath(data.pathLocal)
            itemView.ivKlaim.setImageDrawable(localPhoto)
            itemView.row.setOnClickListener {
                if (itemView.row.tag == "notSelected") {
                    itemView.ivChecked.visibility = View.VISIBLE
                    itemView.row.setTag("Selected")
                    listener.nominalSelected = listener.nominalSelected + data.nominal
                    listener.tvTotal.text = formatRupiah.format(listener.nominalSelected.toDouble())
                    data.selected = true
                } else {
                    itemView.row.tag = "notSelected"
                    itemView.ivChecked.visibility = View.GONE
                    listener.nominalSelected = listener.nominalSelected - data.nominal
                    listener.tvTotal.text = formatRupiah.format(listener.nominalSelected.toDouble())
                    data.selected = false
                }
            }
        }
    }
}
