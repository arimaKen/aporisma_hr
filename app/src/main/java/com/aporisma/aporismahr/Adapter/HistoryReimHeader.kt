package com.aporisma.aporismahr.Adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aporisma.aporisma_api.Model.Reimbursement.DataStatus.ReimbursementHeader
import com.aporisma.aporismahr.Claim.ClaimActivity
import com.aporisma.aporismahr.Claim.ClaimHeaderActivity
import com.aporisma.aporismahr.Helper.indo
import com.aporisma.aporismahr.R
import kotlinx.android.synthetic.main.row_claim_d.view.tvTypeReim
import kotlinx.android.synthetic.main.row_claim_h.view.*
import kotlinx.android.synthetic.main.row_claim_h.view.row


class HistoryReimHeader(
    val items: MutableList<ReimbursementHeader>, val listener: ClaimActivity
) : RecyclerView.Adapter<HistoryReimHeader.ViewHolder>() {

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_claim_h, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position], listener, context)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("ResourceAsColor", "ShowToast", "SetTextI18n", "NewApi")
        fun bindItem(data: ReimbursementHeader, listener: ClaimActivity, context: Context) {
            itemView.row.setOnClickListener {
                if (data.status == "R") ClaimHeaderActivity.startThisActivity(context, "R;" + data.idForm)
                else ClaimHeaderActivity.startThisActivity(context, data.idForm)
            }
            itemView.tvTypeReim.text =
                "Doc # ${data.reimburseNo}\nDate Requested : ${data.createdDate}\nTotal ${indo.currency.format(data.nominal.toDouble())}"
//            itemView.tvTotalReim.text = ""
//            itemView.tvTglReim.text
            when {
                data.status.equals("W") || data.status.equals("w") -> {
                    itemView.tvVertifikasi.text = "Waiting Approval"
                    itemView.tvVertifikasi.setTextColor(context.resources.getColor(android.R.color.holo_orange_dark))
                }
                data.status.equals("R") || data.status.equals("r") -> {
                    itemView.tvVertifikasi.text =
                        "Revisi by " + data.approvedUser + "\n" + data.approvedDate
                    itemView.tvVertifikasi.setTextColor(context.resources.getColor(android.R.color.holo_red_dark))
                }
                data.status.equals("A") || data.status.equals("a") -> {
                    if (data.isPaid == "Y") itemView.tvVertifikasi.text = "Approved by ${data.approvedUser}\nPayment done\n${data.approvedDate}"
                    else itemView.tvVertifikasi.text = "Approved by ${data.approvedUser}\nPayment none\n${data.approvedDate}"

                    itemView.tvVertifikasi.setTextColor(context.resources.getColor(android.R.color.holo_blue_dark))

                }
            }
        }
    }
}
