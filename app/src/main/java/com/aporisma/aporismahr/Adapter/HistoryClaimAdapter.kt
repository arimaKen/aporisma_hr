package com.aporisma.aporismahr.Adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aporisma.aporisma_api.Model.Reimbursement.DataStatus.Reimbursement
import com.aporisma.aporismahr.AddClaimActivity
import com.aporisma.aporismahr.HistoryClaimActivity
import com.aporisma.aporismahr.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.row_claim.view.*

class HistoryClaimAdapter(
    val items: MutableList<Reimbursement>, val listener: HistoryClaimActivity
) : RecyclerView.Adapter<HistoryClaimAdapter.ViewHolder>() {

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_claim, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position], listener, context)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("ResourceAsColor", "ShowToast", "SetTextI18n", "NewApi")
        fun bindItem(data: Reimbursement, listener: HistoryClaimActivity, context: Context) {

            itemView.tvTypeReim.text = data.tipe
            itemView.tvTotalReim.text = data.nominal.toString()
            itemView.tvTglReim.text = data.tanggal.replace("T"," ")
            val localPhoto = Drawable.createFromPath(data.pathLocal)
            itemView.ivKlaim.setImageDrawable(localPhoto)
            itemView.row.setOnClickListener {
                val dataJSON = Gson().toJson(data)
                AddClaimActivity.startThisActivity(context,dataJSON)
            }
        }
    }
}
