package com.aporisma.aporismahr.Adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aporisma.aporisma_api.Model.TimeSheet.TimeSheetDetail
import com.aporisma.aporismahr.Helper.Dialog
import com.aporisma.aporismahr.R
import com.aporisma.aporismahr.Timesheet.TimeSheetDetailsActivity
import com.aporisma.aporismahr.Timesheet.TimeSheetHeaderActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.row_time_sheet_d.view.*

class TSDetailAdapter(
    val items: MutableList<TimeSheetDetail>, val listener: TimeSheetHeaderActivity
) : RecyclerView.Adapter<TSDetailAdapter.ViewHolder>() {

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_time_sheet_d, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position], listener, context)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("ResourceAsColor", "ShowToast", "SetTextI18n", "NewApi")
        fun bindItem(data: TimeSheetDetail, listener: TimeSheetHeaderActivity, context: Context) {
            try {
                itemView.tvCustomer.text = data.customer
                itemView.tvProject.text = data.type
                itemView.tvLocation.text = data.lokasi
                itemView.tvDate.text = data.date
                itemView.tvTimeIn.text = data.timeIn.toString() + "-" + data.timeOut.toString()
                itemView.row.setOnClickListener {
                    if (data.status == "R"){
                        val dataJSON = Gson().toJson(data)
                        TimeSheetDetailsActivity.startThisActivity(context, "R;$dataJSON")
                    } else if (data.status != "A") {
                        val dataJSON = Gson().toJson(data)
                        TimeSheetDetailsActivity.startThisActivity(context, dataJSON)
                    }
                }
                itemView.ivChecked.setOnClickListener {
                    if (itemView.row.tag == "notSelected") {
                        itemView.ivChecked.setImageDrawable(context.resources.getDrawable(R.drawable.ic_check_circle_white))
                        itemView.row.tag = "Selected"
                        data.selected = "y"
                    } else {
                        itemView.ivChecked.setImageDrawable(context.resources.getDrawable(R.drawable.background_round))
                        itemView.row.tag = "notSelected"
                        data.selected = "n"
                    }
                }
                if (data.idTs != null) {
                    itemView.ivChecked.visibility = View.GONE
                } else {
                    itemView.ivChecked.visibility = View.VISIBLE
                }
            } catch (e: Exception) {
                Dialog.error(context, "ASCI-Ardi", "error@HistoryTSD\n$e")
            }
        }
    }
}
