package com.aporisma.aporismahr.Main

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.app.AppCompatActivity
import com.aporisma.aporisma_api.Model.Company.APIResultCompany
import com.aporisma.aporisma_api.Model.Company.DataStatus
import com.aporisma.aporisma_api.Repo.Repo
import com.aporisma.aporisma_api.Response.BaseResponse
import com.aporisma.aporisma_api.Response.ResponseCourier
import com.aporisma.aporismahr.Adapter.CompanyAdapater
import com.aporisma.aporismahr.R
import kotlinx.android.synthetic.main.search_activity.*

class SearchActivity : AppCompatActivity(), ResponseCourier {
    override fun getResponse(response: BaseResponse<*>?, message: String?) {
        if (response is APIResultCompany) {
            if (listCompany.size > 0) listCompany.clear()
            else listCompany.addAll(response.dataStatus)
            dataCompanyAdapter.notifyDataSetChanged()
        } else {
            if (listCompany.size > 0) listCompany.clear()
        }
    }

    val mContext: Context = this
    val mActivity: Activity = this
    lateinit var dataCompanyAdapter: CompanyAdapater
    var listCompany = ArrayList<DataStatus>()
    var repo = Repo(mContext, this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.search_activity)
        searchCompany()
        /*dataCompanyAdapter = CompanyAdapater(listCompany, this)
        val mLayoutManager = LinearLayoutManager(this)
        rvCompanyName.layoutManager = mLayoutManager
        rvCompanyName.itemAnimator = DefaultItemAnimator()
        rvCompanyName.adapter = dataCompanyAdapter
        dataCompanyAdapter.notifyDataSetChanged()*/
    }

    companion object {
        fun startThisActivity(ctx: Context) {
            val intent =
                Intent(ctx, SearchActivity::class.java)
            ctx.startActivity(intent)
        }
    }


    private fun searchCompany() {
        etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (etSearch.length() > 0) repo.searchCompany(charSequence.toString())
                else if (etSearch.length() == 0) listCompany.clear()
                dataCompanyAdapter.notifyDataSetChanged()
            }

            override fun afterTextChanged(editable: Editable) {}
        })
    }
}
