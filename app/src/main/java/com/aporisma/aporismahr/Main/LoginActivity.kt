package com.aporisma.aporismahr.Main

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.text.Editable
import android.text.TextWatcher
import android.text.method.LinkMovementMethod
import android.view.View
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.aporisma.aporisma_api.Model.Company.APIResultCompany
import com.aporisma.aporisma_api.Model.Company.DataStatus
import com.aporisma.aporisma_api.Model.Employee.APIResultEmployee
import com.aporisma.aporisma_api.Model.Employee.APIResultGetAprvAccess
import com.aporisma.aporisma_api.Model.Employee.Data.DataStatusEmployee
import com.aporisma.aporisma_api.Model.Token.APIResultToken
import com.aporisma.aporisma_api.Repo.Repo
import com.aporisma.aporisma_api.Response.BaseResponse
import com.aporisma.aporisma_api.Response.ResponseCourier
import com.aporisma.aporismahr.Adapter.CompanyAdapater
import com.aporisma.aporismahr.Helper.Dialog.dismiss
import com.aporisma.aporismahr.Helper.Dialog.error
import com.aporisma.aporismahr.Helper.Dialog.loading
import com.aporisma.aporismahr.Helper.Dialog.login
import com.aporisma.aporismahr.Helper.Dialog.settingServer
import com.aporisma.aporismahr.Helper.PrefHelper
import com.aporisma.aporismahr.R
import com.aporisma.aporismahr.R.string.app_name
import com.aporisma.base_componen.BaseActivity
import com.aporisma.base_componen.setOnClick
import com.google.gson.Gson
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions
import kotlinx.android.synthetic.main.login_activity.*


@Suppress("DEPRECATION")
class LoginActivity : BaseActivity(), ResponseCourier {
    /* val spinnerArray = ArrayList<String>()
     private fun insertSpinnerMaster() {
          val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, spinnerArray)
          adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
          val sItems = findViewById<View>(R.id.etCompany) as Spinner
          sItems.adapter = adapter
      }*/

    val mContext: Context = this@LoginActivity
    val mActivity: Activity = this@LoginActivity
    var dataCompany = DataStatus()
    lateinit var dataCompanyAdapter: CompanyAdapater
    var listCompany = ArrayList<DataStatus>()

    var repo = Repo(mContext, this)

    override fun initCreate() {
        loading(mContext, repo, "Loading...")
        getVersion()
        etCompany.requestFocus()
        requestPermission()
        val intent = intent
        val getExtraData = intent.extras
        val stringExtra = getExtraData?.get("DataCompany") as String?
        if (stringExtra != null) {
            dataCompany = Gson().fromJson(stringExtra, DataStatus::class.java)
            etCompany.setText(dataCompany.namaCompany)
        }
        tvLinkReg.setOnClickListener {
            tvLinkReg.movementMethod = LinkMovementMethod.getInstance();
        }
        searchCompany()
        initAdapter()
    }

    override fun getLayoutId(): Int = R.layout.login_activity

    override fun onClick() {
        object : setOnClick(arrayOf(btnLogin, tvRegister, ivSetting)) {
            @SuppressLint("NewApi", "SetTextI18n", "SdCardPath")
            override fun doClick(view: View) {
                when (view.id) {
                    R.id.btnLogin -> {
                        if (etUsername.length() > 0 || etPassword.length() > 0) {
                            loading(mContext, repo, "Loading...")
                            val loginRequest =
                                DataStatusEmployee()
                            loginRequest.username = etUsername.text.toString()
                            loginRequest.password = etPassword.text.toString()
                            loginRequest.idCompany = dataCompany.idCompany.toString()/*
                            val pp = File("/sdcard/Aporisma/Picture/pp.jpg")
                            loginRequest.pp = pp.exists()*/
                            repo.login(Gson().toJson(loginRequest))
                        } else {
                            error(mContext, resources.getString(app_name), "Please insert all field")
                        }
                    }
                    R.id.ivSetting -> {
                        settingServer(mContext, "Setting Server")
                    }
                }
            }
        }
    }

    private fun searchCompany() {
        etCompany.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                charSequence: CharSequence,
                i: Int,
                i1: Int,
                i2: Int
            ) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (etCompany.hasFocus() && etCompany.length() > 0) {
                    repo.searchCompany(charSequence.toString())
                    loading(mContext, repo, "Mohon Tunggu")
                } else if (etCompany.length() == 0) {
                    listCompany.clear()
                    svString.visibility = View.GONE
                }
//                dataCompanyAdapter.notifyDataSetChanged()
            }

            override fun afterTextChanged(editable: Editable) {}
        })
//        if (etCompany.hasFocus() && etCompany.length() == 0)
//        if (etCompany.hasFocus()) etCompany.setText("")
//        etCompany.setOnFocusChangeListener { _, _ -> etCompany.setText("") }
    }

    private fun initAdapter() {
        dataCompanyAdapter = CompanyAdapater(listCompany, this)
        val mLayoutManager = LinearLayoutManager(this)
        rvCompanyName.layoutManager = mLayoutManager
        rvCompanyName.itemAnimator = DefaultItemAnimator()
        rvCompanyName.adapter = dataCompanyAdapter
        dataCompanyAdapter.notifyDataSetChanged()
    }

    override fun initDestroy() {
    }

    override fun getResponse(response: BaseResponse<*>?, message: String?) {
        dismiss()
        when (response) {
            is APIResultToken -> {
                checkRemaiderMe()
            }
            is APIResultEmployee -> {
                PrefHelper.saveObjectToPref(mContext, "dataEmployee", response.dataStatus)
//                if (response.dataStatus.photo != null) downloadFileFromBase64(response.dataStatus.photo)
                if (swReminder.isChecked) PrefHelper.saveToPref(mContext, "swReminder", "Y")
                login(mContext, mActivity, "$message")
                PrefHelper.saveObjectToPref(mContext, "dataCompany", dataCompany)
                PrefHelper.saveToPref(mContext, "idCompany", dataCompany.idCompany.toString())

            }
            is APIResultGetAprvAccess -> {

            }
            is APIResultCompany -> {
//                if (response.dataStatus.size > 0) {
                if (listCompany.size > 0) listCompany.clear()
                listCompany.addAll(response.dataStatus)
                dataCompanyAdapter.notifyDataSetChanged()
                svString.visibility = View.VISIBLE
//                    etUsername.requestFocus()
//                } else {
//                    error(mContext, resources.getString(app_name), "Company not found.")
//                    etCompany.setText("")
//                    etCompany.requestFocus()
//                }
            }
            else -> {
                dismiss()
                if (message == "Company not found.")
                    error(mContext, resources.getString(app_name), "$message")
                else
                    error(mContext, resources.getString(app_name), "$message")
            }
        }
    }

    private fun requestPermission() {
        val permission = arrayOf(
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
        Permissions.check(this, permission, null, null, object : PermissionHandler() {
            override fun onGranted() {
                    repo.callTOKEN()
            }

            override fun onDenied(
                context: Context?,
                deniedPermissions: java.util.ArrayList<String>?
            ) {
                finish()
            }
        })
    }

    companion object {
        fun startThisActivity(ctx: Context, DataCompany: String?) {
            val intent =
                Intent(ctx, LoginActivity::class.java).putExtra("DataCompany", DataCompany)
            ctx.startActivity(intent)
        }
    }

    private fun checkRemaiderMe() {
        if (PrefHelper.getPref(mContext, "swReminder") == "Y") {
            login(mContext, mActivity, "Login Succedd")
        }
    }

    @SuppressLint("SetTextI18n")
    private fun getVersion() {
        try {
            val pInfo = packageManager.getPackageInfo(packageName, 0)
            val version = pInfo.versionName
            tvVersion.text = "Version $version"
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

    }

    /* private fun downloadFileFromBase64(fileContent: String?) {
         try {
             val file =
                 File(Environment.getExternalStorageDirectory().toString() + "/Aporisma/Picture/")
             if (!file.exists()) {
                 file.mkdirs()
                 if (fileContent != null) {
                     val attachment = parseBase64(fileContent)
                     val byteArr = Base64.decode(attachment, Base64.DEFAULT)
                     val f = File(file.absolutePath, "pp.jpg")
                     f.createNewFile()
                     val fo = FileOutputStream(f)
                     fo.write(byteArr)
                     fo.close()
                     //Toast.makeText(getApplicationContext(), "File downloaded", Toast.LENGTH_SHORT).show();
                 }
             }
         } catch (e: Exception) {
             e.printStackTrace()
         }

     }*/

}
