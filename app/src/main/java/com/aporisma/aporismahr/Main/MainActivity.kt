@file:Suppress("DEPRECATION", "NAME_SHADOWING")

package com.aporisma.aporismahr.Main

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Build
import android.provider.Settings
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aporisma.aporisma_api.Model.Company.DataStatus
import com.aporisma.aporisma_api.Model.Employee.APIResultGetAprvAccess
import com.aporisma.aporisma_api.Model.Employee.APIResultMasterEmployee
import com.aporisma.aporisma_api.Model.Employee.Data.DataStatusEmployee
import com.aporisma.aporisma_api.Model.Notification.APIResultAnnounc
import com.aporisma.aporisma_api.Model.Notification.APIResultNotification
import com.aporisma.aporisma_api.Model.Notification.APIResultReadNotif
import com.aporisma.aporisma_api.Model.Notification.Announcement
import com.aporisma.aporisma_api.Repo.Repo
import com.aporisma.aporisma_api.Response.BaseResponse
import com.aporisma.aporisma_api.Response.ResponseCourier
import com.aporisma.aporismahr.Adapter.AnnouncementAdapter
import com.aporisma.aporismahr.Adapter.NotificationAdapter
import com.aporisma.aporismahr.ApprovalActivity
import com.aporisma.aporismahr.Claim.ClaimActivity
import com.aporisma.aporismahr.Claim.ClaimDetailActivity
import com.aporisma.aporismahr.Claim.ClaimHeaderActivity
import com.aporisma.aporismahr.Helper.Dialog
import com.aporisma.aporismahr.Helper.Dialog.dismiss
import com.aporisma.aporismahr.Helper.Dialog.error
import com.aporisma.aporismahr.Helper.Dialog.loading
import com.aporisma.aporismahr.Helper.FindFakeApps
import com.aporisma.aporismahr.Helper.GettingLocationHelper.getLocationWithCheckNetworkAndGPS
import com.aporisma.aporismahr.Helper.PrefHelper
import com.aporisma.aporismahr.R
import com.aporisma.aporismahr.Timesheet.TimeSheetActivity
import com.aporisma.aporismahr.Timesheet.TimeSheetDetailsActivity
import com.aporisma.aporismahr.Timesheet.TimeSheetHeaderActivity
import com.aporisma.base_componen.BaseActivity
import com.aporisma.base_componen.setOnClick
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions
import kotlinx.android.synthetic.main.dialog_menu.*
import kotlinx.android.synthetic.main.main_activity.*
import kotlinx.android.synthetic.main.main_activity.btnReimApprov
import kotlinx.android.synthetic.main.main_activity.btnTSApprov


class MainActivity : BaseActivity(), ResponseCourier {

    var timesheetAMenu = false
    var reimbursmentAMenu = false

    @SuppressLint("SetTextI18n")
    override fun getResponse(response: BaseResponse<*>?, message: String?) {
        dismiss()
        when (response) {
            is APIResultMasterEmployee -> {
                dataStatusEmployee = response.dataStatus[0]
                repo.accessAproval(PrefHelper.getPref(mContext,"idCompany"),dataStatusEmployee.idEmployee)
            }
            is APIResultGetAprvAccess -> {
                repo.getlistNotif(dataStatusEmployee.idEmployee)
                repo.getAnnouncement(dataCompany.idCompany.toString())
                for (i in 0 until response.dataStatus.size) {
                    when (response.dataStatus[i].approvalType) {
                        "REIMBURSE" -> {
                            PrefHelper.saveToPref(mContext, "user", "admin")
                            timesheetAMenu = true
                        }
                        "TASK" -> {
                            PrefHelper.saveToPref(mContext, "user", "admin")
                            reimbursmentAMenu = true
                        }
                        else -> {
                            PrefHelper.saveToPref(mContext, "user", "")
                        }
                    }
                }
            }
            is APIResultNotification -> {
                if (listNotif.size > 0) listNotif.clear()
                listNotif.addAll(response.dataStatus)
                for (i in 0 until listNotif.size)
                    if (listNotif[i].isRead == "N") redNotif.visibility = View.VISIBLE
                    else redNotif.visibility = View.GONE
            }
            is APIResultReadNotif -> {
                repo.getlistNotif(dataStatusEmployee.idEmployee)
            }
            is APIResultAnnounc -> {
                if (listAnnouncement.size > 0) listAnnouncement.clear()
                listAnnouncement.addAll(response.dataStatus)
                announcementAdapter.notifyDataSetChanged()
            }
            else -> {
                if (message != "Notification Not Found"
                    && message != "Announcement Not Found"
                    && message != "Not Found")
                    error(mContext, resources.getString(R.string.app_name), "$message")
                else if (message == "Notification Not Found") ic_redDot.visibility = View.GONE
                if (message == "Announcement Not Found") {
                    ivAnnouncement.visibility = View.VISIBLE
                    cvAnnouncement.visibility = View.INVISIBLE
                }
            }
        }
    }

    lateinit var viewActivity: View
    lateinit var mContext: Context
    lateinit var repo: Repo
    val mActivity = this@MainActivity
    var dataStatusEmployee = DataStatusEmployee()
    var listNotif = ArrayList<com.aporisma.aporisma_api.Model.Notification.DataStatus>()
    var listAnnouncement = ArrayList<Announcement>()
    var dataCompany = DataStatus()
    lateinit var announcementAdapter: AnnouncementAdapter
    lateinit var notificationAdapter: NotificationAdapter

    @SuppressLint("SetTextI18n")
    override fun initCreate() {
        mContext = this
        repo = Repo(mContext, this)
        viewActivity = window.decorView.rootView
        dataStatusEmployee =
            PrefHelper.getObject(mContext, "dataEmployee", DataStatusEmployee::class.java)
        dataCompany = PrefHelper.getObject(mContext, "dataCompany", DataStatus::class.java)
        textView.text = dataCompany.namaCompany
        tvUser.text = "Hi! ${dataStatusEmployee.namaDepan} ${dataStatusEmployee.namaBelakang}"
        requestPermission()
        FindFakeApps(mContext, mActivity)
        turnGPSOn()
        initAdapter()
    }

    private fun turnGPSOn() {
        val provider =
            Settings.Secure.getString(contentResolver, Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        try {
            if (!provider.contains("gps")) { //if gps is disabled
                startActivityForResult(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0);
            } else {
                getLocationWithCheckNetworkAndGPS(mContext)
//                tvDate.text = date.toString()
                loading(mContext, repo, "Loading...")
                repo.accessAproval(PrefHelper.getPref(mContext,"idCompany"),dataStatusEmployee.idEmployee)
//                repo.getEmployeeData(dataStatusEmployee.idCompany.toString(), dataStatusEmployee.idEmployee.toString())
            }

        } catch (e: RuntimeException) {
            error(mContext, "RuntimeException", "$e")
        }
    }

    override fun getLayoutId(): Int = R.layout.main_activity

    override fun initDestroy() {
    }

    override fun onBackPressed() {
        Dialog.exit(mContext, "Are you sure want exit ?")
    }

    override fun onClick() {
        object :
            setOnClick(arrayOf(ivClaimD, ivTimesheet, btnTSApprov, btnReimApprov, iconUser)) {
            @SuppressLint("NewApi", "SetTextI18n")
            override fun doClick(view: View) {
                when (view.id) {
                    R.id.ivClaimD -> {
                        ClaimActivity.startThisActivity(mContext, dataStatusEmployee.idEmployee.toInt())
                    }
                    R.id.ivTimesheet -> {
                        TimeSheetActivity.startThisActivity(mContext)
                    }
                    R.id.btnTSApprov -> {
                        ApprovalActivity.startThisActivity(mContext, "T")
                    }
                    R.id.btnReimApprov -> {
                        ApprovalActivity.startThisActivity(mContext, "R")
                    }
                    R.id.iconUser -> {
                        menu()
                    }
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    fun menu() {
        try {
            val dialogBuilder = AlertDialog.Builder(mContext, R.style.DialogTheme)
            @SuppressLint("InflateParams") val dialogView =
                LayoutInflater.from(mContext).inflate(R.layout.dialog_menu, null)
            dialogBuilder.setView(dialogView)
            val btnReim = dialogView.findViewById<Button>(R.id.btnReim)
            val btnTS = dialogView.findViewById<Button>(R.id.btnTS)
            val llReimDMenu = dialogView.findViewById<LinearLayout>(R.id.llReimD)
            val llReimHMenu = dialogView.findViewById<LinearLayout>(R.id.llReimH)
            val llReimAMenu = dialogView.findViewById<LinearLayout>(R.id.llReimAMenu)
            val llTimesheetDMenu = dialogView.findViewById<LinearLayout>(R.id.llTimesheetDMenu)
            val llTimesheetHMenu = dialogView.findViewById<LinearLayout>(R.id.llTimesheetHMenu)
            val llTimesheetAMenu = dialogView.findViewById<LinearLayout>(R.id.llTimesheetAMenu)
            val btnClaimD = dialogView.findViewById<Button>(R.id.btnClaimD)
            val btnClaimH = dialogView.findViewById<Button>(R.id.btnClaimH)
            val btnReimApprov = dialogView.findViewById<Button>(R.id.btnReimApprov)
            val btnTimesheetD = dialogView.findViewById<Button>(R.id.btnTimesheetD)
            val btnTimesheetH = dialogView.findViewById<Button>(R.id.btnTimesheetH)
            val btnTSApprov = dialogView.findViewById<Button>(R.id.btnTSApprov)
            val btnLogout = dialogView.findViewById<Button>(R.id.btnLogout)
            val btnNotif = dialogView.findViewById<Button>(R.id.btnNotif)
            val icRedDot = dialogView.findViewById<TextView>(R.id.ic_redDot)
            val emplName = dialogView.findViewById<TextView>(R.id.emplName)
            val emplMail = dialogView.findViewById<TextView>(R.id.emplMail)
            val b = dialogBuilder.create()
            b.show()

            emplMail.setText(dataStatusEmployee.emplgroupName)
            emplName.setText("${dataStatusEmployee.namaDepan} ${dataStatusEmployee.namaBelakang}")

            val lp = WindowManager.LayoutParams()
            lp.copyFrom(b.window?.attributes)
            lp.gravity = Gravity.TOP
            lp.gravity = Gravity.END
            if (mContext.resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE)
                lp.width = (activity.width / 2.5).toInt()
            else lp.width = (activity.width / 1.5).toInt()
            lp.height = WindowManager.LayoutParams.MATCH_PARENT
            lp.windowAnimations = R.style.DialogAnimation_Right
            b.window?.attributes = lp

            if (listNotif.size > 0) {
                var totalNotif = 0
                for (i in 0 until listNotif.size) {
                    if (listNotif[i].isRead == "N") {
                        totalNotif += 1
                        icRedDot.visibility = View.VISIBLE
                    }
                }
                icRedDot.text = totalNotif.toString()
            }
            btnLogout.setOnClickListener {
                b.dismiss()
                Dialog.logout(mContext, mActivity, "Are you sure want exit ?")
            }
            btnNotif.setOnClickListener {
                b.dismiss()
                dialogNotif()
            }
            btnReim.setOnClickListener {
                if (btnReim.tag == "hide") {
                    llReimHMenu.visibility = View.VISIBLE
                    llReimDMenu.visibility = View.VISIBLE
//                    slideUp(llReimHMenu)
                    if (reimbursmentAMenu) /*slideUp(llReimAMenu)*/ llReimAMenu.visibility =
                        View.VISIBLE
                    btnReim.tag = "unhide"
                } else {
//                    slideDown(llReimHMenu)
                    llReimHMenu.visibility = View.GONE
                    llReimDMenu.visibility = View.GONE
                    if (reimbursmentAMenu) {
                        llReimAMenu.visibility = View.GONE
//                        slideDown(llReimAMenu)
                    }
                    btnReim.tag = "hide"
                }
            }
            btnTS.setOnClickListener {
                if (btnTS.tag == "hide") {
                    llTimesheetHMenu.visibility = View.VISIBLE
                    llTimesheetDMenu.visibility = View.VISIBLE
                    if (timesheetAMenu) llTimesheetAMenu.visibility = View.VISIBLE
                    btnTS.tag = "unhide"
                } else {
                    llTimesheetHMenu.visibility = View.GONE
                    llTimesheetDMenu.visibility = View.GONE
                    if (timesheetAMenu) llTimesheetAMenu.visibility = View.GONE
                    btnTS.tag = "hide"
                }
            }

            btnClaimD.setOnClickListener {
                b.dismiss()
                ClaimDetailActivity.startThisActivity(mContext, null)
            }
            btnClaimH.setOnClickListener {
                b.dismiss()
                ClaimHeaderActivity.startThisActivity(mContext, "")
            }

            btnReimApprov.setOnClickListener {
                b.dismiss()
                ApprovalActivity.startThisActivity(mContext, "R")
            }

            btnTimesheetD.setOnClickListener {
                b.dismiss()
                TimeSheetDetailsActivity.startThisActivity(mContext, "")
            }
            btnTimesheetH.setOnClickListener {
                b.dismiss()
                TimeSheetHeaderActivity.startThisActivity(mContext, "")
            }
            btnTSApprov.setOnClickListener {
                b.dismiss()
                ApprovalActivity.startThisActivity(mContext, "T")
            }
        } catch (e: Exception) {
            error(mContext, "ASCI-Ardi\nShowDialog", "error@BuildMessage\n$e\n${R.string.sendExceptionMessage}")
        }
    }

    fun dialogNotif() {
        val dialogBuilder = AlertDialog.Builder(mContext, R.style.DialogTheme)
        @SuppressLint("InflateParams") val dialogView =
            LayoutInflater.from(mContext).inflate(R.layout.dialog_notification, null)
        dialogBuilder.setView(dialogView)
        val d = dialogBuilder.create()
        val rvAnnouncement = dialogView.findViewById<RecyclerView>(R.id.rvAnnouncement)
        val lpd = WindowManager.LayoutParams()
        lpd.copyFrom(d.window?.attributes)
        lpd.gravity = Gravity.TOP
        lpd.windowAnimations = R.style.DialogAnimation_TopToBottom
        lpd.width = WindowManager.LayoutParams.MATCH_PARENT
        lpd.height = activity.height / 2
        d.window?.attributes = lpd
        initAdapterNotif(rvAnnouncement, d)
        d.show()
    }

    private fun requestPermission() {
        val permission = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET)
            } else {
                arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE, Manifest.permission.INTERNET, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
            }
        } else {
            arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE, Manifest.permission.INTERNET, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
        }

        Permissions.check(this, permission, null, null, object : PermissionHandler() {
            override fun onGranted() {
                Toast.makeText(mContext, "Permission granted", Toast.LENGTH_SHORT).show()
            }

            override fun onDenied(context: Context?, deniedPermissions: ArrayList<String>?) {
                finish()
            }
        })
    }

    companion object {

        fun startThisActivity(ctx: Context) {
            val intent = Intent(ctx, MainActivity::class.java)
            ctx.startActivity(intent)
        }
    }

    private fun initAdapter() {
        announcementAdapter = AnnouncementAdapter(listAnnouncement, this)
        val mLayoutManager = LinearLayoutManager(this)
        rvAnnouncement.layoutManager = mLayoutManager
        rvAnnouncement.itemAnimator = DefaultItemAnimator()
        rvAnnouncement.adapter = announcementAdapter
        announcementAdapter.notifyDataSetChanged()
    }

    private fun initAdapterNotif(recyclerView: RecyclerView, dialog: AlertDialog) {
        notificationAdapter = NotificationAdapter(listNotif, this, dialog)
        val mLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = mLayoutManager
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.adapter = notificationAdapter
        notificationAdapter.notifyDataSetChanged()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 0) {
            turnGPSOn()
        }
    }

    /* fun slideUp(view: View) {
         view.setVisibility(View.VISIBLE);
         val animate = TranslateAnimation(
             0f,                 // fromXDelta
             0f,                 // toXDelta
             view.getHeight().toFloat(),  // fromYDelta
             0f
         );                // toYDelta
         animate.setDuration(500);
         animate.setFillAfter(true);
         view.startAnimation(animate);
         view.visibility = View.VISIBLE
     }

     fun slideDown(view: View) {
         val animate = TranslateAnimation(
             0f,                 // fromXDelta
             0f,                 // toXDelta
             0f,                 // fromYDelta
             view.getHeight().toFloat()
         ); // toYDelta
         animate.setDuration(500);
         animate.setFillAfter(true);
         view.startAnimation(animate);
         view.visibility = View.GONE
     }
 */
}
