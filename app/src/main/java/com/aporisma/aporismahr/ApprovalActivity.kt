package com.aporisma.aporismahr

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aporisma.aporisma_api.Model.Approval.APIResult
import com.aporisma.aporisma_api.Model.Approval.getRow.APIResultApvTS
import com.aporisma.aporisma_api.Model.Approval.getRow.APIResultRApvReim
import com.aporisma.aporisma_api.Model.Approval.getRow.ApvReim
import com.aporisma.aporisma_api.Model.Approval.getRow.ApvTS
import com.aporisma.aporisma_api.Model.Employee.Data.DataStatusEmployee
import com.aporisma.aporisma_api.Repo.Repo
import com.aporisma.aporisma_api.Response.BaseResponse
import com.aporisma.aporisma_api.Response.ResponseCourier
import com.aporisma.aporismahr.Adapter.RowApprovalReimAdapter
import com.aporisma.aporismahr.Adapter.RowApprovalTSAdapter
import com.aporisma.aporismahr.Helper.Dialog
import com.aporisma.aporismahr.Helper.PrefHelper
import com.aporisma.base_componen.BaseActivity
import com.aporisma.base_componen.setOnClick
import kotlinx.android.synthetic.main.approval_activity.*

class ApprovalActivity : BaseActivity(), ResponseCourier {

    val mContext = this@ApprovalActivity
    val mActivity = this@ApprovalActivity
    val repo = Repo(this, this)
    private lateinit var RowApvReimAdapter: RowApprovalReimAdapter
    private lateinit var RowApvTSAdapter: RowApprovalTSAdapter
    private var listApprovalReim = ArrayList<ApvReim>()
    private var listApprovalTS = ArrayList<ApvTS>()
    private var tipe: String? = ""

    @SuppressLint("SetTextI18n")
    override fun initCreate() {
        Dialog.loading(mContext,repo,"Loading...")
        val dataEmployee =
            PrefHelper.getObject(mContext, "dataEmployee", DataStatusEmployee::class.java)
        val intent = intent
        val getExtraData = intent.extras
        if (getExtraData != null) {
            val stringExtra = getExtraData.get("tipe") as String?
            if (stringExtra == "R") {
                tipe = stringExtra
                tvTitle.text = "Approval\nReimburse"
                repo.getRowReimApv(PrefHelper.getPref(mContext,"idCompany"))
                rowClaimApvAdapter()
            } else {
                tipe = stringExtra
                tvTitle.text = "Approval\nTimesheet"
                repo.getRowTSApv(PrefHelper.getPref(mContext,"idCompany"))
                rowTSApvAdapter()
            }
        }
    }

    override fun getLayoutId(): Int = R.layout.approval_activity

    override fun initDestroy() {
    }

    override fun onRestart() {
        super.onRestart()
        if (listApprovalReim.size > 0) listApprovalReim.clear()
        if (listApprovalTS.size > 0) listApprovalTS.clear()
        initCreate()
    }

    override fun onClick() {
        object : setOnClick(arrayOf(iv_back)) {
            @SuppressLint("NewApi", "SetTextI18n")
            override fun doClick(view: View) {
                when (view.id) {
                    R.id.iv_back -> finish()
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    override fun getResponse(response: BaseResponse<*>?, message: String?) {
        Dialog.dismiss()
        if (response is APIResultRApvReim) {
            if(listApprovalReim.size > 0)listApprovalReim.clear()
            listApprovalReim.addAll(response.dataStatus)
            RowApvReimAdapter.notifyDataSetChanged()
        }
        else if(response is APIResultApvTS){
            if(listApprovalTS.size > 0)listApprovalTS.clear()
            listApprovalTS.addAll(response.dataStatus)
            RowApvTSAdapter.notifyDataSetChanged()
        }
        else if(response is APIResult){
            Dialog.succedd(mContext,resources.getString(R.string.app_name),message.toString())
            val intent = intent
            val getExtraData = intent.extras
            if (getExtraData != null) {
                val stringExtra = getExtraData.get("tipe") as String?
                if (stringExtra == "R") {
                    tipe = stringExtra
                    tvTitle.text = "Approval\nReimburse"
                    repo.getRowReimApv(PrefHelper.getPref(mContext,"idCompany"))
                    rowClaimApvAdapter()
                } else {
                    tipe = stringExtra
                    tvTitle.text = "Approval\nTimesheet"
                    repo.getRowTSApv(PrefHelper.getPref(mContext,"idCompany"))
                    rowTSApvAdapter()
                }
            }
        }
        else {
            if(message == "Payment Reimburse succed") {
                try {
                    val dialogBuilder = AlertDialog.Builder(mContext)
                    @SuppressLint("InflateParams") val dialogView =
                        LayoutInflater.from(mContext).inflate(R.layout.dialog_message, null)
                    dialogBuilder.setView(dialogView)

                    val etSetting = dialogView.findViewById<EditText>(R.id.etSetting)
                    val tvSetting = dialogView.findViewById<TextView>(R.id.tvSetting)
                    val tvSettingTitle = dialogView.findViewById<TextView>(R.id.tvSettingTitle)
                    val btnOK = dialogView.findViewById<Button>(R.id.btnOK)
                    val btnCancel = dialogView.findViewById<Button>(R.id.btnCancel)
                    val b = dialogBuilder.create()
                    b.show()
                    b.setCanceledOnTouchOutside(false)
                    b.setCancelable(false)

                    val lp = WindowManager.LayoutParams()
                    lp.copyFrom(b.window?.attributes)
                    lp.width = WindowManager.LayoutParams.MATCH_PARENT
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT
                    lp.windowAnimations = R.style.DialogAnimation

                    b.window?.attributes = lp

                    tvSetting.textAlignment = View.TEXT_ALIGNMENT_CENTER
                    etSetting.visibility = View.GONE
                    btnCancel.visibility = View.GONE
                    tvSetting.text = message
                    tvSettingTitle.text = mContext.resources.getString(R.string.app_name)
                    btnOK.text = "OK"
                    btnOK.setOnClickListener {
                        b.dismiss()
                        onRestart()
                    }
                } catch (e: Exception) {
                    Dialog.error(mContext, "ASCI-Ardi\nShowDialog", "error@BuildMessage\n$e\n${R.string.sendExceptionMessage}")
                }
            }
            else Dialog.error(mContext,resources.getString(R.string.app_name),message.toString())
        }
    }

    companion object {

        fun startThisActivity(ctx: Context, tipe: String) {
            val intent = Intent(ctx, ApprovalActivity::class.java).putExtra("tipe", tipe)
            ctx.startActivity(intent)
        }
    }

    private fun rowClaimApvAdapter() {
        RowApvReimAdapter = RowApprovalReimAdapter(listApprovalReim, this)
        val mLayoutManager = LinearLayoutManager(this)
        rvEmplD.setLayoutManager(mLayoutManager as RecyclerView.LayoutManager?)
        rvEmplD.setItemAnimator(DefaultItemAnimator())
        rvEmplD.setAdapter(RowApvReimAdapter)
        RowApvReimAdapter.notifyDataSetChanged()
    }
    private fun rowTSApvAdapter() {
        RowApvTSAdapter = RowApprovalTSAdapter(listApprovalTS, this)
        val mLayoutManager = LinearLayoutManager(this)
        rvEmplD.setLayoutManager(mLayoutManager as RecyclerView.LayoutManager?)
        rvEmplD.setItemAnimator(DefaultItemAnimator())
        rvEmplD.setAdapter(RowApvTSAdapter)
        RowApvTSAdapter.notifyDataSetChanged()
    }



}
