@file:Suppress("DEPRECATION")

package com.aporisma.aporismahr.Claim

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aporisma.aporisma_api.Model.Param.APIResultParam
import com.aporisma.aporisma_api.Model.Param.DataStatus
import com.aporisma.aporisma_api.Model.Reimbursement.APIResultListReimHeader
import com.aporisma.aporisma_api.Model.Reimbursement.APIResultReimDetail
import com.aporisma.aporisma_api.Model.Reimbursement.DataStatus.ReimbursementDetails
import com.aporisma.aporisma_api.Model.Reimbursement.DataStatus.ReimbursementHeader
import com.aporisma.aporisma_api.Repo.Repo
import com.aporisma.aporisma_api.Response.BaseResponse
import com.aporisma.aporisma_api.Response.ResponseCourier
import com.aporisma.aporismahr.Adapter.HistoryReimDetail
import com.aporisma.aporismahr.Adapter.HistoryReimHeader
import com.aporisma.aporismahr.Helper.Dialog
import com.aporisma.aporismahr.Helper.Dialog.error
import com.aporisma.aporismahr.Helper.Dialog.loading
import com.aporisma.aporismahr.Helper.PrefHelper
import com.aporisma.aporismahr.R
import com.aporisma.base_componen.BaseActivity
import com.aporisma.base_componen.setOnClick
import kotlinx.android.synthetic.main.claim_activity.*


class ClaimActivity : BaseActivity(), ResponseCourier {
    override fun getResponse(response: BaseResponse<*>?, message: String?) {
        Dialog.dismiss()
        try {
            when {
                response is APIResultReimDetail && response.dataStatus.size > 0 -> {
                    if (listReimDetail.size > 0) listReimDetail.clear()
                    for (i in 0 until response.dataStatus.size) {
                        if (response.dataStatus[i].idForm == null) listReimDetail.add(response.dataStatus[i])
                    }
                    historyReimDetail.notifyDataSetChanged()
                }
                response is APIResultListReimHeader && response.dataStatus.size > 0 -> {
                    //                Dialog.dismiss()
                    if (listReimHeader.size > 0) listReimHeader.clear()
                    listReimHeader.addAll(response.dataStatus)
                    historyReimHeader.notifyDataSetChanged()
                }
                response is APIResultParam -> {
                    listDataParam.addAll(response.dataStatus)
                    detailKlaimInitAdapter()
                }
                else -> {
                    //                Dialog.dismiss()
                    Dialog.error(mContext, resources.getString(R.string.app_name), "$message")
                }
            }
        } catch (e: Exception) {
            error(mContext, "ASCI-Ardi", "error@btnTambahKlaim\n$e")
        }
    }

    var mContext = this@ClaimActivity
    var repo = Repo(mContext, this)
    var listReimDetail = ArrayList<ReimbursementDetails>()
    var listReimHeader = ArrayList<ReimbursementHeader>()
    lateinit var historyReimDetail: HistoryReimDetail
    lateinit var historyReimHeader: HistoryReimHeader
    val listDataParam = ArrayList<DataStatus>()

    override fun onRestart() {
        super.onRestart()
        if (listReimDetail.size > 0) listReimDetail.clear()
        if (listReimHeader.size > 0) listReimHeader.clear()
        if (listDataParam.size > 0) listDataParam.clear()
        initCreate()
    }

    override fun initCreate() {
        detailKlaimInitAdapter()
    }

    override fun getLayoutId(): Int = R.layout.claim_activity

    override fun initDestroy() {
    }

    override fun onClick() {
        object : setOnClick(arrayOf(tvKlaim, tvFormKlaim, btnTambahKlaim,iv_back)) {
            @SuppressLint("NewApi", "SetTextI18n")
            override fun doClick(view: View) {
                when (view.id) {
                    R.id.tvKlaim -> {
                        detailKlaimInitAdapter()
                    }
                    R.id.tvFormKlaim -> {
                        headerKlaimInitAdapter()
                    }
                    R.id.btnTambahKlaim -> {
                        try {
                            (if (tvSubject.text == resources.getString(R.string.tambah_klaim)) ClaimDetailActivity.startThisActivity(mContext, null)
                            else ClaimHeaderActivity.startThisActivity(mContext, ""))
                        } catch (e: Exception) {
                            error(mContext, "ASCI-Ardi", "error@btnTambahKlaim\n$e")

                        }
                    }
                    R.id.iv_back -> finish()
                }
            }
        }
    }

    fun detailKlaimInitAdapter() {
        try {
            val intent = intent
            val getExtraData = intent.extras
            if (getExtraData != null) {
                val StringExtra = getExtraData.get("id_employee") as Int?
                repo.getReimDetail(StringExtra.toString())
                loading(mContext, repo, "Loading...")
            }
            tvKlaim.setTextColor(resources.getColor(android.R.color.white))
            tvKlaim.backgroundTintList =
                mContext.resources.getColorStateList(android.R.color.holo_blue_light)
            svReim.visibility = View.VISIBLE
            tvSubject.text = getString(R.string.tambah_klaim)

            tvFormKlaim.setTextColor(resources.getColor(android.R.color.white))
            tvFormKlaim.backgroundTintList =
                mContext.resources.getColorStateList(android.R.color.darker_gray)
            svFormReim.visibility = View.GONE

            historyReimDetail = HistoryReimDetail(listReimDetail, this)
            val mLayoutManager = LinearLayoutManager(this)
            rvKlaim.setLayoutManager(mLayoutManager as RecyclerView.LayoutManager?)
            rvKlaim.setItemAnimator(DefaultItemAnimator())
            rvKlaim.setAdapter(historyReimDetail)
            historyReimDetail.notifyDataSetChanged()
        } catch (e: Exception) {
            error(mContext, "ASCI-Ardi", "error@detailKlaimInitAdapter\n$e")
        }
    }

    fun headerKlaimInitAdapter() {
        try {
            val intent = intent
            val getExtraData = intent.extras
            if (getExtraData != null) {
                val StringExtra = getExtraData.get("id_employee") as Int?
                repo.getReimHeader(StringExtra.toString(),PrefHelper.getPref(mContext,"idCompany"))
                loading(mContext, repo, "Loading...")
            }

            tvKlaim.setTextColor(resources.getColor(android.R.color.white))
            tvKlaim.backgroundTintList =
                mContext.resources.getColorStateList(android.R.color.darker_gray)
            svReim.visibility = View.GONE
            tvSubject.text = getString(R.string.tambah_form_klaim)

            tvFormKlaim.setTextColor(resources.getColor(android.R.color.white))
            tvFormKlaim.backgroundTintList =
                mContext.resources.getColorStateList(android.R.color.holo_blue_light)
            svFormReim.visibility = View.VISIBLE

            historyReimHeader = HistoryReimHeader(listReimHeader, this)
            val mLayoutManager = LinearLayoutManager(this)
            rvFormKlaim.setLayoutManager(mLayoutManager as RecyclerView.LayoutManager?)
            rvFormKlaim.setItemAnimator(DefaultItemAnimator())
            rvFormKlaim.setAdapter(historyReimHeader)
            historyReimHeader.notifyDataSetChanged()
        } catch (e: java.lang.Exception) {
            error(mContext, "ASCI-Ardi", "error@headerKlaimInitAdapter\n$e")
        }
    }

    companion object {
        fun startThisActivity(ctx: Context, id_employee: Int) {
            val intent = Intent(ctx, ClaimActivity::class.java).putExtra("id_employee", id_employee)
            ctx.startActivity(intent)
        }
    }
}
