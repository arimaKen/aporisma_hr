package com.aporisma.aporismahr.Claim

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.provider.MediaStore
import android.text.Editable
import android.text.Html
import android.text.InputType
import android.text.TextWatcher
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.*
import com.aporisma.aporisma_api.Model.BaseModel
import com.aporisma.aporisma_api.Model.Employee.APIResultGroupE
import com.aporisma.aporisma_api.Model.Employee.Data.DataStatusEmployee
import com.aporisma.aporisma_api.Model.Param.APIResultParam
import com.aporisma.aporisma_api.Model.Param.DataStatus
import com.aporisma.aporisma_api.Model.Reimbursement.APIResultReimDetail
import com.aporisma.aporisma_api.Model.Reimbursement.DataStatus.ReimbursementDetails
import com.aporisma.aporisma_api.Repo.Repo
import com.aporisma.aporisma_api.Response.BaseResponse
import com.aporisma.aporisma_api.Response.ResponseCourier
import com.aporisma.aporismahr.Helper.DateHelper
import com.aporisma.aporismahr.Helper.Dialog.dismiss
import com.aporisma.aporismahr.Helper.Dialog.error
import com.aporisma.aporismahr.Helper.Dialog.errorFinish
import com.aporisma.aporismahr.Helper.Dialog.loading
import com.aporisma.aporismahr.Helper.Dialog.succeddFinish
import com.aporisma.aporismahr.Helper.GetBase64.fromBitmapImage
import com.aporisma.aporismahr.Helper.GetBase64.fromFilePath
import com.aporisma.aporismahr.Helper.PrefHelper
import com.aporisma.aporismahr.Helper.getRealPathFromURI
import com.aporisma.aporismahr.Helper.indo
import com.aporisma.aporismahr.R
import com.aporisma.base_componen.BaseActivity
import com.aporisma.base_componen.setOnClick
import com.google.gson.Gson
import kotlinx.android.synthetic.main.claim_detail_activity.*
import java.io.File


@Suppress("DEPRECATION", "ReplaceSizeCheckWithIsNotEmpty")
class ClaimDetailActivity : BaseActivity(), ResponseCourier {

    override fun getResponse(response: BaseResponse<*>?, message: String?) {
        when (response) {
            is APIResultParam -> {
                for (i in 0 until response.dataStatus.size) {
                    spinnerArray.add(response.dataStatus[i].paramCode)
                }
                dataParams.addAll(response.dataStatus)
                insertSpinnerTipe()
                getDataExtra()
                repo.getGReim(PrefHelper.getPref(mContext,"idCompany"),dataEmployee.idEmployee)
            }
            is APIResultReimDetail -> succeddFinish(mContext, mActivity, "$message")
            is APIResultGroupE -> etGroup.setText(response.dataStatus.approvalTplName)
            is BaseModel -> succeddFinish(mContext, mActivity, "$message")
            else ->
                if (message == "You dont have group for approved") errorFinish(mContext, mActivity, message)
                else if (message == "Parameter Reimburse is null\nPlease insert from web by super user") errorFinish(mContext, mActivity, message)
                else error(mContext, resources.getString(R.string.app_name), message.toString())
        }
        dismiss()
    }

    val mActivity = this@ClaimDetailActivity
    var mContext = this@ClaimDetailActivity
    val repo = Repo(mContext, this@ClaimDetailActivity)
    val spinnerArray = ArrayList<String>()
    val dataParams = ArrayList<DataStatus>()
    lateinit var detailReim: ReimbursementDetails
    lateinit var dataEmployee: DataStatusEmployee

    @SuppressLint("SetTextI18n")
    override fun initCreate() {
        etDate.showSoftInputOnFocus = false
        etTotal.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (etTotal.text.toString().matches(Regex("^0"))) etTotal.setText("")
            }

        })
        repo.getTypeClaim(PrefHelper.getPref(mContext, "idCompany"))
        dataEmployee =
            PrefHelper.getObject(mContext, "dataEmployee", DataStatusEmployee::class.java)
        tvUser.text = dataEmployee.namaDepan + " " + dataEmployee.namaBelakang
        etDate.setText(DateHelper.now("yyyy-MM-dd"))
        etDate.showSoftInputOnFocus = false
        etTotal.setOnFocusChangeListener { _v, _b ->
            if (_b) etTotal.setText("") }
        loading(mContext, repo, "Loading...")
    }

    @SuppressLint("SetTextI18n")
    fun getDataExtra() {
        val intent = intent
        val getExtraData = intent.extras
        if (getExtraData != null) {
            try {
                val stringExtra = getExtraData.get("data") as String?
                if (stringExtra != null) {
                    detailReim = Gson().fromJson(stringExtra, ReimbursementDetails::class.java)
                    if (detailReim.pathLocal != null) {
                        if (detailReim.pathLocal != "") {
                            imageurl = detailReim.pathLocal
                            val file = File(detailReim.pathLocal)
                            if (file.exists()) {
                                ivFoto.setImageDrawable(Drawable.createFromPath(detailReim.pathLocal))
                                ivFoto.visibility = View.VISIBLE
                            } else {
                                error(mContext, resources.getString(R.string.app_name), "Foto kemungkinan sudah terhapus dari memory internal")
                            }
                        }
                    }
                    etTotal.setText(indo.currency.format(detailReim.nominal.toDouble()))
                    etDate.setText(detailReim.tanggal.split(" ")[0])
                    etUpload.setText(detailReim.pathLocal)
                    etDesc.setText(detailReim.catatan)
                    for (i in 0 until dataParams.size) {
                        if (dataParams[i].paramCode.equals(detailReim.tipeValue)) spType.setSelection(i)
                    }
                    etNote.setText(Html.fromHtml(detailReim.catatan.toString()))
                    btnAddKlaim.text = "Update"
                    llDel.visibility = View.VISIBLE
                }
            } catch (e: java.lang.Exception) {
                error(mContext, resources.getString(R.string.app_name), e.toString())
            }
        }
    }

    override fun getLayoutId(): Int = R.layout.claim_detail_activity

    override fun initDestroy() {
    }

    companion object {
        fun startThisActivity(ctx: Context, data: String?) {
            val intent = Intent(ctx, ClaimDetailActivity::class.java).putExtra("data", data)
            ctx.startActivity(intent)
        }
    }

    override fun onClick() {
        object : setOnClick(arrayOf(ivFoto, ivCal, btnAddKlaim, ivUpload, btnDel,iv_back)) {
            @SuppressLint("NewApi", "SetTextI18n")
            override fun doClick(view: View) {
                when (view.id) {
                    R.id.ivUpload -> {
                        capturePhoto()
                    }
                    R.id.ivFoto -> {
                        capturePhoto()
                    }
                    R.id.ivCal -> {
                        DateHelper.datePicker(mContext, etDate, "Y")
                    }
                    R.id.btnAddKlaim -> {
                        try {
                            if (etTotal.text.toString() != "") {
                                var match = false
                                var maxValue = 0
                                val currentValue: Int =
                                    if (etTotal.text.toString().split("Rp").size > 1)
                                        etTotal.text.toString().split("Rp")[1].replace(".", "").toInt()
                                    else etTotal.text.toString().toInt()


                                for (i in 0 until dataParams.size) {
                                    if (spType.selectedItem.equals(dataParams[i].paramCode)) {
                                        match = true
                                        maxValue = dataParams[i].paramValue.toInt()
                                    }
                                }
                                if (match && maxValue >= currentValue) {
                                    inputNote()
                                } else {
                                    error(mContext, resources.getString(R.string.app_name), "The amount you entered exceeds the conditions")
                                }
                            } else {
                                error(mContext, resources.getString(R.string.app_name), "Please insert you amount")
                            }
                        } catch (e: java.lang.Exception) {
                            error(mContext, "ASCI-Ardi", "error@AddKlaim\n$e")
                        }
                    }
                    R.id.btnDel -> {
                        repo.delReimD(detailReim.idReim.toString())
                    }
                    R.id.iv_back -> finish()
                }
            }
        }
    }

    lateinit var imageUri: Uri
    private val takePicture = 115
    private val insertImage = 116
    var imageurl: String = ""
    private var bitmap: Bitmap? = null

    private fun capturePhoto() {
        try {
            if (ivFoto.drawable == null) {
                chooseInsertImage()
            } else {
                openPreview()
            }
        } catch (e: Exception) {
            error(mContext, "ASCI-Ardi", "error@CapturePhoto\n$e")
        }
    }

    @SuppressLint("SetTextI18n")
    fun chooseInsertImage() {
        try {
            val dialogBuilder = AlertDialog.Builder(mContext)
            @SuppressLint("InflateParams") val dialogView =
                LayoutInflater.from(mContext).inflate(R.layout.dialog_choose_image, null)
            dialogBuilder.setView(dialogView)

            val tvTitel = dialogView.findViewById<TextView>(R.id.tvTitel)
            val btnFromCamera = dialogView.findViewById<Button>(R.id.btnFromCamera)
            val btnFromGallery = dialogView.findViewById<Button>(R.id.btnFromGallery)
            val btnDel = dialogView.findViewById<Button>(R.id.btnDel)
            val btnClose = dialogView.findViewById<Button>(R.id.btnClose)
            val b = dialogBuilder.create()
            tvTitel.text = "Insert Picture"
            b.setCanceledOnTouchOutside(false)
            b.show()

            val lp = WindowManager.LayoutParams()
            lp.copyFrom(b.window?.attributes)
            lp.gravity = Gravity.BOTTOM
            lp.width = WindowManager.LayoutParams.FILL_PARENT
            lp.windowAnimations = R.style.DialogAnimation
            b.window?.attributes = lp

            if (etUpload.text.toString() == "") btnDel.visibility = View.GONE

            btnFromCamera.setOnClickListener {
                try {
                    val values = ContentValues()
                    values.put(MediaStore.Images.Media.TITLE, "New Picture")
                    values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera")
                    imageUri =
                        contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)!!
                    val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
                    startActivityForResult(intent, takePicture)
                    b.dismiss()
                } catch (e: java.lang.Exception) {
                    error(mContext, "ASCI-Ardi", "error@getImageFromCamera\n$e")
                }
            }
            btnFromGallery.setOnClickListener {
                try {
                    val intent = Intent()
                    intent.type = "image/*"
                    intent.action = Intent.ACTION_GET_CONTENT
                    startActivityForResult(
                        Intent.createChooser(intent, "Select image"), insertImage
                    )
                    b.dismiss()
                } catch (e: java.lang.Exception) {
                    error(mContext, "ASCI-Ardi", "error@getImageFromGallery\n$e")
                }
            }
            btnClose.setOnClickListener {
                b.dismiss()
            }
            btnDel.setOnClickListener {
                imageurl = ""
                bitmap = null
                etUpload.setText("")
                ivFoto.setImageDrawable(null)
                ivFoto.visibility = View.GONE
                b.dismiss()
            }

        } catch (e: Exception) {
            error(mContext, "ASCI-Ardi", "error@chooseInsertImage\n$e")
        }
    }

    private fun openPreview() {
        try {
            val dialogBuilder = AlertDialog.Builder(mContext)
            @SuppressLint("InflateParams") val dialogView =
                LayoutInflater.from(mContext).inflate(R.layout.dialog_preview, null)
            dialogBuilder.setView(dialogView)
            val ivPreview = dialogView.findViewById<ImageView>(R.id.ivPreview)
            val btnUbah = dialogView.findViewById<Button>(R.id.btnChange)
            val btnClose = dialogView.findViewById<Button>(R.id.btnClose)
            val b = dialogBuilder.create()

            val lp = WindowManager.LayoutParams()
            lp.copyFrom(b.window?.attributes)
            lp.gravity = Gravity.BOTTOM
            lp.width = WindowManager.LayoutParams.FILL_PARENT
            lp.windowAnimations = R.style.DialogAnimation
            b.window?.attributes = lp
            b.show()
            ivPreview.setImageDrawable(ivFoto.drawable)
            btnUbah.setOnClickListener {
                b.dismiss()
                chooseInsertImage()
            }
            btnClose.setOnClickListener {
                b.dismiss()
            }
        } catch (e: Exception) {
            error(
                mContext, "ASCI-Ardi\nShowDialog", "error@openPreview\n$e}"
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (resultCode == RESULT_OK && requestCode == takePicture) {
                bitmap = MediaStore.Images.Media.getBitmap(contentResolver, imageUri)
                imageurl = getRealPathFromURI(this@ClaimDetailActivity, imageUri)
                val file = File(imageurl)
                if (file.exists()) {
                    etUpload.setText(imageurl)
                    ivFoto.visibility = View.VISIBLE
                    ivFoto.setImageBitmap(bitmap)
                }
            } else if (resultCode == RESULT_OK && requestCode == insertImage && data != null) {
                imageUri = data.data!!
                bitmap = MediaStore.Images.Media.getBitmap(contentResolver, imageUri)
                imageurl = getRealPathFromURI(this@ClaimDetailActivity, imageUri)
                etUpload.setText(imageurl)
                ivFoto.visibility = View.VISIBLE
                ivFoto.setImageBitmap(bitmap)

            }
        } catch (e: Exception) {
            error(mContext, "ASCI-Ardi", "error@ActivityResult\n $e")
        }
    }

    private fun insertSpinnerTipe() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, spinnerArray)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        val sItems = findViewById<View>(R.id.spType) as Spinner
        sItems.adapter = adapter
    }

    @SuppressLint("SetTextI18n")
    fun inputNote() {
        try {
            val dialogBuilder = AlertDialog.Builder(mContext)
            @SuppressLint("InflateParams") val dialogView =
                LayoutInflater.from(mContext).inflate(R.layout.dialog_message, null)
            dialogBuilder.setView(dialogView)

            val etSetting = dialogView.findViewById<EditText>(R.id.etSetting)
            val tvSetting = dialogView.findViewById<TextView>(R.id.tvSetting)
            val tvSettingTitle = dialogView.findViewById<TextView>(R.id.tvSettingTitle)
            val btnOK = dialogView.findViewById<Button>(R.id.btnOK)
            val btnCancel = dialogView.findViewById<Button>(R.id.btnCancel)
            val b = dialogBuilder.create()
            b.show()
            b.setCanceledOnTouchOutside(false)
            b.setCancelable(false)


            val lp = WindowManager.LayoutParams()
            lp.copyFrom(b.window?.attributes)

            lp.width = WindowManager.LayoutParams.MATCH_PARENT

            lp.height = WindowManager.LayoutParams.WRAP_CONTENT

            lp.windowAnimations = R.style.DialogAnimation

            b.window?.attributes = lp

            tvSetting.textAlignment = View.TEXT_ALIGNMENT_CENTER
            etSetting.hint = "Insert note here!"
            etSetting.visibility = View.GONE
            tvSetting.text = "Are you sure wan't save ?"
            etSetting.inputType = InputType.TYPE_TEXT_FLAG_MULTI_LINE
            tvSettingTitle.text = mContext.resources.getString(R.string.app_name)
            btnCancel.setOnClickListener {
                b.dismiss()
            }
            btnOK.setOnClickListener {
                try {

                   /* if (etSetting.text.toString() != "") {*/
                        val dataEmployee: DataStatusEmployee =
                            PrefHelper.getObject(mContext, "dataEmployee", DataStatusEmployee::class.java)

                        val newReim = ReimbursementDetails()
//                        detailReim = ReimbursementDetails()
                        newReim.idEmployee = dataEmployee.idEmployee.toInt()
                        if (etTotal.text.toString().split("Rp").size > 1) newReim.nominal =
                            Integer.valueOf(etTotal.text.toString().split("Rp")[1].replace(".", ""))
                        else newReim.nominal = Integer.valueOf(etTotal.text.toString())
                        newReim.tanggal =
                            etDate.text.toString() + "T" + DateHelper.now("HH:mm:ss")
                        for (i in 0 until dataParams.size) {
                            if (dataParams[i].paramCode.equals(spType.selectedItem.toString())) {
                                newReim.tipe = dataParams[i].paramId.toString()
                            }
                        }
                        /*spType.selectedItem.toString()*/ /*PrefHelper.getPref(mContext,"id_tipe")*/
                        newReim.catatan = etDesc.text.toString()  /*etSetting.text.toString()*/
                        newReim.pathLocal = imageurl
                        if (imageurl != "") {
                            if (bitmap != null) {
                                newReim.photo = fromBitmapImage(mContext, bitmap)
                                newReim.photoType =
                                    imageurl.substring(imageurl.lastIndexOf("."));
                            } else {
                                newReim.photo = fromFilePath(mContext, File(imageurl))
                                newReim.photoType =
                                    imageurl.substring(imageurl.lastIndexOf("."));
                            }
                        } else {
                            newReim.photo = ""
                            newReim.photoType = ""
                        }
                        newReim.selected = null
                        newReim.createdUser =
                            dataEmployee.namaDepan + " " + dataEmployee.namaBelakang
                        val s = Gson().toJson(newReim)
                        if (btnAddKlaim.text == "Save") {
                            repo.addReimDetail(s)
                            loading(mContext, repo, "Loading...")
                        } else {
                            newReim.idReim = detailReim.idReim
                            if (detailReim.status == "R") newReim.status = "W"
                            else newReim.status = detailReim.status
                            /*val newReim = ReimbursementDetails()
                    newReim.idReim = detailReim.idReim
                    newReim.idEmployee = dataEmployee.idEmployee.toInt()
                    if (imageurl != "") {
                        newReim.photo = fromBitmapImage(mContext, bitmap)
                    } else {
                        newReim.photo = ""
                    }
                    newReim.nominal = Integer.valueOf(etTotal.text.toString())
                    newReim.pathLocal = imageurl
                    newReim.tipe = spType.selectedItem.toString()
                    newReim.catatan = etNote.text.toString()
                    newReim.tanggal = etDate.text.toString()
                    newReim.updateUser =
                        dataEmployee.namaDepan + " " + dataEmployee.namaBelakang*/
//                                Log.e("JSON ---->",Gson().toJson(newReim))
                            repo.updateReimDetail(Gson().toJson(newReim))
                            loading(mContext, repo, "Loading...")
                        }
                  /*  } else if (etSetting.text.toString() == "") {
                        error(mContext, resources.getString(R.string.app_name), "Please insert note")
                    }*/
                } catch (e: Exception) {
                    error(mContext, "ASCI-Ardi", "error@gJsonS\n$e")
                }
                b.dismiss()
            }
        } catch (e: Exception) {
            error(mContext, "ASCI-Ardi\nShowDialog", "error@BuildMessage\n$e\n${R.string.sendExceptionMessage}")
        }
    }

}
