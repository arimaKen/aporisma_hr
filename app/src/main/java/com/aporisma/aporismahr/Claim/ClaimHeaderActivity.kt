package com.aporisma.aporismahr.Claim

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.aporisma.aporisma_api.Model.Approval.APIResult
import com.aporisma.aporisma_api.Model.Approval.sendReim.SendApvReim
import com.aporisma.aporisma_api.Model.Employee.APIResultGroupE
import com.aporisma.aporisma_api.Model.Employee.Data.DataStatusEmployee
import com.aporisma.aporisma_api.Model.Param.APIResultParam
import com.aporisma.aporisma_api.Model.Param.DataStatus
import com.aporisma.aporisma_api.Model.Reimbursement.APIResultReimDetail
import com.aporisma.aporisma_api.Model.Reimbursement.APIResultReimHeader
import com.aporisma.aporisma_api.Model.Reimbursement.APIResultSendForm
import com.aporisma.aporisma_api.Model.Reimbursement.DataStatus.ReimbursementDetails
import com.aporisma.aporisma_api.Model.Reimbursement.DataStatus.ReimbursementHeader
import com.aporisma.aporisma_api.Repo.Repo
import com.aporisma.aporisma_api.Response.BaseResponse
import com.aporisma.aporisma_api.Response.ResponseCourier
import com.aporisma.aporismahr.Adapter.ReimHeaderAdapter
import com.aporisma.aporismahr.Helper.DateHelper.now
import com.aporisma.aporismahr.Helper.Dialog.dismiss
import com.aporisma.aporismahr.Helper.Dialog.error
import com.aporisma.aporismahr.Helper.Dialog.errorFinish
import com.aporisma.aporismahr.Helper.Dialog.loading
import com.aporisma.aporismahr.Helper.Dialog.succedd
import com.aporisma.aporismahr.Helper.Dialog.succeddFinish
import com.aporisma.aporismahr.Helper.PrefHelper
import com.aporisma.aporismahr.Helper.PrefHelper.getObject
import com.aporisma.aporismahr.Helper.indo
import com.aporisma.aporismahr.R
import com.aporisma.base_componen.BaseActivity
import com.aporisma.base_componen.setOnClick
import com.google.gson.Gson
import kotlinx.android.synthetic.main.claim_header_activity.*
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList

class ClaimHeaderActivity : BaseActivity(), ResponseCourier {

    var mContext = this@ClaimHeaderActivity
    var mActivity = this@ClaimHeaderActivity
    var repo = Repo(mContext, this)
    lateinit var dataStatusEmployee: DataStatusEmployee
    var listReimbursement = ArrayList<ReimbursementDetails>()
    private var dataTypeClaim = ArrayList<DataStatus>()
    //    private var listIdReim = ArrayList<idReim>()
    lateinit var historyClaimAdapter: ReimHeaderAdapter
    lateinit var dataHeader: ReimbursementHeader
    var nominalSelected: Int = 0
    var stringExtra: String? = null
    lateinit var status: String

    @SuppressLint("SetTextI18n")
    override fun initCreate() {
        try {
            loading(mContext, repo, "Loading...")
            dataStatusEmployee = getObject(mContext, "dataEmployee", DataStatusEmployee::class.java)
//            etName.setText(dataStatusEmployee.namaDepan + " " + dataStatusEmployee.namaBelakang)
            etName.isEnabled = false
            repo.getGReim(PrefHelper.getPref(mContext,"idCompany"),dataStatusEmployee.idEmployee)
//            repo.getTypeClaim()
            tvTotal.setText("$nominalSelected")
            etJudul.setText("Pengajuan Reimbursment")
            etJudul.isEnabled = false
            initAdapter()
        } catch (e: java.lang.Exception) {
            error(mContext, resources.getString(R.string.app_name), e.toString())
        }
    }

    private fun initAdapter() {
        historyClaimAdapter = ReimHeaderAdapter(listReimbursement, this)
        val mLayoutManager = LinearLayoutManager(this)
        rvKlaim.layoutManager = mLayoutManager
        rvKlaim.itemAnimator = DefaultItemAnimator()
        rvKlaim.adapter = historyClaimAdapter
        historyClaimAdapter.notifyDataSetChanged()
    }

    override fun getLayoutId(): Int = R.layout.claim_header_activity

    override fun initDestroy() {}

    override fun getResponse(response: BaseResponse<*>?, message: String?) {
        dismiss()
        when (response) {
            is APIResultReimDetail -> for (i in 0 until response.dataStatus.size) {
                if (response.dataStatus[i].idForm == null) {
                    listReimbursement.add(response.dataStatus[i])
                }
                historyClaimAdapter.notifyDataSetChanged()
            }
            is APIResultSendForm -> if (message.equals("detail found")) {
                for (i in 0 until response.dataStatus.details.size) {
                    listReimbursement.add(response.dataStatus.details[i])
                    nominalSelected += response.dataStatus.details[i].nominal
                }
                historyClaimAdapter.notifyDataSetChanged()
                val localeID = Locale("in", "ID")
                val formatRupiah = NumberFormat.getCurrencyInstance(localeID)
                tvTotal.setText(formatRupiah.format(nominalSelected.toDouble()))
                etJudul.setText(response.dataStatus.subject)
                stringExtra = response.dataStatus.idForm
                etJudul.isEnabled = false
            } else {
                errorFinish(mContext, mActivity, message.toString())
            }
            is APIResultReimHeader -> {
                if (response.status == "form succed") {
                    succeddFinish(mContext, mActivity, message.toString())
                } else {
                    if (response.dataStatus.status == "R") error(mContext, "Approver Note", response.dataStatus.approveDesc)
                    else if (response.dataStatus.status == "A") succedd(mContext, "Approver Note", response.dataStatus.approveDesc)
                    dataHeader = response.dataStatus
                    etJudul.setText(response.dataStatus.subject)
                    etGroup.setText(response.dataStatus.approvalTplName)
                    etName.setText(response.dataStatus.namaDepan)
                    listReimbursement.addAll(response.dataStatus.details)
                    for (i in 0 until listReimbursement.size) {
                        for (j in 0 until dataTypeClaim.size) {
                            if (listReimbursement[i].tipe.equals(dataTypeClaim[j].paramCode)) {
                                listReimbursement[i].tipeValue = dataTypeClaim[j].paramValue
                            }
                        }
                        nominalSelected += listReimbursement[i].nominal
                    }
                    tvTotal.setText(indo.currency.format(nominalSelected.toDouble()))
                    historyClaimAdapter.notifyDataSetChanged()
                }
            }
            is APIResultParam -> {
                dataTypeClaim.addAll(response.dataStatus)
                repo.getReimDetail(dataStatusEmployee.idEmployee.toString())
            }
            is APIResult -> {
                succeddFinish(mContext, mActivity, "$message")
            }
            is APIResultGroupE->{
                etGroup.setText(response.dataStatus.approvalTplName)
                getDataExtra()
            }
            else ->
                when (message) {
                    "Data Not Found" -> errorFinish(mContext, mActivity, message.toString())
                    "Expired token" -> repo.callTOKEN()
                    "You dont have group for approved" -> errorFinish(mContext,mActivity,message)
                    else -> error(mContext, resources.getString(R.string.app_name), "$message")
                }
        }
    }

    fun getDataExtra() {
        val intent = intent
        val getExtraData = intent.extras
        if (getExtraData != null) {
            try {
                stringExtra = getExtraData.get("data") as String
                if (stringExtra != "") {
                    if (stringExtra!!.split(";").size > 1 && stringExtra!!.split(";")[0] == "A") {
                        repo.getReimHDetail(stringExtra!!.split(";")[1])
                        textView.text = "Approved"
                        llbtnReject.visibility = View.VISIBLE
                    } else if (stringExtra!!.split(";").size > 1 && stringExtra!!.split(";")[0] == "R") {
                        repo.getReimHDetail(stringExtra!!.split(";")[1])
                        textView.text = "Re-Submit"
                        llbtnReject.visibility = View.GONE
                    } else {
                        repo.getReimHDetail(stringExtra)
                        btnPengajuanKlaim.visibility = View.GONE
                        textView.visibility = View.GONE
                    }
                    etJudul.isEnabled = false
                } else {
                    etJudul.isEnabled = false
                    etName.setText(dataStatusEmployee.namaDepan)
                    repo.getReimDetail(dataStatusEmployee.idEmployee.toString())
                }
            } catch (e: java.lang.Exception) {
                error(mContext, resources.getString(R.string.app_name), e.toString())
            }
        }
    }

    companion object {
        fun startThisActivity(ctx: Context, data: String) {
            val intent = Intent(ctx, ClaimHeaderActivity::class.java).putExtra("data", data)
            ctx.startActivity(intent)
        }
    }

    override fun onClick() {
        object : setOnClick(arrayOf(btnPengajuanKlaim, btnRejectClaim,iv_back)) {
            @SuppressLint("NewApi", "SetTextI18n")
            override fun doClick(view: View) {
                when (view.id) {
                    R.id.btnPengajuanKlaim -> {
                        try {
                            if (etJudul.text.toString() == "") {
                                error(mContext, "ASCI-HR", "Please insert subject")
                            } else if (textView.text == "Submit Form") {
                                var saveData = false
                                for (i in 0 until listReimbursement.size) {
                                    if (listReimbursement[i].selected == "true") {
                                        saveData = true
                                    }
                                }
                                if (saveData) {
                                    loading(mContext, repo, "Loading...")
                                    val dataEmployee =
                                        getObject(mContext, "dataEmployee", DataStatusEmployee::class.java)
                                    val request = ReimbursementHeader()
                                    request.idEmployee = dataEmployee.idEmployee.toString()
                                    request.idCompany = PrefHelper.getPref(mContext,"idCompany")
                                    request.nominal = nominalSelected.toString()
                                    request.subject = "Pengajuan Reimbursment"
                                    request.dateAssign = now("yyyy-MM-dd HH:mm:ss")
                                    request.createdUser =
                                        dataEmployee.namaDepan.toString() + " " + dataEmployee.namaBelakang
                                    for (i in 0 until listReimbursement.size) {
                                        if (listReimbursement[i].selected == "true") request.details.add(listReimbursement[i])
                                    }

                                    repo.addReimHeader(Gson().toJson(request))
                                } else {
                                    error(mContext, "ASCI-HR", "Please pick reimburse item!")
                                }

                            } else if (textView.text == "Re-Submit") {
                                loading(mContext, repo, "Loading...")
                                val dataEmployee =
                                    getObject(mContext, "dataEmployee", DataStatusEmployee::class.java)
                                val request = ReimbursementHeader()
                                request.idForm = dataHeader.idForm
                                request.idEmployee = dataEmployee.idEmployee.toString()
                                request.idCompany = PrefHelper.getPref(mContext,"idCompany")
                                request.nominal = nominalSelected.toString()
                                request.subject = etJudul.text.toString()
                                request.dateAssign = now("yyyy-MM-dd HH:mm:ss")
                                request.createdUser =
                                    dataEmployee.namaDepan.toString() + " " + dataEmployee.namaBelakang
                                for (i in 0 until listReimbursement.size) {
                                    request.details.add(listReimbursement[i])
                                }

                                repo.updateReimHeader(Gson().toJson(request))
                            } else if (textView.text == "Approved") {
                                status = "Approved"
                                inputNote()
                            }
                        } catch (e: Exception) {
                            dismiss()
                            error(mContext, "ASCI-Ardi", "error@save\n$e")
                        }
                    }
                    R.id.btnRejectClaim -> {
                        try {
                            status = "Reject"
                            inputNote()
                        } catch (e: java.lang.Exception) {
                            error(mContext, resources.getString(R.string.app_name), e.toString())
                        }
                    }
                    R.id.iv_back -> finish()
                }
            }
        }
    }

    override fun onRestart() {
        super.onRestart()
        if (listReimbursement.size > 0) listReimbursement.clear()
        nominalSelected = 0
        initCreate()
    }

    @SuppressLint("SetTextI18n")
    fun inputNote() {
        try {
            val dialogBuilder = AlertDialog.Builder(mContext)
            @SuppressLint("InflateParams") val dialogView =
                LayoutInflater.from(mContext).inflate(R.layout.dialog_message, null)
            dialogBuilder.setView(dialogView)

            val etSetting = dialogView.findViewById<EditText>(R.id.etSetting)
            val tvSetting = dialogView.findViewById<TextView>(R.id.tvSetting)
            val tvSettingTitle = dialogView.findViewById<TextView>(R.id.tvSettingTitle)
            val btnOK = dialogView.findViewById<Button>(R.id.btnOK)
            val btnCancel = dialogView.findViewById<Button>(R.id.btnCancel)
            val b = dialogBuilder.create()
            b.show()
            b.setCanceledOnTouchOutside(false)
            b.setCancelable(false)


            val lp = WindowManager.LayoutParams()
            lp.copyFrom(b.window?.attributes)

            lp.width = WindowManager.LayoutParams.MATCH_PARENT

            lp.height = WindowManager.LayoutParams.WRAP_CONTENT

            lp.windowAnimations = R.style.DialogAnimation

            b.window?.attributes = lp

            tvSetting.textAlignment = View.TEXT_ALIGNMENT_CENTER
            etSetting.hint = "Insert note here!"
            tvSetting.text = "Please insert Note"
            etSetting.inputType = InputType.TYPE_TEXT_FLAG_MULTI_LINE
            tvSettingTitle.text = mContext.resources.getString(R.string.app_name)
            btnCancel.setOnClickListener {
                b.dismiss()
            }
            btnOK.setOnClickListener {
                if (etSetting.text.toString() != "") {
                    var approval = SendApvReim()
                    approval.id_form = dataHeader.idForm
                    approval.id_employee = dataHeader.idEmployee
                    approval.id_company = PrefHelper.getPref(mContext,"idCompany")
                    approval.approved_date = now("yyyy-MM-dd'T'HH:mm:ss")
                    approval.approved_user =
                        dataStatusEmployee.namaDepan + " " + dataStatusEmployee.namaBelakang
                    approval.approved_desc = etSetting.text.toString()
                    approval.status = status
                    for (i in 0 until listReimbursement.size) {
                        approval.details.add(listReimbursement[i].idReim)
                    }
//                    Log.e("JSON---->",Gson().toJson(approval))
                    b.dismiss()
                    repo.approveReim(Gson().toJson(approval))
                } else {
                    error(mContext, resources.getString(R.string.app_name), "Please insert note")
                }
            }
        } catch (e: Exception) {
            error(mContext, "ASCI-Ardi\nShowDialog", "error@BuildMessage\n$e\n${R.string.sendExceptionMessage}")
        }
    }
}
