package com.aporisma.aporismahr

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aporisma.aporisma_api.Model.Employee.DataStatusEmployee
import com.aporisma.aporisma_api.Model.Employee.HistoryCuti
import com.aporisma.aporismahr.Adapter.HistoryCutiAdapter
import com.aporisma.base_componen.BaseActivity
import com.aporisma.base_componen.setOnClick
import com.google.gson.Gson
import kotlinx.android.synthetic.main.content_main_navigation.btnPengajuanCuti
import kotlinx.android.synthetic.main.history_activity_leave.*


class HistoryLeaveActivity : BaseActivity() {

    val mContext = this
    var listHistoryCuti = ArrayList<HistoryCuti>()
    lateinit var detailCuti : HistoryCuti
    lateinit var historyCutiAdapter: HistoryCutiAdapter
    lateinit var dataStatusEmployee: DataStatusEmployee

    override fun initCreate() {
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener { finish() }
        val intent = intent
        val getExtraData = intent.extras
        if (getExtraData != null) {
            val StringExtra = getExtraData.get("historyCuti") as String?
            dataStatusEmployee = Gson().fromJson(StringExtra, DataStatusEmployee::class.java)
            listHistoryCuti.addAll(dataStatusEmployee.cuti[0].historyCuti)
        }
        initJobAdapter()
    }

    private fun initJobAdapter() {
        historyCutiAdapter=HistoryCutiAdapter(listHistoryCuti,this)
        val mLayoutManager= LinearLayoutManager(this)
        rvRiwayatCuti.layoutManager = mLayoutManager as RecyclerView.LayoutManager?
        rvRiwayatCuti.itemAnimator = DefaultItemAnimator()
        rvRiwayatCuti.adapter = historyCutiAdapter
        historyCutiAdapter.notifyDataSetChanged()
    }

    override fun getLayoutId(): Int = R.layout.history_activity_leave

    override fun initDestroy() {
    }

    override fun onClick() {
        object : setOnClick(arrayOf(btnPengajuanCuti)) {
            @SuppressLint("NewApi", "SetTextI18n")
            override fun doClick(view: View) {
                when (view.id) {
                    R.id.btnPengajuanCuti -> {
                        FormCutiActivity.startThisActivity(this@HistoryLeaveActivity)
                    }
                }
            }
        }
    }

    companion object {
        fun startThisActivity(ctx: Context,historyCuti:String) {
            val intent = Intent(ctx, HistoryLeaveActivity::class.java).putExtra("historyCuti",historyCuti)
            ctx.startActivity(intent)
        }
    }
}
