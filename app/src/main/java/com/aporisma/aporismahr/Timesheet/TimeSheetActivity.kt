package com.aporisma.aporismahr.Timesheet

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aporisma.aporisma_api.Model.Employee.Data.DataStatusEmployee
import com.aporisma.aporisma_api.Model.Param.APIResultParam
import com.aporisma.aporisma_api.Model.Param.DataStatus
import com.aporisma.aporisma_api.Model.TimeSheet.APIResultTSD
import com.aporisma.aporisma_api.Model.TimeSheet.APIResultTSHRow
import com.aporisma.aporisma_api.Model.TimeSheet.TimeSheetDetail
import com.aporisma.aporisma_api.Model.TimeSheet.TimeSheetHeader
import com.aporisma.aporisma_api.Repo.Repo
import com.aporisma.aporisma_api.Response.BaseResponse
import com.aporisma.aporisma_api.Response.ResponseCourier
import com.aporisma.aporismahr.Adapter.HistoryTSDetail
import com.aporisma.aporismahr.Adapter.HistoryTSHeader
import com.aporisma.aporismahr.Helper.Dialog.dismiss
import com.aporisma.aporismahr.Helper.Dialog.error
import com.aporisma.aporismahr.Helper.Dialog.loading
import com.aporisma.aporismahr.Helper.PrefHelper
import com.aporisma.aporismahr.R
import com.aporisma.base_componen.BaseActivity
import com.aporisma.base_componen.setOnClick
import kotlinx.android.synthetic.main.timesheet_activity.*

@Suppress("DEPRECATION")
class TimeSheetActivity : BaseActivity(), ResponseCourier {

    var mContext = this@TimeSheetActivity
    var repo = Repo(mContext, this)
    lateinit var dataEmployee: DataStatusEmployee
    var listTSDetail = ArrayList<TimeSheetDetail>()
    var listTSHeader = ArrayList<TimeSheetHeader>()
    val dParamType = ArrayList<DataStatus>()
    val dParamCust = ArrayList<DataStatus>()
    lateinit var historyTSDetail: HistoryTSDetail
    lateinit var historyTSHeader: HistoryTSHeader

    override fun initCreate() {
        repo.getCustList(PrefHelper.getPref(mContext, "idCompany"))
        repo.getTypeTask(PrefHelper.getPref(mContext, "idCompany"))
//        repo.getTypeClaim()
        dataEmployee =
            PrefHelper.getObject(mContext, "dataEmployee", DataStatusEmployee::class.java)
    }

    override fun onRestart() {
        super.onRestart()
        if (listTSDetail.size > 0) listTSDetail.clear()
        if (listTSHeader.size > 0) listTSHeader.clear()
        initCreate()
    }

    override fun getLayoutId(): Int = R.layout.timesheet_activity
    override fun initDestroy() {
    }

    override fun getResponse(response: BaseResponse<*>?, message: String?) {
        when (response) {
            is APIResultTSD -> {
                dismiss()
                if (listTSDetail.size > 0) listTSDetail.clear()
                for (i in 0 until response.dataStatus.size) {
                    for (j in 0 until dParamType.size) {
                        if (response.dataStatus[i].type.equals(dParamType[j].paramId.toString()))
                            response.dataStatus[i].type= dParamType[j].paramValue
                    }
                    for (j in 0 until dParamCust.size) {
                        if (response.dataStatus[i].customer.equals(dParamCust[j].paramId.toString()))
                            response.dataStatus[i].customer = dParamCust[j].paramValue
                    }
                    if (response.dataStatus[i].idTs == null) {
                        listTSDetail.add(response.dataStatus[i])
                    }
                }
                historyTSDetail.notifyDataSetChanged()
            }
            is APIResultTSHRow -> {
                dismiss()
                if (listTSHeader.size > 0) listTSHeader.clear()

                listTSHeader.addAll(response.dataStatus)
                historyTSHeader.notifyDataSetChanged()
            }
            is APIResultParam -> {
                if (response.status == "TASK") dParamType.addAll(response.dataStatus)
                else if (response.status == "CUST"){
                    dParamCust.addAll(response.dataStatus)
                    detailTSInitAdapter()
                }
            }
            else -> {
                dismiss()
                error(mContext, resources.getString(R.string.app_name), message.toString())
            }
        }
    }

    override fun onClick() {
        object : setOnClick(arrayOf(tvTimeSheetD, tvTimeSheetH, btnTambah,iv_back)) {
            @SuppressLint("NewApi", "SetTextI18n")
            override fun doClick(view: View) {
                when (view.id) {
                    R.id.tvTimeSheetD -> {
                        detailTSInitAdapter()
                    }
                    R.id.tvTimeSheetH -> {
                        headerTSInitAdapter()
                    }
                    R.id.btnTambah ->{
                        if (textView.text == "Add New"){
                            TimeSheetDetailsActivity.startThisActivity(mContext, "")
                        } else {
                            TimeSheetHeaderActivity.startThisActivity(mContext, "")
                        }
                    }
                    R.id.iv_back -> finish()
                }
            }
        }
    }

    fun detailTSInitAdapter() {
        repo.getTimesheetD(dataEmployee.idEmployee.toString())
        loading(mContext, repo, "Loading...")

        textView.text = "Add New"
        svTimeSheetD.visibility = View.VISIBLE
        tvTimeSheetD.setTextColor(resources.getColor(android.R.color.white))
        tvTimeSheetD.backgroundTintList =
            mContext.resources.getColorStateList(android.R.color.holo_blue_light)

        tvTimeSheetH.setTextColor(resources.getColor(android.R.color.white))
        tvTimeSheetH.backgroundTintList =
            mContext.resources.getColorStateList(android.R.color.darker_gray)
        svTimeSheetH.visibility = View.GONE

        historyTSDetail = HistoryTSDetail(listTSDetail, this)
        val mLayoutManager = LinearLayoutManager(this)
        rvTSd.setLayoutManager(mLayoutManager as RecyclerView.LayoutManager?)
        rvTSd.setItemAnimator(DefaultItemAnimator())
        rvTSd.setAdapter(historyTSDetail)
        historyTSDetail.notifyDataSetChanged()
    }

    fun headerTSInitAdapter() {

        repo.getTimesheetH(dataEmployee.idEmployee.toString())
        loading(mContext, repo, "Loading...")
        textView.text = "Add Form"

        svTimeSheetD.visibility = View.GONE
        tvTimeSheetD.setTextColor(resources.getColor(android.R.color.white))
        tvTimeSheetD.backgroundTintList =
            mContext.resources.getColorStateList(android.R.color.darker_gray)

        tvTimeSheetH.setTextColor(resources.getColor(android.R.color.white))
        tvTimeSheetH.backgroundTintList =
            mContext.resources.getColorStateList(android.R.color.holo_blue_light)

        svTimeSheetH.visibility = View.VISIBLE

        historyTSHeader = HistoryTSHeader(listTSHeader, this)
        val mLayoutManager = LinearLayoutManager(this)
        rvTSh.setLayoutManager(mLayoutManager as RecyclerView.LayoutManager?)
        rvTSh.setItemAnimator(DefaultItemAnimator())
        rvTSh.setAdapter(historyTSHeader)
        historyTSHeader.notifyDataSetChanged()
    }

    companion object {

        fun startThisActivity(ctx: Context) {
            val intent = Intent(ctx, TimeSheetActivity::class.java)
            ctx.startActivity(intent)
        }
    }


}
