package com.aporisma.aporismahr.Timesheet

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.text.Html
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.*
import com.aporisma.aporisma_api.Model.BaseModel
import com.aporisma.aporisma_api.Model.Employee.APIResultGroupE
import com.aporisma.aporisma_api.Model.Employee.Data.DataStatusEmployee
import com.aporisma.aporisma_api.Model.Param.APIResultParam
import com.aporisma.aporisma_api.Model.Param.DataStatus
import com.aporisma.aporisma_api.Model.TimeSheet.APIResultTSD
import com.aporisma.aporisma_api.Model.TimeSheet.TimeSheetDetail
import com.aporisma.aporisma_api.Repo.Repo
import com.aporisma.aporisma_api.Response.BaseResponse
import com.aporisma.aporisma_api.Response.ResponseCourier
import com.aporisma.aporismahr.Helper.DateHelper
import com.aporisma.aporismahr.Helper.Dialog.dismiss
import com.aporisma.aporismahr.Helper.Dialog.error
import com.aporisma.aporismahr.Helper.Dialog.errorFinish
import com.aporisma.aporismahr.Helper.Dialog.loading
import com.aporisma.aporismahr.Helper.Dialog.succeddFinish
import com.aporisma.aporismahr.Helper.PrefHelper
import com.aporisma.aporismahr.R
import com.aporisma.base_componen.BaseActivity
import com.aporisma.base_componen.setOnClick
import com.github.jhonnyx2012.horizontalpicker.DatePickerListener
import com.github.jhonnyx2012.horizontalpicker.HorizontalPicker
import com.google.gson.Gson
import kotlinx.android.synthetic.main.timesheet_details_activity.*
import org.joda.time.DateTime

@Suppress("DEPRECATION")
class TimeSheetDetailsActivity : BaseActivity(), ResponseCourier, DatePickerListener {

    var dateSelect: DateTime? = null
    val mContext = this@TimeSheetDetailsActivity
    val mActivity = this@TimeSheetDetailsActivity
    val repo = Repo(mContext, this)
    var detilTS = TimeSheetDetail()
    var dataEmployee = DataStatusEmployee()
    val sArrayCust = ArrayList<String>()
    val sArrayType = ArrayList<String>()
    val sArrayPos = ArrayList<String>()
    val dParamCust = ArrayList<DataStatus>()
    val dParamType = ArrayList<DataStatus>()


    override fun initCreate() {
        repo.getCustList(PrefHelper.getPref(mContext, "idCompany"))
        repo.getTypeTask(PrefHelper.getPref(mContext, "idCompany"))
        initSpinnerPos()

        dataEmployee =
            PrefHelper.getObject(mContext, "dataEmployee", DataStatusEmployee::class.java)
//        repo.getGTask(dataEmployee.idEmployee)
        etHoursOut.showSoftInputOnFocus = false
        etHoursIn.showSoftInputOnFocus = false
        etMinuteIn.showSoftInputOnFocus = false
        etMinuteOut.showSoftInputOnFocus = false


    }

    override fun onDateSelected(dateSelected: DateTime?) {
        dateSelect = dateSelected
        showToast(dateSelected.toString())
    }

    private fun datePicker(initializeDate: String) {
        val picker = findViewById<HorizontalPicker>(R.id.dateTimeline)
        picker.setListener(this@TimeSheetDetailsActivity)
            .setDays(120).setOffset(31)
            .setDateSelectedColor(Color.WHITE)
            .setDateSelectedTextColor(Color.WHITE)
            .setMonthAndYearTextColor(Color.DKGRAY)
            .setTodayDateBackgroundColor(Color.GRAY).setUnselectedDayTextColor(Color.DKGRAY)
            .setDayOfWeekTextColor(Color.DKGRAY)
            .showTodayButton(false)
            .init()
        if (initializeDate != "") {
            picker.setDate(DateTime(initializeDate))
        } else {
            picker.setDate(DateTime())
        }

    }

    override fun getLayoutId(): Int = R.layout.timesheet_details_activity

    override fun onClick() {
        object : setOnClick(arrayOf(btnSave, btnTimeIn, btnTimeOut, btnDel, iv_back)) {
            @SuppressLint("NewApi", "SetTextI18n")
            override fun doClick(view: View) {
                when (view.id) {
                    R.id.btnSave -> {
                        if (etHoursIn.text.toString() == "" || etHoursOut.text.toString() == "" || etPosition.selectedItem.toString() == "" || etCustomer.selectedItem.toString() == "" || etType.selectedItem.toString() == "" || etDesc.text.toString() == "") {
                            error(mContext, resources.getString(R.string.app_name), "Please fill all field")
                        } else {
                            inputNote("Apakah anda yakin untuk mengirim data timesheet untuk tanggal ${dateSelect.toString().split("T")[0]}")
                        }
                    }
                    R.id.btnTimeIn -> DateHelper.getTime(mContext, etHoursIn, etMinuteIn)
                    R.id.btnTimeOut -> DateHelper.getTime(mContext, etHoursOut, etMinuteOut)
                    R.id.btnDel -> repo.delTimeD(detilTS.idTsdetail)
                    R.id.iv_back -> finish()
                }
            }
        }
    }

    override fun initDestroy() {}

    override fun getResponse(response: BaseResponse<*>?, message: String?) {
        dismiss()
        when (response) {
            is APIResultTSD -> {
                succeddFinish(mContext, mActivity, "$message")
            }
            is APIResultGroupE -> {
                etGroup.setText(response.dataStatus.approvalTplName)
            }
            is BaseModel -> {
                succeddFinish(mContext, mActivity, "$message")
            }
            is APIResultParam -> {
                if (response.status == "CUST") {
                    for (i in 0 until response.dataStatus.size) {
                        sArrayCust.add(response.dataStatus[i].paramCode)
                    }
                    dParamCust.addAll(response.dataStatus)
                    initSpinnerCust()
                } else if (response.status == "TASK") {
                    for (i in 0 until response.dataStatus.size) {
                        sArrayType.add(response.dataStatus[i].paramCode)
                    }
                    dParamType.addAll(response.dataStatus)
                    initSpinnerType()

                    val intent = intent
                    val getExtraData = intent.extras
                    if (getExtraData != null) {
//            try {
                        val stringExtra = getExtraData.get("data") as String
                        if (stringExtra != "") {
                            if (stringExtra.split("#").size > 1) {
                                detilTS =
                                    Gson().fromJson(stringExtra.split("#")[1], TimeSheetDetail::class.java)
                                btnSave.visibility = View.VISIBLE
                                btnSave.text = "Update"
                                btnDel.visibility = View.VISIBLE
                            } else {
                                etCustomer.isEnabled = false
                                etPosition.isEnabled = false
                                etProject.isEnabled = false
                                etDesc.isEnabled = false
                                etType.isEnabled = false
                                btnTimeIn.isEnabled = false
                                btnTimeOut.isEnabled = false
                                detilTS = Gson().fromJson(stringExtra, TimeSheetDetail::class.java)
                                btnSave.visibility = View.GONE
                            }
                            etHoursIn.setText(detilTS.timeIn.toString().split(":")[0])
                            etMinuteIn.setText(detilTS.timeIn.toString().split(":")[1])
                            etHoursOut.setText(detilTS.timeOut.toString().split(":")[0])
                            etMinuteOut.setText(detilTS.timeOut.toString().split(":")[1])
                            for (i in 0 until sArrayPos.size) if (sArrayPos[i].equals(detilTS.lokasi)) etPosition.setSelection(i)
                            for (i in 0 until sArrayCust.size) if (sArrayCust[i].equals(detilTS.customer)) etCustomer.setSelection(i)
                            for (i in 0 until sArrayType.size) if (sArrayType[i].equals(detilTS.type)) etType.setSelection(i)
//                            etProject.setText(detilTS.project)
                            etDesc.setText(Html.fromHtml(detilTS.description))
                            datePicker(detilTS.date)
                            tvTitle.setText("Timesheet Detail")
                        } else {
                            datePicker("")
                        }
                    }
                }
            }
            else -> {
                if (message == "You dont have group for approved") errorFinish(mContext, mActivity, message)
                else error(mContext, resources.getString(R.string.app_name), "$message")
            }
        }
    }

    companion object {
        fun startThisActivity(ctx: Context, data: String) {
            val intent = Intent(ctx, TimeSheetDetailsActivity::class.java).putExtra("data", data)
            ctx.startActivity(intent)
        }
    }

    private fun initSpinnerCust() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, sArrayCust)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        val sItems = findViewById<View>(R.id.etCustomer) as Spinner
        sItems.adapter = adapter
    }

    private fun initSpinnerType() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, sArrayType)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        val sItems = findViewById<View>(R.id.etType) as Spinner
        sItems.adapter = adapter
    }

    private fun initSpinnerPos() {
        sArrayPos.add("ON SITE")
        sArrayPos.add("OFF SITE")
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, sArrayPos)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        val sItems = findViewById<View>(R.id.etPosition) as Spinner
        sItems.adapter = adapter
    }


    @SuppressLint("SetTextI18n")
    fun inputNote(message: String) {
        try {
            val dialogBuilder = AlertDialog.Builder(mContext)
            @SuppressLint("InflateParams") val dialogView =
                LayoutInflater.from(mContext).inflate(R.layout.dialog_message, null)
            dialogBuilder.setView(dialogView)

            val etSetting = dialogView.findViewById<EditText>(R.id.etSetting)
            val tvSetting = dialogView.findViewById<TextView>(R.id.tvSetting)
            val tvSettingTitle = dialogView.findViewById<TextView>(R.id.tvSettingTitle)
            val btnOK = dialogView.findViewById<Button>(R.id.btnOK)
            val btnCancel = dialogView.findViewById<Button>(R.id.btnCancel)
            val b = dialogBuilder.create()
            b.show()
            b.setCanceledOnTouchOutside(false)
            b.setCancelable(false)


            val lp = WindowManager.LayoutParams()
            lp.copyFrom(b.window?.attributes)

            lp.width = WindowManager.LayoutParams.MATCH_PARENT

            lp.height = WindowManager.LayoutParams.WRAP_CONTENT

            lp.windowAnimations = R.style.DialogAnimation

            b.window?.attributes = lp

            android.R.color.holo_blue_light

            tvSetting.textAlignment = View.TEXT_ALIGNMENT_CENTER
            etSetting.visibility = View.GONE
            tvSetting.text = message
            etSetting.inputType = InputType.TYPE_TEXT_FLAG_MULTI_LINE
            tvSettingTitle.text = mContext.resources.getString(R.string.app_name)
            btnCancel.setOnClickListener {
                b.dismiss()
            }
            btnOK.setOnClickListener {
                loading(mContext, repo, "Loading...")
                try {
                    val dataEmployee =
                        PrefHelper.getObject(mContext, "dataEmployee", DataStatusEmployee::class.java)
                    val request = TimeSheetDetail()
                    request.idEmployee = dataEmployee.idEmployee.toString()
                    request.date = dateSelect.toString().split("T")[0]
                    request.timeIn = etHoursIn.text.toString() + ":" + etMinuteIn.text.toString()
                    request.timeOut = etHoursOut.text.toString() + ":" + etMinuteOut.text.toString()
                    request.lokasi = etPosition.selectedItem.toString()
                    request.project = "";

                    for (i in 0 until dParamCust.size)
                        if (dParamCust[i].paramValue.equals(etCustomer.selectedItem.toString()))
                            request.customer = dParamCust[i].paramId.toString()

                    for (i in 0 until dParamType.size)
                        if (dParamType[i].paramValue.equals(etType.selectedItem.toString()))
                            request.type = dParamType[i].paramId.toString()

                    request.description = etDesc.text.toString()
                    request.createdUser = "${dataEmployee.namaDepan} ${dataEmployee.namaBelakang}"
                    if (btnSave.text.toString() == "Save") repo.addTSDetail(Gson().toJson(request))
                    else {
                        if (detilTS.status == "R") request.status = "W"
                        else request.status = detilTS.status
                        request.idTsdetail = detilTS.idTsdetail
                        repo.updateTSDetail(Gson().toJson(request))
                    }
                    b.dismiss()
                } catch (e: Exception) {
                    error(mContext, "ASCI-Ardi", "error@Save\n$e")
                    dismiss()
                }
            }
        } catch (e: Exception) {
            error(mContext, "ASCI-Ardi\nShowDialog", "error@BuildMessage\n$e\n${R.string.sendExceptionMessage}")
        }
    }

}
