package com.aporisma.aporismahr.Timesheet

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aporisma.aporisma_api.Model.Approval.APIResult
import com.aporisma.aporisma_api.Model.Approval.sendReim.SendApvReim
import com.aporisma.aporisma_api.Model.Company.DataStatus
import com.aporisma.aporisma_api.Model.Employee.APIResultGroupE
import com.aporisma.aporisma_api.Model.Employee.Data.DataStatusEmployee
import com.aporisma.aporisma_api.Model.Param.APIResultParam
import com.aporisma.aporisma_api.Model.TimeSheet.*
import com.aporisma.aporisma_api.Repo.Repo
import com.aporisma.aporisma_api.Response.BaseResponse
import com.aporisma.aporisma_api.Response.ResponseCourier
import com.aporisma.aporismahr.Adapter.TSDetailAdapter
import com.aporisma.aporismahr.Helper.Dialog
import com.aporisma.aporismahr.Helper.Dialog.dismiss
import com.aporisma.aporismahr.Helper.Dialog.error
import com.aporisma.aporismahr.Helper.Dialog.succedd
import com.aporisma.aporismahr.Helper.Dialog.succeddFinish
import com.aporisma.aporismahr.Helper.PrefHelper
import com.aporisma.aporismahr.R
import com.aporisma.base_componen.BaseActivity
import com.aporisma.base_componen.setOnClick
import com.google.gson.Gson
import kotlinx.android.synthetic.main.timesheet_activity.rvTSd
import kotlinx.android.synthetic.main.timesheet_header_activity.*

class TimeSheetHeaderActivity : BaseActivity(), ResponseCourier {

    val mContext = this@TimeSheetHeaderActivity
    private val mActivity = this@TimeSheetHeaderActivity
    val repo = Repo(mContext, this)
    var dataHeader = TimeSheetHeader()
    var dataEmployee = DataStatusEmployee()
    var dataCompany = DataStatus()
    val dParamCust = ArrayList<com.aporisma.aporisma_api.Model.Param.DataStatus>()
    val dParamType = ArrayList<com.aporisma.aporisma_api.Model.Param.DataStatus>()
    lateinit var TSDetail: TSDetailAdapter
    var listTSDetail = ArrayList<TimeSheetDetail>()
    var revisi = false

    fun detailTSInitAdapter() {
        TSDetail = TSDetailAdapter(listTSDetail, this)
        val mLayoutManager = LinearLayoutManager(this)
        rvTSd.setLayoutManager(mLayoutManager as RecyclerView.LayoutManager?)
        rvTSd.setItemAnimator(DefaultItemAnimator())
        rvTSd.setAdapter(TSDetail)
        TSDetail.notifyDataSetChanged()
    }

    override fun initCreate() {
        try {
            Dialog.loading(mContext, repo, "Loading...")
            dataEmployee =
                PrefHelper.getObject(mContext, "dataEmployee", DataStatusEmployee::class.java)
            dataCompany = PrefHelper.getObject(mContext, "dataCompany", DataStatus::class.java)
            repo.getCustList(PrefHelper.getPref(mContext, "idCompany"))
            repo.getTypeTask(PrefHelper.getPref(mContext, "idCompany"))

            repo.getGTask(PrefHelper.getPref(mContext, "idCompany"), dataEmployee.idEmployee)
        } catch (e: Exception) {
            error(mContext, "ASCI-Ardi\nerror@TSHAct", "initCreated\n$e")
        }
    }

    private var statusForm:String = ""
    private fun getExtras() {
        val intent = intent
        val getExtraData = intent.extras
        if (getExtraData != null) {
            val stringExtra = getExtraData.get("data") as String
            if (stringExtra != "") {
                when {
                    stringExtra.split(";").size > 1 && stringExtra.split(";")[0] == "A" -> {
                        repo.getTimesheetHD(stringExtra.split(";")[1])
                        textView4.text = "Approved Form"
                        llbtnReject.visibility = View.VISIBLE
                        etSubject.isEnabled = false
                        statusForm = stringExtra.split(";")[0]
                    }
                    stringExtra.split(";").size > 1 && stringExtra.split(";")[0] == "R" -> {
                        revisi = true
                        dataHeader =
                            Gson().fromJson(stringExtra.split(";")[1], TimeSheetHeader::class.java)
                        llbtnReject.visibility = View.GONE
                        etSubject.isEnabled = false
                        textView4.text = "Re-Submit Form"
                        repo.getTimesheetHD(dataHeader.idTs)
                    }
                    else -> {
                        dataHeader = Gson().fromJson(stringExtra, TimeSheetHeader::class.java)
                        ll1.visibility = View.GONE
                        etSubject.isEnabled = false
                        repo.getTimesheetHD(dataHeader.idTs)
                    }
                }
            } else {
                repo.getTimesheetD(dataEmployee.idEmployee.toString())
                etEmployeeName.setText(dataEmployee.namaDepan)
            }
            detailTSInitAdapter()
        }
    }

    override fun getLayoutId(): Int = R.layout.timesheet_header_activity

    override fun initDestroy() {}

    @SuppressLint("SetTextI18n")
    override fun getResponse(response: BaseResponse<*>?, message: String?) {
        dismiss()
//        etEmployeeName.setText("${dataEmployee.namaDepan} ${dataEmployee.namaBelakang}")
        when (response) {
            is APIResultTSHD -> when {
                response.status == "form succedd" -> succeddFinish(mContext, mActivity, "$message")
                else -> {
                    dataHeader.idTs = response.dataStatus.idTs
                    dataHeader.idEmployee = response.dataStatus.details[0].idEmployee
                    etSubject.setText(response.dataStatus.subject)

                    for (i in 0 until response.dataStatus.details.size) {
                        for (ii in 0 until dParamCust.size) {
                            if (dParamCust[ii].paramId.toString().equals(response.dataStatus.details[i].customer))
                                response.dataStatus.details[i].customer = dParamCust[ii].paramValue
                        }
                        for (ii in 0 until dParamType.size) {
                            if (dParamType[ii].paramId.toString().equals(response.dataStatus.details[i].type))
                                response.dataStatus.details[i].type = dParamType[ii].paramValue
                        }
                    }

                    etEmployeeName.setText(response.dataStatus.namaDepan)
                    etGroup.setText(response.dataStatus.approvalTplName)

                    listTSDetail.addAll(response.dataStatus.details)
                    TSDetail.notifyDataSetChanged()
                    if (response.dataStatus.approvedDesc != "" && response.dataStatus.status == "R") error(mContext, "Approver Note", response.dataStatus.approvedDesc)
                    else if (response.dataStatus.approvedDesc != "" && response.dataStatus.status == "A") succedd(mContext, "Approver Note", response.dataStatus.approvedDesc)
                }
            }
            is APIResultTSD -> {
                for (i in 0 until response.dataStatus.size) {
                    for (ii in 0 until dParamCust.size) {
                        if (dParamCust[ii].paramId.toString().equals(response.dataStatus[i].customer))
                            response.dataStatus[i].customer = dParamCust[ii].paramValue
                    }
                    for (ii in 0 until dParamType.size) {
                        if (dParamType[ii].paramId.toString().equals(response.dataStatus[i].type))
                            response.dataStatus[i].type = dParamType[ii].paramValue
                    }
                    if (response.dataStatus[i].idTs == null) {
                        listTSDetail.add(response.dataStatus[i])
                    }
                }
                TSDetail.notifyDataSetChanged()
            }
            is APIResult -> succeddFinish(mContext, mActivity, message.toString())
            is APIResultTSH -> succeddFinish(mContext, mActivity, message.toString())
            is APIResultGroupE -> etGroup.setText(response.dataStatus.approvalTplName)
            is APIResultParam -> {
                if (response.status == "CUST") {
                    dParamCust.addAll(response.dataStatus)
                    getExtras()
                } else if (response.status == "TASK") dParamType.addAll(response.dataStatus)
            }
            else -> if (message == "Data Not Found") Dialog.errorFinish(mContext, mActivity, message.toString())
                else if (message == "Expired token") repo.callTOKEN()
//                else if (message == "You dont have group for approved" && statusForm == "") Dialog.errorFinish(mContext,mActivity,message)
                else if (message != "You dont have group for approved") error(mContext, resources.getString(R.string.app_name), "$message")
        }
    }

    lateinit var status: String
    override fun onClick() {
        object : setOnClick(arrayOf(btnTambah, btnRejectTS, iv_back)) {
            @SuppressLint("NewApi", "SetTextI18n")
            override fun doClick(view: View) {
                when (view.id) {
                    R.id.btnTambah -> {
                        try {
                            if (etSubject.text.toString() == "") {
                                error(mContext, "ASCI-HR", "Tolong masukkan subject terlebih dahulu")
                            } else if (textView4.text == "Submit Form") {
                                var saveData = false
                                for (i in 0 until listTSDetail.size) {
                                    if (listTSDetail.get(i).selected == "y") {
                                        saveData = true
                                    }
                                }
                                if (saveData) {
                                    val request = TimeSheetHeader()
                                    request.idEmployee = dataEmployee.idEmployee.toString()
                                    request.idCompany = dataCompany.idCompany.toString()
                                    request.subject = etSubject.text.toString()
                                    request.createdUser =
                                        dataEmployee.namaDepan + " " + dataEmployee.namaBelakang
                                    for (i in 0 until listTSDetail.size) {
                                        if (listTSDetail[i].selected == "y") request.details.add(listTSDetail[i])
                                    }
                                    repo.addTSHeader(Gson().toJson(request))
                                } else {
                                    error(mContext, resources.getString(R.string.app_name), "Please pick item!")
                                }
                            } else if (textView4.text == "Approved Form") {
                                status = "Approved"
                                inputNote()
                            } else if (textView4.text == "Re-Submit Form") {
                                val request = TimeSheetHeader()
                                request.idTs = dataHeader.idTs
                                request.idEmployee = dataEmployee.idEmployee.toString()
                                request.idCompany = dataCompany.idCompany.toString()
                                request.subject = etSubject.text.toString()
                                request.createdUser =
                                    dataEmployee.namaDepan + " " + dataEmployee.namaBelakang
                                for (i in 0 until listTSDetail.size) {
                                    request.details.add(listTSDetail[i])
                                }
                                repo.updateTSHeader(Gson().toJson(request))
                            }
                        } catch (e: Exception) {
                            error(mContext, "ASCI-Ardi", "error@SaveTSH\n$e")
                        }

                    }
                    R.id.btnRejectTS -> {
                        status = "Reject"
                        inputNote()
                    }
                    R.id.iv_back -> finish()
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    fun inputNote() {
        try {
            val dialogBuilder = AlertDialog.Builder(mContext)
            @SuppressLint("InflateParams") val dialogView =
                LayoutInflater.from(mContext).inflate(R.layout.dialog_message, null)
            dialogBuilder.setView(dialogView)

            val etSetting = dialogView.findViewById<EditText>(R.id.etSetting)
            val tvSetting = dialogView.findViewById<TextView>(R.id.tvSetting)
            val tvSettingTitle = dialogView.findViewById<TextView>(R.id.tvSettingTitle)
            val btnOK = dialogView.findViewById<Button>(R.id.btnOK)
            val btnCancel = dialogView.findViewById<Button>(R.id.btnCancel)
            val b = dialogBuilder.create()
            b.show()
            b.setCanceledOnTouchOutside(false)
            b.setCancelable(false)


            val lp = WindowManager.LayoutParams()
            lp.copyFrom(b.window?.attributes)

            lp.width = WindowManager.LayoutParams.MATCH_PARENT

            lp.height = WindowManager.LayoutParams.WRAP_CONTENT

            lp.windowAnimations = R.style.DialogAnimation

            b.window?.attributes = lp

            android.R.color.holo_blue_light

            tvSetting.textAlignment = View.TEXT_ALIGNMENT_CENTER
            etSetting.hint = "Insert note here!"
            tvSetting.text = "Please insert Note"
            tvSettingTitle.text = mContext.resources.getString(R.string.app_name)
            btnCancel.setOnClickListener {
                b.dismiss()
            }
            btnOK.setOnClickListener {
                if (etSetting.text.toString() != "") {
                    val approval = SendApvReim()
                    approval.id_form = dataHeader.idTs
                    approval.id_employee = dataHeader.idEmployee
                    approval.approved_user =
                        dataEmployee.namaDepan + " " + dataEmployee.namaBelakang
                    approval.approved_desc = etSetting.text.toString()
                    approval.status = status
                    for (i in 0 until listTSDetail.size) {
                        approval.details.add(listTSDetail[i].idTsdetail.toInt())
                    }
//                    Log.e("JSON---->",Gson().toJson(approval))
                    b.dismiss()
                    repo.approveTimeS(Gson().toJson(approval))
                } else {
                    error(mContext, resources.getString(R.string.app_name), "Please insert note")
                }
            }
        } catch (e: Exception) {
            error(mContext, "ASCI-Ardi\nShowDialog", "error@BuildMessage\n$e\n${R.string.sendExceptionMessage}")
        }
    }

    companion object {
        fun startThisActivity(ctx: Context, data: String) {
            val intent = Intent(ctx, TimeSheetHeaderActivity::class.java).putExtra("data", data)
            ctx.startActivity(intent)
        }
    }
}
