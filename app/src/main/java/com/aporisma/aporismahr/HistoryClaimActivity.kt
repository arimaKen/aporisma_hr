@file:Suppress("DEPRECATION")

package com.aporisma.aporismahr

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aporisma.aporisma_api.Model.Reimbursement.APIResultForm
import com.aporisma.aporisma_api.Model.Reimbursement.APIResultReimbusement
import com.aporisma.aporisma_api.Model.Reimbursement.DataStatus.FormReimbursement
import com.aporisma.aporisma_api.Model.Reimbursement.DataStatus.Reimbursement
import com.aporisma.aporisma_api.Repo.Repo
import com.aporisma.aporisma_api.Response.BaseResponse
import com.aporisma.aporisma_api.Response.ResponseCourier
import com.aporisma.aporismahr.Adapter.HistoryClaimAdapter
import com.aporisma.aporismahr.Adapter.HistoryFormAdapter
import com.aporisma.aporismahr.Helper.Dialog
import com.aporisma.aporismahr.Helper.Dialog.dismiss
import com.aporisma.aporismahr.Helper.Dialog.loading
import com.aporisma.base_componen.BaseActivity
import com.aporisma.base_componen.setOnClick
import kotlinx.android.synthetic.main.history_activity_claim.*


class HistoryClaimActivity : BaseActivity(), ResponseCourier {
    override fun getResponse(response: BaseResponse<*>?, message: String?) {
        dismiss()
        if (response is APIResultReimbusement && response.dataStatus.size > 0) {
            if (listReimbursement.size > 0) listReimbursement.clear()
            for (i in 0 until response.dataStatus.size) {
                if (response.dataStatus[i].status == "P") listReimbursement.add(response.dataStatus[i])
            }
            historyClaimAdapter.notifyDataSetChanged()
        } else if (response is APIResultForm && response.dataStatus.size > 0) {
            if (listFormReimbursement.size > 0) listFormReimbursement.clear()
            listFormReimbursement.addAll(response.dataStatus)
            historyFormAdapter.notifyDataSetChanged()
        } else {
            Dialog.error(mContext, resources.getString(R.string.app_name), "$message")
        }
    }

    lateinit var mContext: Context
    lateinit var repo: Repo
    var listReimbursement = ArrayList<Reimbursement>()
    var listFormReimbursement = ArrayList<FormReimbursement>()
    lateinit var historyClaimAdapter: HistoryClaimAdapter
    lateinit var historyFormAdapter: HistoryFormAdapter

    override fun initCreate() {
        mContext = this@HistoryClaimActivity
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        toolbar.title = "Reimbursement"
        toolbar.navigationIcon = resources.getDrawable(R.drawable.ic_back)
        setSupportActionBar(toolbar)
        repo = Repo(mContext, this)
        toolbar.setNavigationOnClickListener { finish() }
        formKlaim()
    }

    override fun onRestart() {
        initCreate()
        super.onRestart()
    }

    override fun getLayoutId(): Int = R.layout.history_activity_claim

    override fun initDestroy() {
    }

    override fun onClick() {
        object : setOnClick(arrayOf(tvKlaim, tvFormKlaim, btnTambahKlaim)) {
            @SuppressLint("NewApi", "SetTextI18n")
            override fun doClick(view: View) {
                when (view.id) {
                    R.id.tvKlaim -> {
                        klaim()
                    }
                    R.id.tvFormKlaim -> {
                        formKlaim()
                    }
                    R.id.btnTambahKlaim -> (
                            if (btnTambahKlaim.text == "Tambah Klaim") AddClaimActivity.startThisActivity(mContext, null)
                            else FormClaimActivity.startThisActivity(mContext,null)
                            )
                }
            }
        }
    }

    fun klaim() {
        val intent = intent
        val getExtraData = intent.extras
        if (getExtraData != null) {
            val StringExtra = getExtraData.get("id_employee") as Int?
            repo.getHistoryClaim(StringExtra.toString())
            loading(mContext, repo, "Sedang mendapatkan data")
        }
        tvKlaim.setTextColor(resources.getColor(android.R.color.holo_blue_light))
        vKlaim.setBackgroundColor(resources.getColor(android.R.color.holo_blue_light))
        svReim.visibility = View.VISIBLE
        btnTambahKlaim.text = getString(R.string.tambah_klaim)

        tvFormKlaim.setTextColor(resources.getColor(android.R.color.darker_gray))
        vFormKlaim.setBackgroundColor(resources.getColor(android.R.color.darker_gray))
        svFormReim.visibility = View.GONE

        historyClaimAdapter = HistoryClaimAdapter(listReimbursement, this)
        val mLayoutManager = LinearLayoutManager(this)
        rvKlaim.setLayoutManager(mLayoutManager as RecyclerView.LayoutManager?)
        rvKlaim.setItemAnimator(DefaultItemAnimator())
        rvKlaim.setAdapter(historyClaimAdapter)
        historyClaimAdapter.notifyDataSetChanged()
    }

    fun formKlaim() {
        val intent = intent
        val getExtraData = intent.extras
        if (getExtraData != null) {
            val StringExtra = getExtraData.get("id_employee") as Int?
            repo.getForm(StringExtra.toString())
            loading(mContext, repo, "Sedang mendapatkan data")
        }

        tvKlaim.setTextColor(resources.getColor(android.R.color.darker_gray))
        vKlaim.setBackgroundColor(resources.getColor(android.R.color.darker_gray))
        svReim.visibility = View.GONE
        btnTambahKlaim.text = getString(R.string.tambah_form_klaim)

        tvFormKlaim.setTextColor(resources.getColor(android.R.color.holo_blue_light))
        vFormKlaim.setBackgroundColor(resources.getColor(android.R.color.holo_blue_light))
        svFormReim.visibility = View.VISIBLE

        historyFormAdapter = HistoryFormAdapter(listFormReimbursement, this)
        val mLayoutManager = LinearLayoutManager(this)
        rvFormKlaim.setLayoutManager(mLayoutManager as RecyclerView.LayoutManager?)
        rvFormKlaim.setItemAnimator(DefaultItemAnimator())
        rvFormKlaim.setAdapter(historyFormAdapter)
        historyFormAdapter.notifyDataSetChanged()
    }

    companion object {
        fun startThisActivity(ctx: Context, id_employee: Int) {
            val intent =
                Intent(ctx, HistoryClaimActivity::class.java).putExtra("id_employee", id_employee)
            ctx.startActivity(intent)
        }
    }
}
