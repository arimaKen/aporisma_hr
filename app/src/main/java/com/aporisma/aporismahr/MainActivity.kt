@file:Suppress("DEPRECATION")

package com.aporisma.aporismahr

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Build
import android.provider.Settings
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.aporisma.aporisma_api.Model.Employee.APIResultEmployee
import com.aporisma.aporisma_api.Model.Employee.DataStatusEmployee
import com.aporisma.aporisma_api.Repo.Repo
import com.aporisma.aporisma_api.Response.BaseResponse
import com.aporisma.aporisma_api.Response.ResponseCourier
import com.aporisma.aporismahr.Helper.Dialog
import com.aporisma.aporismahr.Helper.Dialog.dismiss
import com.aporisma.aporismahr.Helper.Dialog.error
import com.aporisma.aporismahr.Helper.FindFakeApps
import com.aporisma.aporismahr.Helper.GettingLocationHelper.date
import com.aporisma.aporismahr.Helper.GettingLocationHelper.getLocationWithCheckNetworkAndGPS
import com.aporisma.aporismahr.Helper.PrefHelper
import com.aporisma.base_componen.BaseActivity
import com.aporisma.base_componen.setOnClick
import com.google.android.material.navigation.NavigationView
import com.google.gson.Gson
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions
import kotlinx.android.synthetic.main.content_main_navigation.*
import kotlinx.android.synthetic.main.nav_header_main_navigation.view.*
import java.util.*


class MainActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener,
    ResponseCourier {
    override fun getResponse(response: BaseResponse<*>?, message: String?) {
        dismiss()
        if (response is APIResultEmployee) {
            dataStatusEmployee = response.dataStatus
            if (dataStatusEmployee.cuti.size > 0) {
                cutiTerpakai = dataStatusEmployee.cuti[0].jatah - dataStatusEmployee.cuti[0].sisa
            }
            addData()
        } else {
            error(mContext, resources.getString(R.string.app_name), "$message")
        }
    }

    lateinit var viewActivity: View
    lateinit var mContext: Context
    lateinit var repo: Repo
    val activity: String = "Main"
    val mActivity = this@MainActivity
    var cutiTerpakai: Int = 0
    var dataStatusEmployee = DataStatusEmployee()

    @SuppressLint("SetTextI18n")
    override fun initCreate() {
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        mContext = this
        repo = Repo(mContext, this)
        viewActivity = window.decorView.rootView
        dataStatusEmployee =
            PrefHelper.getObject(mContext, "dataEmployee", DataStatusEmployee::class.java)
        if (dataStatusEmployee.userPermission != "Super") cvAdmin.visibility = View.GONE
        requestPermission()
        FindFakeApps(mContext, mActivity)
        turnGPSOn()
        setSupportActionBar(toolbar)
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        navView.getHeaderView(0).tvFullName.text = dataStatusEmployee.fullname
        navView.getHeaderView(0).tvEmail.text = dataStatusEmployee.email
        val toggle =
            ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        navView.setNavigationItemSelectedListener(this)
    }

    private fun turnGPSOn() {
        val provider =
            Settings.Secure.getString(contentResolver, Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        try {
            if (!provider.contains("gps")) { //if gps is disabled
                startActivityForResult(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0);
            } else {
                getLocationWithCheckNetworkAndGPS(mContext)
                tvDate.text = date.toString()
                Dialog.loading(mContext, repo, "Sedang mendapatkan data")
                repo.getEmployeeData(dataStatusEmployee.idKaryawan.toString())
            }

        } catch (e: RuntimeException) {
            error(mContext, "RuntimeException", "$e")
        }
    }

    @SuppressLint("SetTextI18n")
    fun addData() {
        try {
            val navView: NavigationView = findViewById(R.id.nav_view)
            navView.getHeaderView(0).tvFullName.text = dataStatusEmployee.fullname
            navView.getHeaderView(0).tvEmail.text = dataStatusEmployee.email
            if (dataStatusEmployee.cuti.size > 0) {
                pbCuti.max = dataStatusEmployee.cuti[0].jatah
                pbCuti.progress = cutiTerpakai
                tvTotalHariCuti.text = "${dataStatusEmployee.cuti[0].jatah} Hari"
                tvSisaJatah.text = "${dataStatusEmployee.cuti[0].sisa} Hari"
                tvJatahTerpakai.text = "$cutiTerpakai Hari"
            } else {
                pbCuti.max = 0
                pbCuti.progress = 0
                tvTotalHariCuti.text = "0"
                tvSisaJatah.text = "0"
                tvJatahTerpakai.text = "0"
            }
        } catch (e: NullPointerException) {
            error(mContext, "ASCI-Ardi\n$activity", "$e")
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_main_navigation

    override fun initDestroy() {
    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onClick() {
        object : setOnClick(arrayOf(btnRiwayat, btnPengajuanCuti)) {
            @SuppressLint("NewApi", "SetTextI18n")
            override fun doClick(view: View) {
                when (view.id) {
                    R.id.btnRiwayat -> {
                        val dataEmployee = Gson().toJson(dataStatusEmployee)
                        HistoryLeaveActivity.startThisActivity(mContext, dataEmployee)
                    }
                    R.id.btnPengajuanCuti -> {
                        FormCutiActivity.startThisActivity(mContext)
                    }
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main_navigation, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action1 -> {
                Toast.makeText(mContext, "Notification Action Needed", Toast.LENGTH_LONG).show()
            }
            else -> super.onOptionsItemSelected(item)
        }
        return false
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_kehadiran -> {
                HistoryAttendanceActivity.startThisActivity(mContext)
            }
            R.id.nav_klaim -> {
                HistoryClaimActivity.startThisActivity(mContext, dataStatusEmployee.idKaryawan)
            }
            R.id.nav_bisnis -> {
                Toast.makeText(this, "Bisnis", Toast.LENGTH_LONG).show()
            }
            R.id.nav_lembur -> {
                Toast.makeText(this, "Lembur", Toast.LENGTH_LONG).show()
            }
            R.id.nav_slip -> {
                Toast.makeText(this, "Slip Gaji", Toast.LENGTH_LONG).show()
            }
            R.id.nav_tugas -> {
                Toast.makeText(this, "Daftar Tugas", Toast.LENGTH_LONG).show()
            }
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return false
    }

    private fun requestPermission() {
        val permission = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET)
            } else {
                arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE, Manifest.permission.INTERNET, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
            }
        } else {
            arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE, Manifest.permission.INTERNET, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
        }

        Permissions.check(this, permission, null, null, object : PermissionHandler() {
            override fun onGranted() {
                Toast.makeText(mContext, "Permission granted", Toast.LENGTH_SHORT).show()
            }

            override fun onDenied(context: Context?, deniedPermissions: ArrayList<String>?) {
                finish()
            }
        })
    }

    companion object {

        fun startThisActivity(ctx: Context) {
            val intent = Intent(ctx, MainActivity::class.java)
            ctx.startActivity(intent)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 0) {
            turnGPSOn()
        }
    }

}
