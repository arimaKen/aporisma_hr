package com.aporisma.aporismahr

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.aporisma.aporisma_api.Model.Employee.DataStatusEmployee
import com.aporisma.aporisma_api.Model.Reimbursement.APIResultReimbusement
import com.aporisma.aporisma_api.Model.Reimbursement.APIResultSendForm
import com.aporisma.aporisma_api.Model.Reimbursement.DataStatus.FormReimbursement
import com.aporisma.aporisma_api.Model.Reimbursement.DataStatus.Reimbursement
import com.aporisma.aporisma_api.Repo.Repo
import com.aporisma.aporisma_api.Response.BaseResponse
import com.aporisma.aporisma_api.Response.ResponseCourier
import com.aporisma.aporismahr.Adapter.FormAdapter
import com.aporisma.aporismahr.Helper.DateHelper
import com.aporisma.aporismahr.Helper.Dialog.dismiss
import com.aporisma.aporismahr.Helper.Dialog.error
import com.aporisma.aporismahr.Helper.Dialog.loading
import com.aporisma.aporismahr.Helper.PrefHelper.getObject
import com.aporisma.base_componen.BaseActivity
import com.aporisma.base_componen.setOnClick
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_form_claim.*

class FormClaimActivity : BaseActivity(), ResponseCourier {

    var mContext = this@FormClaimActivity
    var mActivity = this@FormClaimActivity
    var repo = Repo(mContext, this)
    lateinit var dataStatusEmployee: DataStatusEmployee
    var listReimbursement = ArrayList<Reimbursement>()
    lateinit var historyClaimAdapter: FormAdapter
    var nominalSelected: Int = 0

    override fun initCreate() {
        try {

            loading(mContext, repo, "Mohon tunggu")
            val intent = intent
            val getExtraData = intent.extras
            val stringExtra = getExtraData?.get("data") as String?
            if (stringExtra != null) {
                repo.getFormDetail(stringExtra)
                btnPengajuanKlaim.visibility = View.GONE
            } else {
                dataStatusEmployee =
                    getObject(mContext, "dataEmployee", DataStatusEmployee::class.java)
                repo.getHistoryClaim(dataStatusEmployee.idKaryawan.toString())
            }
            tvTotal.text = "$nominalSelected"
            initAdapter()
            val toolbar: Toolbar = findViewById(R.id.toolbar)
            setSupportActionBar(toolbar)
            toolbar.setNavigationOnClickListener { finish() }
        } catch (e: java.lang.Exception) {
            error(mContext, resources.getString(R.string.app_name), e.toString())
        }
    }

    private fun initAdapter() {
        historyClaimAdapter = FormAdapter(listReimbursement, this)
        val mLayoutManager = LinearLayoutManager(this)
        rvKlaim.layoutManager = mLayoutManager
        rvKlaim.itemAnimator = DefaultItemAnimator()
        rvKlaim.adapter = historyClaimAdapter
        historyClaimAdapter.notifyDataSetChanged()
    }

    override fun getLayoutId(): Int = R.layout.activity_form_claim

    override fun initDestroy() {}

    override fun getResponse(response: BaseResponse<*>?, message: String?) {
        dismiss()
        when (response) {
            is APIResultReimbusement -> for (i in 0 until response.dataStatus.size) {
                if (response.dataStatus[i].status == "P") listReimbursement.add(response.dataStatus[i])
                historyClaimAdapter.notifyDataSetChanged()
            }
            is APIResultSendForm -> try {
                if (message.equals("detail found")) {
                    for(i in 0 until  response.dataStatus.details.size){
                        listReimbursement.add(response.dataStatus.details[i])
                        nominalSelected += response.dataStatus.details[i].nominal
                    }
                    historyClaimAdapter.notifyDataSetChanged()
                    tvTotal.setText(nominalSelected)
                } else {
                    val dialogBuilder = AlertDialog.Builder(mContext)
                    @SuppressLint("InflateParams") val dialogView =
                        LayoutInflater.from(mContext).inflate(R.layout.dialog_message, null)
                    dialogBuilder.setView(dialogView)

                    val etSetting = dialogView.findViewById<EditText>(R.id.etSetting)
                    val tvSetting = dialogView.findViewById<TextView>(R.id.tvSetting)
                    val tvSettingTitle = dialogView.findViewById<TextView>(R.id.tvSettingTitle)
                    val btnOK = dialogView.findViewById<Button>(R.id.btnOK)
                    val btnCancel = dialogView.findViewById<Button>(R.id.btnCancel)
                    val b = dialogBuilder.create()
                    b.show()
                    b.setCanceledOnTouchOutside(false)

                    val lp = WindowManager.LayoutParams()
                    lp.copyFrom(b.window?.attributes)
                    lp.width = WindowManager.LayoutParams.MATCH_PARENT
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT
                    lp.windowAnimations = R.style.DialogAnimation

                    b.window?.attributes = lp

                    tvSetting.textAlignment = View.TEXT_ALIGNMENT_CENTER
                    etSetting.visibility = View.GONE
                    btnCancel.visibility = View.GONE
                    tvSetting.text = message
                    tvSettingTitle.text = resources.getString(R.string.app_name)
                    btnOK.text = "OK"
                    btnOK.setOnClickListener {
                        b.dismiss()
                        finish()
                    }
                }
            } catch (e: Exception) {
                error(mContext, "ASCI-Ardi\nShowDialog", "error@BuildMessage\n$e\n${R.string.sendExceptionMessage}")
            }
            else -> error(mContext, resources.getString(R.string.app_name), "$message")
        }
    }

    companion object {
        fun startThisActivity(ctx: Context, data: String?) {
            val intent =
                Intent(ctx, FormClaimActivity::class.java).putExtra("data", data)
            ctx.startActivity(intent)
        }
    }

    override fun onClick() {
        object : setOnClick(arrayOf(btnPengajuanKlaim)) {
            @SuppressLint("NewApi", "SetTextI18n")
            override fun doClick(view: View) {
                when (view.id) {
                    R.id.btnPengajuanKlaim -> {
                        if (etJudul.text.toString() == "") error(mContext, "ASCI-HR", "Tolong masukkan subject terlebih dahulu")
                        else {
                            val dataEmployee =
                                getObject(mContext, "dataEmployee", DataStatusEmployee::class.java)
                            val request = FormReimbursement()
                            request.idEmployee = dataEmployee.idKaryawan.toString()
                            request.nominal = tvTotal.text.toString()
                            request.subject = etJudul.text.toString()
                            request.dateAssign = DateHelper.now("yyyy-MM-dd")
                            request.status = "P"
                            for (i in 0 until listReimbursement.size) {
                                if (listReimbursement[i].selected) request.details.add(listReimbursement[i])
                            }
                            val s = Gson().toJson(request)
                            repo.addForm(s)
                            loading(mContext, repo, "Sedang mengirim data reimburse")

                        }
                    }
                }
            }
        }
    }


}
