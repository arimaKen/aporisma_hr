package com.aporisma.aporismahr

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Spinner
import com.aporisma.aporisma_api.AporismaClient.BASE_URL
import com.aporisma.aporisma_api.Model.Company.APIResultCompany
import com.aporisma.aporisma_api.Model.Employee.APIResultEmployee
import com.aporisma.aporisma_api.Model.Token.APIResultToken
import com.aporisma.aporisma_api.Repo.Repo
import com.aporisma.aporisma_api.Response.BaseResponse
import com.aporisma.aporisma_api.Response.ResponseCourier
import com.aporisma.aporismahr.Helper.Dialog.dismiss
import com.aporisma.aporismahr.Helper.Dialog.error
import com.aporisma.aporismahr.Helper.Dialog.loading
import com.aporisma.aporismahr.Helper.Dialog.login
import com.aporisma.aporismahr.Helper.Dialog.settingServer
import com.aporisma.aporismahr.Helper.PrefHelper
import com.aporisma.base_componen.BaseActivity
import com.aporisma.base_componen.setOnClick
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : BaseActivity(), ResponseCourier {
    val spinnerArray = ArrayList<String>()
    private fun insertSpinnerMaster() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, spinnerArray)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        val sItems = findViewById<View>(R.id.spCompany) as Spinner
        sItems.adapter = adapter
    }

    val mContext: Context = this@LoginActivity
    val mActivity: Activity = this@LoginActivity
    var repo = Repo(mContext, this)
    override fun initCreate() {
        BASE_URL = PrefHelper.getPref(mContext, "IPserver")
        loading(mContext, repo, "Sedang memulai aplikasi")
        repo.callTOKEN()

    }

    override fun getLayoutId(): Int = R.layout.activity_login

    override fun onClick() {
        object : setOnClick(arrayOf(btnLogin, tvRegister, ivSetting)) {
            @SuppressLint("NewApi", "SetTextI18n")
            override fun doClick(view: View) {
                when (view.id) {
                    R.id.btnLogin -> {
                        loading(mContext, repo, "Sedang mencoba masuk")
                        repo.login(etUsername.text.toString(), etPassword.text.toString(), spCompany.selectedItem.toString())
                    }
                    R.id.tvRegister -> {
                        RegisterActivity.startThisActivity(mContext)
                    }
                    R.id.ivSetting -> {
                        settingServer(mContext, mActivity, resources.getString(R.string.app_name), "Setting Server")
                    }
                }
            }
        }
    }

    override fun initDestroy() {
    }

    override fun getResponse(response: BaseResponse<*>?, message: String?) {
        when (response) {
            is APIResultCompany -> {
                dismiss()
                spinnerArray.add("Company Name")
                for (i in 0 until response.dataStatus.size) {
                    if (response.dataStatus[i].approved.equals("Y")) spinnerArray.add(response.dataStatus[i].namaCompany)
                }
                insertSpinnerMaster()
                CheckRemaiderMe()
            }
            is APIResultToken -> repo.getMasterCompany()
            is APIResultEmployee -> {
                dismiss()
                PrefHelper.saveObjectToPref(mContext, "dataEmployee", response.dataStatus)
                if (swReminder.isChecked) PrefHelper.saveToPref(mContext, "swReminder", "Y")
                login(mContext, mActivity, resources.getString(R.string.app_name), "$message")
            }
            else -> {
                dismiss()
                error(mContext, "ASCI-HR", "$message")
            }
        }
    }

    fun CheckRemaiderMe() {
        if (PrefHelper.getPref(mContext, "swReminder") == "Y") {
            login(mContext, mActivity, resources.getString(R.string.app_name), "Login Succedd")
        }
    }
}
